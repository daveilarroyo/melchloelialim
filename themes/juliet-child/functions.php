<?php
/**
 * Functions
 *
 * This is the main functions file for the child theme.
 */

/*****************************************************************************
 * Register & Enqueue Child Theme Styles
****************************************************************************/
add_action( 'wp_enqueue_scripts', 'juliet_child_enqueue_styles' );

function juliet_child_enqueue_styles() {

    $parent_style = 'juliet-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ));
    wp_enqueue_style( 'responsive-style', get_stylesheet_directory_uri() . '/css/responsive.css', array( $parent_style ));
}

