<?php

/* Template Name: Block Layout */

//Page Meta
$juliet_meta = juliet_get_post_meta(get_the_ID(), array('block_cat', 'juliet-standard-page-title', 'juliet-promo-feature-type')); 
$juliet_page_title = isset($juliet_meta['juliet-standard-page-title']) && $juliet_meta['juliet-standard-page-title'] == 'juliet-title' ? true : false;
$juliet_below_feat_type = $juliet_meta['juliet-promo-feature-type'];
$juliet_below_feature = isset($juliet_below_feat_type) && ($juliet_below_feat_type == 'juliet-featured-slider' || $juliet_below_feat_type  == 'juliet-featured-image' || $juliet_below_feat_type == 'juliet-triple-images') ? 'juliet_below_feature' : ''; 
$juliet_cat = isset($juliet_meta['block_cat']) ? $juliet_meta['block_cat'] : 'juliet-all-cats';
$juliet_cat = is_numeric($juliet_cat) ? get_category($juliet_cat)->slug : $juliet_cat;
$juliet_theme_data->count = 0;

get_header(); 

//Load the Featured Section (Feat Image/Slider)
locate_template(array( 'inc/featured/featured.php' ), true, true ); ?>
	
</div>	<!-- End juliet-header-wrapper -->

<div id="juliet-content-container" class="<?php echo esc_attr($juliet_below_feature); ?>">

	<div class="juliet-container">

		<div id="juliet-content" class="juliet-full-width">

			<?php if($juliet_page_title) { ?>		
				<h1 class="juliet-template-title"><?php the_title(); ?></h1>
			<?php } ?>	

			<?php if(is_front_page()) {
				$paged = (get_query_var('page')) ? absint(get_query_var('page')) : 1;
			} else {
				$paged = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;
			}

			//Set Single Cat or All Cats
			if($juliet_cat == 'juliet-all-cats'){
				$args = array(
					'post_type' => 'post',
					'paged' => $paged,
					'ignore_sticky_posts' => 1,
				);
			} else {
				$args = array(
					'post_type' => 'post',
					'category_name' => $juliet_cat,
					'paged' => $paged,
					'ignore_sticky_posts' => 1
				);
			}

			// The Query
			$wp_query = new WP_Query( $args );

			// The Loop
			if ( $wp_query->have_posts() ) {
						
				while ( $wp_query->have_posts() ) {

					$wp_query->the_post();

					$juliet_theme_data->count++;

					get_template_part('content', 'block'); 
					
				} ?>

				<?php juliet_pagination();
					
				wp_reset_postdata(); 

			} ?>

		</div>	<!--end juliet-content -->	

	</div>	<!--end juliet-container -->	

	<?php get_footer(); ?>