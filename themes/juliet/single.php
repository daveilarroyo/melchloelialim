<?php 

//Theme Options
$juliet_general_options = get_option('juliet_general_theme_options'); 
$juliet_left_sidebar = isset($juliet_general_options['juliet_left_sidebar']) ? $juliet_general_options['juliet_left_sidebar'] : false;

//Post Options
$juliet_post_options = get_option('juliet_post_options'); 
$juliet_sidebar = isset($juliet_post_options['juliet_enable_post_sidebar']) ? $juliet_post_options['juliet_enable_post_sidebar'] : false;
$juliet_content_classes;
if($juliet_sidebar && $juliet_left_sidebar) {
	$juliet_content_classes = 'juliet-default-width juliet-content-right';
} else if ($juliet_sidebar) {
	$juliet_content_classes = 'juliet-default-width';
} else {
	$juliet_content_classes = 'juliet-full-width';
} 

get_header(); ?>

</div>	<!-- End juliet-header-wrapper -->

<div id="juliet-content-container">
	
	<div class="juliet-container">
		
		<div id="juliet-content" class="<?php echo esc_attr($juliet_content_classes); ?>">
		
			<div id="juliet-main">
			
				<?php if (have_posts()) {

					while (have_posts()) {

						the_post(); 
				
						get_template_part('content');
					}
				} ?>
				
			</div>

		</div>	<!--end content -->	

		<?php if($juliet_sidebar) {
			get_sidebar(); 
		} ?>
			
	</div>	<!--end juliet-container -->	

	<?php get_footer(); ?>