<article class="post">
					
	<div class="juliet-post-header nothing">
	
			<h1><?php esc_html_e( 'Nothing Found', 'juliet' ); ?></h1>
	</div>
	
	<div class="juliet-post-entry nothing">
	
		<?php if ( is_search() ) { ?>

			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'juliet' ); ?></p>
			<?php get_search_form(); ?>

		<?php } else { ?>

			<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'juliet' ); ?></p>
			<?php get_search_form(); ?>

		<?php } ?>
		
	</div>
	
</article>