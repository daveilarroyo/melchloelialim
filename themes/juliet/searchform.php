<?php
/**
* Template for WP Search Widget
*
* @author Pix & Hue
*/
?>

<form role="search" method="get" id="juliet-searchform" action="<?php echo esc_url(home_url( '/' )); ?>">
    <div><label class="screen-reader-text" for="s">Search for:</label>
        <input type="text" value="" name="s" id="s" placeholder="<?php esc_html_e('search', 'juliet'); ?>"/>
        <button type="submit" id="searchsubmit"><i class="fa fa-search"></i></button>
    </div>
</form>