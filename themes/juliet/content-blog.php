<?php 

/**
* Template for blog posts displayed on a page with the Default Blog Layout
* Used by: page-blog.php
*/

//Blog Options
global $juliet_gen_theme_options;
$juliet_sidebar = $juliet_gen_theme_options['sidebar'];
$juliet_post_options = get_option('juliet_blog_options');
$juliet_gen_theme_options['hide_share_buttons'] = isset($juliet_post_options['juliet_blog_hide_share_buttons']) ? $juliet_post_options['juliet_blog_hide_share_buttons'] : false;
$juliet_gen_theme_options['hide_feat_image'] = isset($juliet_post_options['juliet_blog_hide_featured_image']) ? $juliet_post_options['juliet_blog_hide_featured_image'] : false;
$juliet_gen_theme_options['hide_post_excerpt'] = isset($juliet_post_options['juliet_blog_hide_excerpt']) ? $juliet_post_options['juliet_blog_hide_excerpt'] : false;
$juliet_gen_theme_options['hide_comm_count'] = isset($juliet_post_options['juliet_blog_hide_comment_count']) ? $juliet_post_options['juliet_blog_hide_comment_count'] : false;
$juliet_gen_theme_options['enable_pinterest'] = isset($juliet_post_options['juliet_blog_enable_pinterest']) ? $juliet_post_options['juliet_blog_enable_pinterest'] : false;
$juliet_gen_theme_options['no_post_footer'] = $juliet_gen_theme_options['hide_post_excerpt'] && $juliet_gen_theme_options['hide_share_buttons'] && ($juliet_gen_theme_options['hide_comm_count'] || !comments_open()) ? 'juliet-no-post-footer' : '';
$juliet_gen_theme_options['block_layout'] = false;
$juliet_no_full_content_footer = $juliet_gen_theme_options['hide_post_excerpt'] && ($juliet_gen_theme_options['hide_comm_count'] || !comments_open()) && $juliet_gen_theme_options['hide_share_buttons'] ? 'juliet-full-no-footer' : '';
$juliet_show_full_content = $juliet_gen_theme_options['hide_post_excerpt'] ? 'juliet-full-post-content' : '';

//Custom Post Types
$juliet_classes = get_post_class(); $juliet_wp_custom_type = false;
$juliet_custom_type = has_post_format('gallery') || has_post_format('video') || has_post_format('audio') ? true : false;
$juliet_wp_custom_type = in_array('tag-post-formats', $juliet_classes) ? true : false;
$juliet_gen_theme_options['post_img_type'] = ((!$juliet_custom_type || ($juliet_custom_type && $juliet_wp_custom_type)) && ($juliet_gen_theme_options['hide_feat_image'] || !has_post_thumbnail()))? 'juliet-no-post-img' : '';

//Post Meta 
$juliet_gen_theme_options['aff_type'] = get_post_meta(get_the_ID(), 'juliet-affiliate-type', true);
$juliet_gen_theme_options['aff_url'] = get_post_meta(get_the_ID(), 'juliet-affiliate-url', true);
$juliet_gen_theme_options['aff_image'] = get_post_meta(get_the_ID(), 'juliet-affiliate-img', true);
$juliet_gen_theme_options['aff_code'] = get_post_meta(get_the_ID(), 'juliet-affiliate-code', true);
$juliet_gen_theme_options['side_excerpt_size'] = $juliet_sidebar ? 275 : 290;
$juliet_gen_theme_options['excerpt_size'] =  $juliet_sidebar ? 245 : 405;
$juliet_gen_theme_options['link'] = false; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('juliet-blog-item'); ?>>

	<div class="juliet-post-content <?php echo esc_attr($juliet_show_full_content) . ' ' . esc_attr($juliet_no_full_content_footer); ?>">	
		
		<!--Display Post Media-->
		<?php locate_template( array('inc/templates/blog_templates/post_media.php'), true, false); ?>
	
		<!--Display Post Content-->
		<div class="juliet-post-entry <?php echo esc_attr($juliet_gen_theme_options['post_img_type']); ?>">
			<?php if(($juliet_gen_theme_options['aff_type'] == 'juliet-aff-link' && $juliet_gen_theme_options['aff_image'] != "") && !$juliet_gen_theme_options['hide_post_excerpt']) { 
				locate_template( array('inc/templates/blog_templates/affiliate_links/affiliate_link_side.php'), true, false);
			} else if(($juliet_gen_theme_options['aff_type'] == 'juliet-aff-code') && !$juliet_gen_theme_options['hide_post_excerpt']) {
				locate_template( array('inc/templates/blog_templates/affiliate_links/affiliate_link_below.php'), true, false);
			} else {
				locate_template( array('inc/templates/blog_templates/affiliate_links/affiliate_link_none.php'), true, false);
			} ?>
		</div>

	</div>

</article>