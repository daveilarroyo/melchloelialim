<?php 

//Theme Options
$juliet_general_options = get_option('juliet_general_theme_options');
$juliet_archive_options = get_option('juliet_archive_options');
$juliet_left_sidebar = isset($juliet_general_options['juliet_left_sidebar']) ? $juliet_general_options['juliet_left_sidebar'] : false;
$juliet_layout = isset($juliet_archive_options['juliet_archive_layout']) ? $juliet_archive_options['juliet_archive_layout'] : 'default';

//Archive Options
$juliet_gen_theme_options['sidebar'] = isset($juliet_archive_options['juliet_archive_page_width']) ? $juliet_archive_options['juliet_archive_page_width'] : false;
$juliet_gen_theme_options['add_pin_it'] = isset($juliet_archive_options['juliet_archive_gal_options']) && $juliet_archive_options['juliet_archive_gal_options'] == 'gal-pin-button' ? true : false; 
$juliet_gen_theme_options['gal_overlay'] = isset($juliet_archive_options['juliet_archive_gal_options']) && $juliet_archive_options['juliet_archive_gal_options'] == 'gal-hover-overlay' ? true : false; 
$juliet_theme_data->count = 0; 

//Layout Class Name
$juliet_content_classes;
if($juliet_gen_theme_options['sidebar'] && $juliet_left_sidebar && $juliet_layout != 'block') {
	$juliet_content_classes = 'juliet-default-width juliet-content-right';
} else if ($juliet_gen_theme_options['sidebar'] && $juliet_layout != 'block') {
	$juliet_content_classes = 'juliet-default-width';
} else {
	$juliet_content_classes = 'juliet-full-width';
} ?>

<?php get_header(); ?>

</div>	<!-- End juliet-header-wrapper -->

<div id="juliet-content-container">

	<div class="juliet-container">

		<div id="juliet-content" class="<?php echo esc_attr($juliet_content_classes); ?>">

			<div class="juliet-archive-box">
		
				<span><?php esc_html_e( 'Category:', 'juliet' ); ?></span>
				<h1><?php printf( esc_html__( '%s', 'juliet' ), single_cat_title( '', false ) ); ?></h1>
			
			</div>	

			<?php if($juliet_layout == 'gallery') { ?>
				<div class="juliet-gallery">	
			<?php }

			if (have_posts()) { 

				while (have_posts()) { 

					the_post();

					$juliet_theme_data->count++;
			
					if($juliet_layout == 'default') {

						get_template_part('content', 'blog'); 

					} else if ($juliet_layout == 'block') {

						get_template_part('content', 'block'); 
					
					} else if ($juliet_layout == 'gallery') {

						get_template_part('content', 'gallery'); 
					} 
				}

				if($juliet_layout == 'gallery') { ?>
					</div>
				<?php }	

				//Pagination 
				juliet_pagination();	

			} else {
				
				get_template_part('content', 'none'); 

			} ?>

		</div> <!--End juliet-content -->

		<?php if($juliet_gen_theme_options['sidebar'] && $juliet_layout != 'block') {
			get_sidebar(); 
		} ?>
		
	</div> 	<!--End juliet-container -->

	<?php get_footer(); ?>		