<?php 

//Theme Options
$juliet_general_options = get_option('juliet_general_theme_options');
$juliet_left_sidebar = isset($juliet_general_options['juliet_left_sidebar']) ? $juliet_general_options['juliet_left_sidebar'] : false;
$juliet_content_classes = $juliet_left_sidebar ? 'juliet-default-width juliet-content-right' : 'juliet-default-width';
$juliet_meta = juliet_get_post_meta(get_the_ID(), array('juliet-promo-feature-type', 'juliet-standard-page-title'));
$juliet_below_feat_type = $juliet_meta['juliet-promo-feature-type'];
$juliet_below_feature = isset($juliet_below_feat_type) && ($juliet_below_feat_type == 'juliet-featured-slider' || $juliet_below_feat_type  == 'juliet-featured-image' || $juliet_below_feat_type == 'juliet-triple-images') ? 'juliet_below_feature' : ''; 

//Page Options
$juliet_gen_theme_options['page_title'] = isset($juliet_meta['juliet-standard-page-title']) && $juliet_meta['juliet-standard-page-title'] == 'juliet-title' ? true : false;
$juliet_gen_theme_options['sidebar'] = true;

get_header(); 

//Load the Featured Section
locate_template(array( 'inc/featured/featured.php' ), true, true ); ?>

</div>	<!-- End juliet-header-wrapper -->

<div id="juliet-content-container" class="<?php echo esc_attr($juliet_below_feature); ?>">

	<div class="juliet-container">

		<div id="juliet-content" class="<?php echo esc_attr($juliet_content_classes); ?>">	

			<?php if(have_posts()) { 

				while(have_posts()) { 

					the_post();

					get_template_part('content', 'page'); 	
				}
				
			} ?>

		</div>	<!--end juliet-content -->	

		<?php get_sidebar(); ?>		

	</div>	<!--end juliet-container -->	
		
	<?php get_footer(); ?>				