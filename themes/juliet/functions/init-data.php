<?php

/**
 * This file contains init data for the theme menu pages: 
 * Menu Page: "Juliet Options"
 * Submenu pages: 3 Custom Pages
 * Custom pages are used to create featured image, feature slider, and triple images.
 *
 * @author Pix & Hue
 */

/*****************************************************************************
 * Define Constants 
****************************************************************************/
if ( !defined( 'JULIET_FEAT_IMAGE_CAT' ) )
	define( 'JULIET_FEAT_IMAGE_CAT', 'juliet_cp_feature_image' );

if ( !defined( 'JULIET_FEAT_SLIDER_CAT' ) )
	define( 'JULIET_FEAT_SLIDER_CAT', 'juliet_cp_feat_slider' );

if ( !defined( 'JULIET_TRIPLE_IMAGES_CAT' ) )
	define( 'JULIET_TRIPLE_IMAGES_CAT', 'juliet_cp_triple_images' );


/*****************************************************************************
 * Render the Primary Admin Menu (and associated submenu page)
****************************************************************************/
add_action( 'admin_menu', 'juliet_admin_menu' );

if ( !function_exists('juliet_admin_menu') )  {

	function juliet_admin_menu() {
	    add_theme_page( 'Juliet Options', 'Juliet Options', 'edit_theme_options', 'juliet_options', 'juliet_render_options_page');
	}
}

/*****************************************************************************
 * Create Custom Category for Submenu Pages
****************************************************************************/
add_action( 'admin_init', 'juliet_create_custom_page_cats' );

if ( !function_exists('juliet_create_custom_page_cats') )  {

	function juliet_create_custom_page_cats() {
		
		wp_create_category(JULIET_FEAT_IMAGE_CAT);
		wp_create_category(JULIET_FEAT_SLIDER_CAT);
		wp_create_category(JULIET_TRIPLE_IMAGES_CAT);

		add_filter('list_terms_exclusions', 'juliet_exclude_custom_categories', 10, 2);
	}
}	


if ( !function_exists('juliet_exclude_custom_categories') )  {
	
	function juliet_exclude_custom_categories($exclusions) {

		remove_filter('list_terms_exclusions', 'juliet_exclude_custom_categories');

		$feat_image_cat_id = get_category_by_slug(JULIET_FEAT_IMAGE_CAT)->term_id;
		$feat_slider_cat_id = get_category_by_slug(JULIET_FEAT_SLIDER_CAT)->term_id;
		$feat_promo_cat_id = get_category_by_slug(JULIET_TRIPLE_IMAGES_CAT)->term_id;

		add_filter('list_terms_exclusions', 'juliet_exclude_custom_categories');
		
		return $exclusions . ' AND ( t.term_id <> ' . $feat_image_cat_id . ') AND ( t.term_id <> ' . $feat_slider_cat_id . ') AND ( t.term_id <> ' . $feat_promo_cat_id . ')';
	}
}	


add_action( 'pre_get_posts', 'juliet_exclude_categories_pages' );

if ( !function_exists('juliet_exclude_categories_pages') )  {

	function juliet_exclude_categories_pages( $query ) {

		remove_filter('list_terms_exclusions', 'juliet_exclude_custom_categories');
	    
	    $feat_image_cat_id = get_category_by_slug(JULIET_FEAT_IMAGE_CAT)->term_id;
	    $feat_slider_cat_id = get_category_by_slug(JULIET_FEAT_SLIDER_CAT)->term_id;
		$feat_promo_cat_id = get_category_by_slug(JULIET_TRIPLE_IMAGES_CAT)->term_id;

		$feat_exclude_cats = array( $feat_image_cat_id, $feat_slider_cat_id, $feat_promo_cat_id);	

	    global $pagenow; 

	    if( !is_admin() || (is_admin() && $pagenow == 'edit.php')) {
	    	$query->set('category__not_in', $feat_exclude_cats);
	    }
	}
}	

/*****************************************************************************
 * Create the Custom Submenu Pages (and options for each page type)
****************************************************************************/
if ( !function_exists( 'juliet_define_custom_pages' ) ) {

	function juliet_define_custom_pages() {

		global $juliet_theme_data;

		$juliet_theme_data->custom_pages = array(

			//Featured Slider
			JULIET_FEAT_SLIDER_CAT => new Juliet_Custom_Page(JULIET_FEAT_SLIDER_CAT, 'Featured Sliders', 'juliet_featured_sliders', array(
				array('title' => 'Select Slider Type', 'id' => 'feat_slider_type', 'name' => 'feat_slider_type', 'type' => 'select', 'start_field' => 'start_row', 'end_field' => 'end_row',
					'options'=>array(array('id'=>'cat-slides', 'name'=>'Category'), array('id'=>'man-slides', 'name'=>'Enter Post Ids'))),
				array('title' => 'Select A Category', 'id' => 'feat_content_cat', 'name' => 'feat_content_cat', 'type' => 'select_cat', 'start_field' => 'start_row', 'end_field' => 'end'),
				array('title'=>'Number of Slides', 'id'=>'feat_slider_num', 'name' => 'feat_slider_num', 'start_field' => 'start', 'end_field' => 'end_row', 'type'=>'select',  
					'options'=>array(array('id'=>'1', 'name'=>1), array('id'=>'2', 'name'=>2), array('id'=>'3', 'name'=>3), array('id'=>'4', 'name'=>4), array('id'=>'5', 'name'=>5))),
				array('title' => 'Enter Post IDs Separated by Commas (ex. 2,89,123)', 'id' => 'feat_post_ids', 'name' => 'feat_post_ids', 'type' => 'text', 'start_field' => 'start_row', 'end_field' => 'end_row'),

			)),	

			//Featured Image
			JULIET_FEAT_IMAGE_CAT => new Juliet_Custom_Page(JULIET_FEAT_IMAGE_CAT, 'Featured Images', 'juliet_featured_images', array(
				array('title' => 'Image', 'id'=>'feat_url', 'name'=>'feat_url', 'type'=>'upload', 'start_field'=>'start_row', 'end_field' => 'end'),
				array('title' => 'Background: Color', 'id' => 'feat_background_color', 'name' => 'feat_background_color', 'type' => 'color', 'default' => '#e7e7e7', 'start_field'=>'start'),
				array('title' => 'Title: Text', 'id' => 'feat_title', 'name' => 'feat_title', 'type' => 'text'),
				array('title' => 'Link', 'id' => 'feat_link', 'name' => 'feat_link', 'type' => 'text', 'text_type'=>'website'),
				array('title'=>'Open link in', 'id'=>'feat_open_option', 'name' => 'feat_open_option', 'end_field' => 'end_row', 'type'=>'select',  
					'options'=>array(array('id'=>'same', 'name'=>'Same tab / window'), array('id'=>'new', 'name'=>'New tab / window'))), 		
			)),

			//Triple Images
			JULIET_TRIPLE_IMAGES_CAT => new Juliet_Custom_Page(JULIET_TRIPLE_IMAGES_CAT, 'Triple Images', 'juliet_triple_images', array(
				array('title' => 'Image', 'id'=>'url_one', 'name'=>'url_one', 'type'=>'upload', 'start_field'=>'start_row', 'end_field' => 'end', 'number' => 1),
				array('title' => 'Link', 'id' => 'link_one', 'name' => 'link_one', 'type' => 'text', 'start_field'=>'start', 'text_type'=>'website'),
				array('title'=>'Open link in', 'id'=>'open_option_one', 'name' => 'open_option_one', 'end_field' => 'end_row', 'type'=>'select',  
					'options'=>array(array('id'=>'same', 'name'=>'Same tab / window'), array('id'=>'new', 'name'=>'New tab / window'))),
				array('title' => 'Image', 'id'=>'url_two', 'name'=>'url_two', 'type'=>'upload', 'start_field'=>'start_row', 'end_field' => 'end', 'number' => 2),
				array('title' => 'Link', 'id' => 'link_two', 'name' => 'link_two', 'type' => 'text', 'start_field'=>'start', 'text_type'=>'website'),
				array('title'=>'Open link in', 'id'=>'open_option_two', 'name' => 'open_option_two', 'end_field' => 'end_row', 'type'=>'select',  
					'options'=>array(array('id'=>'same', 'name'=>'Same tab / window'), array('id'=>'new', 'name'=>'New tab / window'))), 
				array('title' => 'Image', 'id'=>'url_three', 'name'=>'url_three', 'type'=>'upload', 'start_field'=>'start_row', 'end_field' => 'end', 'number' => 3),
				array('title' => 'Link', 'id' => 'link_three', 'name' => 'link_three', 'type' => 'text', 'start_field'=>'start', 'text_type'=>'website'),
				array('title'=>'Open link in', 'id'=>'open_option_three', 'name' => 'open_option_three', 'end_field' => 'end_row', 'type'=>'select',  
					'options'=>array(array('id'=>'same', 'name'=>'Same tab / window'), array('id'=>'new', 'name'=>'New tab / window'))), 
			)), 		
		);
	}
}

juliet_define_custom_pages();