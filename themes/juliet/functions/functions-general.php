<?php
/**
 * This file contains the general functions used by the theme.
 * 
 * @author Pix & Hue
 */

/*****************************************************************************
 * Title Tag
****************************************************************************/
add_action( 'after_setup_theme', 'juliet_title_setup' );

if ( !function_exists('juliet_title_setup') )  {

	function juliet_title_setup() {

		add_theme_support( 'title-tag' );
	}
}

/*****************************************************************************
 * Google Font Functions
****************************************************************************/
if ( !function_exists('juliet_google_fonts_url') )  {

	function juliet_google_fonts_url() {

		$fonts_url = '';

		 /*
   		 Translators: If there are characters in your language that are not supported
   		 by chosen font(s), translate this to 'off'. Do not translate into your own language.
    	*/
   		 if ( 'off' !== _x( 'on', 'Google font: on or off', 'juliet' ) ) {
        	$fonts_url = add_query_arg( 'family', urlencode( 'Raleway:300,400,800&subset=latin,latin-ext' ), "//fonts.googleapis.com/css" );
   		 }
   		 
   		return esc_url_raw($fonts_url);
	}
}	

//Enqueue Fonts
add_action( 'wp_enqueue_scripts', 'juliet_enqueue_google_fonts' );

if ( !function_exists('juliet_enqueue_google_fonts') )  {
	
	function juliet_enqueue_google_fonts() {
		
		wp_enqueue_style( 'juliet-google-fonts', juliet_google_fonts_url(), array(), null );
	}
}	

/*****************************************************************************
 * Get Post Meta Data
****************************************************************************/	
if ( !function_exists('juliet_get_post_meta') )  {

	function juliet_get_post_meta($post_id, $keys) {

		$meta = array();
		foreach($keys as $key) {
			$meta[$key]= get_post_meta( $post_id, $key, true);
		}

		return $meta;
	}
}

/*****************************************************************************
 * Create Custom Post Status for Custom Pages
****************************************************************************/
add_action( 'init', 'juliet_custom_page_post_status' );

if ( !function_exists('juliet_custom_page_post_status') )  {

	function juliet_custom_page_post_status(){
		
		register_post_status( 'juliet-custom-page', array(
			'label' 					=> esc_html__( 'Juliet Custom Page Status', 'juliet' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => false,
			'show_in_admin_status_list' => false,
			'query_var'					=> 'juliet-featured-image',
		) );
	}
}

/*****************************************************************************
 * Custom Excerpt Length - Used in juliet templates
****************************************************************************/
if ( !function_exists('juliet_custom_excerpt') )  {

	function juliet_custom_excerpt($wpfunction, $charCount) {
		
		$excerpt = $wpfunction;
		$length = strlen($excerpt);
		
		$excerpt = strip_shortcodes($excerpt);
		$excerpt = strip_tags($excerpt);
		$the_str = substr($excerpt, 0, $charCount);
		$the_str = trim(preg_replace( '/\s+/', ' ', $the_str));
		
		if($length > $charCount) {
			$the_str .= '[...]';
		}
			
		return $the_str;
	}
}

/*****************************************************************************
* Social Media Entity Decode
****************************************************************************/
if ( !function_exists('juliet_social_media_decode') )  {

	function juliet_social_media_decode( $title ) {
	    $title = html_entity_decode( $title ); 
	   	$title = urlencode( $title );

	    return $title;
	}
}	

/*****************************************************************************
* Top Bar Social Media Buttons
****************************************************************************/
if ( !function_exists('juliet_top_bar_social_media') )  {

	function juliet_top_bar_social_media() {

		$juliet_social_options = get_option('juliet_social_media_options');

		if($juliet_social_options['juliet_facebook']) : ?><a href="<?php echo esc_url('http://facebook.com/' . $juliet_social_options['juliet_facebook']); ?>" target="_blank"><i class="fa fa-facebook"></i></a><?php endif; ?>
		<?php if($juliet_social_options['juliet_instagram']) : ?><a href="<?php echo esc_url('http://instagram.com/' . $juliet_social_options['juliet_instagram']);?>" target="_blank"><i class="fa fa-instagram"></i></a><?php endif; ?>
		<?php if($juliet_social_options['juliet_pinterest']) : ?><a href="<?php echo esc_url('http://pinterest.com/' . $juliet_social_options['juliet_pinterest']);?>" target="_blank"><i class="fa fa-pinterest-p"></i></a><?php endif; ?>
		<?php if($juliet_social_options['juliet_twitter']) : ?><a href="<?php echo esc_url('http://twitter.com/' . $juliet_social_options['juliet_twitter']);?>" target="_blank"><i class="fa fa-twitter"></i></a><?php endif; ?>
		<?php if($juliet_social_options['juliet_google_plus']) : ?><a href="<?php echo esc_url('http://plus.google.com/' . $juliet_social_options['juliet_google_plus']);?>" target="_blank"><i class="fa fa-google-plus"></i></a><?php endif; ?>
		<?php if($juliet_social_options['juliet_bloglovin']) : ?><a href="<?php echo esc_url('http://bloglovin.com/' . $juliet_social_options['juliet_bloglovin']);?>" target="_blank"><i class="fa fa-heart"></i></a><?php endif; ?>	
		<?php if($juliet_social_options['juliet_tumblr']) : ?><a href="<?php echo esc_url('http://' . $juliet_social_options['juliet_tumblr'] . '.tumblr.com/'); ?>" target="_blank"><i class="fa fa-tumblr"></i></a><?php endif; ?>
		<?php if(isset($juliet_social_options['juliet_youtube']) && $juliet_social_options['juliet_youtube']) : ?><a href="<?php echo esc_url($juliet_social_options['juliet_youtube']); ?>" target="_blank"><i class="fa fa-youtube-play"></i></a><?php endif; ?>
		<?php if($juliet_social_options['juliet_vimeo']) : ?><a href="<?php echo esc_url('http://vimeo.com/' . $juliet_social_options['juliet_vimeo']);?>" target="_blank"><i class="fa fa-vimeo"></i></a><?php endif; ?>
		<?php if($juliet_social_options['juliet_linked_in']) : ?><a href="<?php echo esc_url($juliet_social_options['juliet_linked_in']);?>" target="_blank"><i class="fa fa-linkedin"></i></a><?php endif; ?>
		<?php if($juliet_social_options['juliet_rss']) : ?><a href="<?php echo esc_url($juliet_social_options['juliet_rss']);?>" target="_blank"><i class="fa fa-rss"></i></a><?php endif;
	}	
}

/*****************************************************************************
* Share Buttons
****************************************************************************/
if ( !function_exists('juliet_share_buttons') )  {

	function juliet_share_buttons() {

		global $post;
		$pin_image = get_the_ID() ? wp_get_attachment_url( get_post_thumbnail_id($post->ID)) : '';
		$meta = juliet_get_post_meta(get_the_ID(), array('juliet-pin-desc'));
		$pin_desc = isset($meta['juliet-pin-desc']) && $meta['juliet-pin-desc'] != '' ? juliet_social_media_decode($meta['juliet-pin-desc']) : juliet_social_media_decode(get_the_title()); 
		
		$html = '<a target="_blank" href="' . esc_url('https://www.facebook.com/sharer/sharer.php?u=' . get_the_permalink()) . '"><i class="fa fa-facebook"></i></a>';
		$html .= '<a target="_blank" href="' . esc_url('http://twitter.com/intent/tweet?text=Check%20out%20this%20article:%20' . juliet_social_media_decode(get_the_title()) . '%20-%20' . urlencode(get_the_permalink())) . '"><i class="fa fa-twitter"></i></a>';
		$html .= '<a data-pin-do="skipLink" target="_blank" href="' . esc_url('https://pinterest.com/pin/create/button/?url=' . get_the_permalink() . '&media=' . $pin_image .'&description=' . $pin_desc) . '"><i class="fa fa-pinterest-p"></i></a>';
		$html .= '<a target="_blank" href="' . esc_url('https://plus.google.com/share?url=' . get_the_permalink()) . '"><i class="fa fa-google-plus"></i></a>';
	
		return $html;
	}	
}	

/*****************************************************************************
 * Add Pinterest Button to Images in Post Content
 * Code modified from "Pinterest Img Tag Button" Plugin by Jan Dembowski
 * Code Source - http://pastebin.com/PJ9S4zWe
****************************************************************************/
add_filter( 'the_content' , 'juliet_post_pin_it_button' );

if ( !function_exists('juliet_post_pin_it_button') )  {

	function juliet_post_pin_it_button( $content ) {

		//Check if Post Option is Enabled
		$juliet_post_options = get_option('juliet_post_options');
		$enable_post_pinterest = isset($juliet_post_options['juliet_enable_pinterest']) ? $juliet_post_options['juliet_enable_pinterest'] : false;

		//Get Custom Pin Description 
		$meta = juliet_get_post_meta(get_the_ID(), array('juliet-pin-desc'));
		$pin_desc = isset($meta['juliet-pin-desc']) && $meta['juliet-pin-desc'] != '' ? juliet_social_media_decode($meta['juliet-pin-desc']) : juliet_social_media_decode(get_the_title()); 
	
		if(!$enable_post_pinterest) {
			return $content;
		} 

		else if($enable_post_pinterest) {
		
			$url_regex = "/\<img [^>]*src=\"([^\"]+)\"[^>]*>(?!<\/a>)/";
			$caption_regex = "/\[caption.*\[\/caption\]/";
			$link_regex = "/<a href=\"(.*)\">(.*)<\/a>(?!<p>)/";
			$regex_array = array($link_regex, $caption_regex, $url_regex);
			
			for($i = 0; $i < count($regex_array); $i++) {

				// If we get any hits then put the code before and after the img tags
				if ( preg_match_all($regex_array[$i], $content, $matches )) {
			        
			       for ( $count = 0; $count < count( $matches[0] ); $count++ ) {

		                $old_html = $matches[0][$count];
			        	
		                //Get the img URL & resize the image for Pinterest
		               	$image_url = preg_replace( '/^.*src="/' , '' , $old_html );
		                $image_url = preg_replace( '/".*$/' , '' , $image_url );
		              	$image = juliet_get_the_attachment_id($image_url);

		            	if($image) {
			             	$crop_width = 735;
			             	$crop_height = 	juliet_get_cropped_height($image);

			                $image_url = juliet_resize_wp_image($image_url, $crop_height, $crop_width); 
								
			                //Replace the image html with the pinterest button div
			                $pinterest_wrapper = '<span class="juliet-pin-it-wrapper">';
			                $pinterest_code = '<span class="juliet-pin-it-border"><a class="juliet-pin-it-button" data-pin-do="skipLink" target="_blank" href="'. esc_url('http://pinterest.com/pin/create/button/?url=' . get_the_permalink() . '&media=' . $image_url);
			                $pinterest_code .= '&description=' . esc_attr($pin_desc) . '">' . esc_html__('PIN IT!', 'juliet') . '</a></span>';
			                $pinterest_code .= '</span>';

			                $new_html = preg_replace('/^/', $pinterest_wrapper, $old_html );
			                $new_html = preg_replace('/$/', $pinterest_code, $new_html );

			                $content = str_replace( $old_html, $new_html , $content ); 
			            }
			        }
			    } 
			}  
				  
			return $content;
		}	
	}
}

/*****************************************************************************
 * Add Pinterest Button to Images in Page Templates
****************************************************************************/
if ( !function_exists('juliet_page_pin_it_button') )  {

	function juliet_page_pin_it_button($img_html, $img_id) {

		global $post;
		$pin_image = wp_get_attachment_url($img_id);
		$crop_width = 735;
		$crop_height = juliet_get_cropped_height($img_id);
		$pin_image = juliet_resize_wp_image($pin_image, $crop_height, $crop_width); 
		$meta = juliet_get_post_meta(get_the_ID(), array('juliet-pin-desc'));
		$pin_desc = isset($meta['juliet-pin-desc']) && $meta['juliet-pin-desc'] != '' ? juliet_social_media_decode($meta['juliet-pin-desc']) : juliet_social_media_decode(get_the_title()); 

		$html = '<div class="juliet-pin-it-wrapper">';
		$html .= $img_html;
		$html .= '<span class="juliet-pin-it-border"><a class="juliet-pin-it-button" data-pin-do="skipLink" target="_blank" href="' . esc_url('http://pinterest.com/pin/create/button/?url=' . get_the_permalink() . '&media=' . $pin_image);
		$html .= '&description=' . esc_attr($pin_desc) . '">' . esc_html__('PIN IT!', 'juliet') . '</a></span>';
		$html .= '</div>';

		return $html;
	}
}

/*****************************************************************************
 * Calculate Height of Img to Pin
****************************************************************************/
if ( !function_exists('juliet_get_cropped_height') )  {

	function juliet_get_cropped_height($img_id) {

		$img_meta = wp_get_attachment_metadata($img_id);
		$img_height = isset($img_meta['height']) ? $img_meta['height'] : '';
		$img_width = isset($img_meta['width']) ? $img_meta['width'] : '';

		$crop_width = 735;
		$crop_height = 0;
		if($img_width != 0) {
			$crop_height = ($crop_width * $img_height)/$img_width;
		}

		return $crop_height;
	}
}	

/*****************************************************************************
 * Get Attachment ID of an Image
****************************************************************************/	
if ( !function_exists('juliet_get_the_attachment_id') )  {

	function juliet_get_the_attachment_id($image_url) {
		
		global $wpdb;

		$strip_image_url = preg_replace( '/-\d+[Xx]\d+/' , '' , $image_url );
		$attachment_id = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $strip_image_url )); 

		if(!$attachment_id) {
			$attachment_id = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
		}

		if($attachment_id) {
			return $attachment_id[0];
		}
	}
}

/*****************************************************************************
 * Escape the Output of Affiliate Code 
****************************************************************************/		
if ( !function_exists('juliet_filter_affiliate_code') )  {

	function juliet_filter_affiliate_code($aff_code) {

		//Find the <script> tags in affiliate code
		$updated_html;
		$regex = '#&lt;script(.*?)?&gt;(.*?)&lt;/script?&gt;#is';
		
		if(preg_match_all($regex, $aff_code, $matches)) {
			$updated_html = preg_replace($regex, esc_js($matches[0][0]), $aff_code);
			$updated_html = str_replace("\'", "\"", $updated_html);
		} else {
			$updated_html = $aff_code;
		}	

		return $updated_html;
	}
}	

/*****************************************************************************
 * Modify Text of Read More Tag 
****************************************************************************/
add_filter( 'the_content_more_link', 'juliet_modify_read_more_link' );

if ( !function_exists('juliet_modify_read_more_link') )  {

	function juliet_modify_read_more_link() {
		$link = sprintf(wp_kses(__('<a class="more-link" href="%s">continue reading</a>', 'juliet'), array('a' => array('href' => array(), 'class'=> 'more-link'))), esc_url(get_permalink()));
		return $link;
	}
}	

/*****************************************************************************
 * Prevent Scroll on Read More Link
****************************************************************************/
add_filter( 'the_content_more_link', 'juliet_remove_more_link_scroll' );

if ( !function_exists('juliet_remove_more_link_scroll') )  {

	function juliet_remove_more_link_scroll( $link ) {
		$link = preg_replace( '|#more-[0-9]+|', '', $link );
		return $link;
	}
}

/*****************************************************************************
 * Resize Images
****************************************************************************/
if ( !function_exists('juliet_resize_wp_image') )  {

	function juliet_resize_wp_image($img_url, $height, $width) {
		$height = $height;
		$width = $width;
		$resized_img = aq_resize( $img_url, $width, $height, true, true, true );

		return $resized_img;
	}	
}

/*****************************************************************************
 * Imported Images - Convert to Local URL
****************************************************************************/
if ( !function_exists('juliet_adjust_img_url') )  {

	function juliet_adjust_img_url($url, $post_id, $field_id) {

		//Updated path name of imported image to be a local image
		$upload_dir = wp_upload_dir();
		$pos = strpos($url, 'uploads');
		$img_path = substr($url, $pos+strlen('uploads'));
		$new_path = $upload_dir['baseurl'] . $img_path;

		//Updated post meta of feature element to be local image 
		update_post_meta($post_id, $field_id, $new_path);
			
		return $new_path;

	}
}

/*****************************************************************************
 * Modify Comments Form - Add Placeholders & Remove Labels from Fields 
****************************************************************************/	
add_filter('comment_form_default_fields','juliet_mod_comment_form');	

if ( !function_exists('juliet_mod_comment_form') )  {
	
	function juliet_mod_comment_form($fields){

	    $fields['author'] = '<input placeholder="' . esc_html__( 'NAME', 'juliet' ) . '" id="author" name="author" type="text">';
	    $fields['email'] = '<input type="email" placeholder="' . esc_html__( 'EMAIL', 'juliet' ) . '"  id="email" name="email">';
	    $fields['url'] = '<input type="text" id="url" name="url" placeholder="' . esc_html__( 'WEBSITE', 'juliet' ) . '">';
       
	    return $fields;
	}
}

/*****************************************************************************
 * Comment Layout
****************************************************************************/
if ( !function_exists('juliet_comments') )  {

	function juliet_comments($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment;
		
		?>
		<li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
			
			<div class="thecomment">
									
				<div class="comment-text">
					<h6 class="author"><?php echo get_comment_author_link(); ?></h6>
					<?php if( $comment->comment_parent ) { ?>
    					<span class="comment-parent"><i class="fa fa-reply"></i><?php comment_author( $comment->comment_parent ); ?></span>
    				<?php } ?>
					<?php if ($comment->comment_approved == '0') { ?>
						<em><i class="icon-info-sign"></i> <?php esc_html_e('Comment awaiting approval', 'juliet'); ?></em>
						<br />
					<?php } ?>
					<?php comment_text(); ?>
					<?php if(get_comment_author_url()) { ?>
						<div class="comment-author-url"><a href="<?php esc_url(comment_author_url());?>" target="_blank"><?php esc_url(comment_author_url());?></a></div>
					<?php } ?>	
					<div class="comment-footer">
						<span class="date"><?php printf(esc_html__('%1$s', 'juliet'), get_comment_date()) ?></span>
					
						<span class="reply">
							<?php comment_reply_link(array_merge( $args, array('reply_text' => esc_html__('Reply', 'juliet'), 'depth' => $depth, 'max_depth' => $args['max_depth'])), $comment->comment_ID); ?>
						</span>
						<span class="edit"><?php edit_comment_link(wp_kses(__('Edit <i class="fa fa-circle"></i>', 'juliet'), array('i' => array('class' => 'fa fa-circle')))); ?></span>
					</div>	
				</div>
						
			</div>
			
		</li>

		<?php 
	}	
}	

/*****************************************************************************
 * Post Pagination
****************************************************************************/
if ( !function_exists('juliet_pagination') )  {

	function juliet_pagination() { ?>

		<div class="juliet-pagination">
			<div class="juliet-newer"><?php previous_posts_link(wp_kses(__( '<span class="juliet-pagination-wrapper"><i class="fa fa-angle-left"></i>Previous</span>', 'juliet'), array('span' => array('class' => 'pagination-wrapper'), 'i' => array('class' => 'fa fa-angle-left')))); ?></div>
			<div class="juliet-older"><?php next_posts_link(wp_kses(__( '<span class="juliet-pagination-wrapper">Next<i class="fa fa-angle-right"></i></span>', 'juliet'),  array('span' => array('class' => 'pagination-wrapper'), 'i' => array('class' => 'fa fa-angle-right')))); ?></div>
		</div>
						
	<?php }
}	