<?php

/**
 * This file contains the ajax functions for WP Admin, including:
 * adding, updating, and deleting objects within the custom pages.
 * 
 * @author Pix & Hue
 */

/*****************************************************************************
 * Add New Instance of Promo/Feature Object 
***************************************************************************/
add_action('wp_ajax_juliet_add_instance', 'juliet_add_instance');

if ( !function_exists('juliet_add_instance') ) {

	function juliet_add_instance() {

		global $wpdb;
		global $juliet_theme_data;

		//Check Nonce for Security
		check_ajax_referer('juliet-custom-nonce', 'nonce');

		//Create New Instance of Promo/Feature
		$title = isset($_POST['name']) ? wp_strip_all_tags($_POST['name']) : '';
		$cat_name = isset($_POST['cat_name']) ? $_POST['cat_name']: '';
		$cat_id = isset($_POST['cat_id']) ? $_POST['cat_id']: '';

		$new_feature = new Juliet_Custom_Data_Manager();
		$new_feature->juliet_set_post_title($title);
		$new_feature->juliet_set_post_cat_name($cat_name);
		$new_feature->juliet_set_post_cat_id($cat_id);
		$new_feature->juliet_insert_custom_post();
		
		//Add New Set Feature/Promo to the Custom Page 
		$post_id = $new_feature->juliet_get_post_ID();
		$custom_page = $juliet_theme_data->custom_pages[$cat_name];
		$fields = $custom_page->fields;
		
		$build_new_feature = new Juliet_Custom_Page_Builder($custom_page);
		$build_new_feature->juliet_build_custom_feature($post_id, $fields);

		wp_die();

	}
}

/*****************************************************************************
 * Resize Image of Promo/Feature Object 
***************************************************************************/
add_action('wp_ajax_juliet_resize_img', 'juliet_resize_img');

if ( !function_exists('juliet_resize_img') ) {

	function juliet_resize_img() {

		global $wpdb;	

		$img_url = isset($_POST['image']) ? $_POST['image'] : '';
		$resized_image = juliet_resize_wp_image($img_url, $_POST['height'], $_POST['width']);

		echo esc_url($resized_image);

		wp_die();

	}	
}	

/*****************************************************************************
 * Update Instance of Promo Object 
***************************************************************************/
add_action('wp_ajax_juliet_update_instance', 'juliet_update_instance');	

if ( !function_exists('juliet_update_instance') ) {

	function juliet_update_instance() {

		global $wpdb;

		//Check Nonce for Security
		check_ajax_referer('juliet-custom-nonce', 'nonce');
	
		//Update Instance of a Promo Feature
		$cat_name = isset($_POST['juliet-promo-feature-cat-type']) ? $_POST['juliet-promo-feature-cat-type'] : '';
		$cat_id = isset($_POST['juliet-promo-feature-cat-id']) ? $_POST['juliet-promo-feature-cat-id'] : '';
		$post_id = isset($_POST['juliet-promo-feature-post-id']) ? $_POST['juliet-promo-feature-post-id'] : '';
		
		$update_feature = new Juliet_Custom_Data_Manager();
		$update_feature->juliet_set_post_cat_name($cat_name);
		$update_feature->juliet_set_post_cat_id($cat_id);
		$update_feature->juliet_set_post_ID($post_id);
		$update_feature->juliet_update_custom_post();
		
		wp_die();

	}
}	

/*****************************************************************************
 * Delete Instance of Promo Object 
***************************************************************************/
add_action('wp_ajax_juliet_delete_instance', 'juliet_delete_instance');	

if ( !function_exists('juliet_delete_instance') ) {

	function juliet_delete_instance() {

		global $wpdb;

		//Check Nonce for Security
		check_ajax_referer('juliet-custom-nonce', 'nonce');

		//Delete Instance of a Promo Feature
		$post_id = isset($_POST['juliet-promo-feature-post-id']) ? $_POST['juliet-promo-feature-post-id'] : '';
		$delete_feature = new Juliet_Custom_Data_Manager();
		$delete_feature->juliet_set_post_ID($post_id);
		$delete_feature->juliet_delete_custom_post();

		wp_die();

	}
}