<?php
/**
 * This file contains the general functions used for the WooCommerce Plugin
 * 
 * @author Pix & Hue
 */

if (in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	/************************************************************************
 	* Remove Breadcrumbs
	*************************************************************************/
	add_action( 'init', 'juliet_woo_remove_breadcrumbs' );

	if ( !function_exists('juliet_woo_remove_breadcrumbs') )  {

		function juliet_woo_remove_breadcrumbs() {
	    	remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );	
		}
	}

	/************************************************************************
 	* Show/Hide Shop Page Title
	*************************************************************************/
	add_filter( 'woocommerce_show_page_title' , 'juliet_woo_hide_page_title' );

	if ( !function_exists('juliet_woo_hide_page_title') )  {

		function juliet_woo_hide_page_title() {
			$meta = juliet_get_post_meta(get_option( 'woocommerce_shop_page_id' ), array('juliet-standard-page-title')); 
			$hide_shop_title = isset($meta['juliet-standard-page-title']) && $meta['juliet-standard-page-title'] == 'juliet-title' ? false : true;
			return !$hide_shop_title;
		}	
	}

	/************************************************************************
 	* Change # of Related Products
	*************************************************************************/
	add_filter( 'woocommerce_output_related_products_args', 'juliet_woo_related_products_args' );

	if ( !function_exists('juliet_woo_related_products_args') )  {
		
		function juliet_woo_related_products_args( $args ) {
			$args['posts_per_page'] = 3; // 4 related products
			$args['columns'] = 3; // arranged in 2 columns
			return $args;	
		}
	}		

	/************************************************************************
 	* Change # of Products/Row
	*************************************************************************/
	add_filter('loop_shop_columns', 'juliet_woo_loop_columns');

	if (!function_exists('juliet_woo_loop_columns')) {
		
		function juliet_woo_loop_columns() {
			return 3;	
		}
	}

	/************************************************************************
 	* Theme Options - Set Items & Hide Related Products
	*************************************************************************/
	$juliet_shop_options = get_option('juliet_shop_options');
	$hide_related_products = isset($juliet_shop_options['juliet_hide_woo_related_products']) ? $juliet_shop_options['juliet_hide_woo_related_products'] : false;
	
	if($hide_related_products) {
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
	}

	$items_per_page = isset($juliet_shop_options['juliet_woo_items_per_page']) && $juliet_shop_options['juliet_woo_items_per_page'] != '' ? $juliet_shop_options['juliet_woo_items_per_page'] : 9;
	add_filter( 'loop_shop_per_page', create_function( '$cols', 'return ' . $items_per_page . ';' ), 20 );

	/************************************************************************
 	* Modify Review Form
	*************************************************************************/
	add_filter( 'woocommerce_product_review_comment_form_args', 'juliet_woo_mod_review_form' );

	if (!function_exists('juliet_woo_mod_review_form')) {

		function juliet_woo_mod_review_form( $review_form ) {

			$commenter = wp_get_current_commenter();

	    	// Shown to non-logged in users above name / email
	    	$review_form['fields'] = array(
	    		'author' => '<p class="comment-form-author">' . '<input id="author" name="author" type="text" placeholder="' . esc_html__( 'Author *', 'juliet' ) . 
	    		            '" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" required /></p>',
				'email' => '<p class="comment-form-email">' . '<input id="email" name="email" type="email" placeholder="' . esc_html__( 'Email *', 'juliet' ) . 
				           '" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-required="true" required /></p>',			
				);

	    	if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {
				$review_form['comment_field'] = '<p class="comment-form-rating"><label for="rating">' . esc_html__( 'Your Rating', 'juliet' ) .'</label><select name="rating" id="rating" aria-required="true" required>
					<option value="">' . esc_html__( 'Rate&hellip;', 'juliet' ) . '</option>
					<option value="5">' . esc_html__( 'Perfect', 'juliet' ) . '</option>
					<option value="4">' . esc_html__( 'Good', 'juliet' ) . '</option>
					<option value="3">' . esc_html__( 'Average', 'juliet' ) . '</option>
					<option value="2">' . esc_html__( 'Not that bad', 'juliet' ) . '</option>
					<option value="1">' . esc_html__( 'Very Poor', 'juliet' ) . '</option>
				</select></p>';
			}   

			$review_form['comment_field'] .= '<p class="comment-form-comment"><textarea id="comment" name="comment" placeholder="' . esc_html__( 'Your Review *', 'juliet' ) . 
	    	                                '" cols="45" rows="8" aria-required="true" required></textarea></p>';                             
	   
	    	return $review_form;
		}
	} 
}