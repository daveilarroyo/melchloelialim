<?php

/**
 * Functions support theme options in WP Admin
 * Functions used by /lib/options/init-options.php
 * Uses Wordpress Settings API
 * 
 * @author Pix & Hue
 */

/*****************************************************************************
 * Create Options Settings Field
****************************************************************************/	
if ( !function_exists('juliet_create_settings_field') ) {

	function juliet_create_settings_field($field, $callback, $page, $section) {

		$setting_id = 'juliet_' . $field['id'];
		$setting_title = $field['title'];
		$height = isset($field['height']) ? $field['height'] : '';
		$width = isset($field['width']) ? $field['width'] : '';
		$options = isset($field['options']) ? $field['options'] : '';

		add_settings_field($setting_id, $setting_title, 
    	 	$callback, $page, $section, 
    	 	array('id' => $setting_id, 'option_name' => $page, 'type' => $field['type'], 'height' => $height, 'width' => $width, 'options' => $options));
	}
}

/*****************************************************************************
 * Callback Function w/ Option Description
****************************************************************************/
//General Theme Options
if ( !function_exists('juliet_theme_options_callback') )  {

	function juliet_theme_options_callback($args) { 

		if($args['id'] == 'juliet_general_options_section') { 
			wp_kses(_e('<p class="option-description">Customize the General Settings for the Juliet Theme.</p>', 'juliet'),  array('p' => array('class' => 'option-description')));
		} else if($args['id'] == 'juliet_social_media_section') {
			wp_kses(_e('<p class="option-description">Enter your social media usernames. Icons will not show if left blank.</p>', 'juliet'), array('p' => array('class' => 'option-description')));
		} else if($args['id'] == 'juliet_blog_section') {
			wp_kses(_e('<p class="option-description">Customize the Settings for Pages.</p>', 'juliet'), array('p' => array('class' => 'option-description')));
		} else if($args['id'] == 'juliet_archive_section') {
			wp_kses(_e('<p class="option-description">Customize the Settings for Archive Pages.</p>', 'juliet'), array('p' => array('class' => 'option-description')));
		} else if($args['id'] == 'juliet_post_section') {
			wp_kses(_e('<p class="option-description">Customize the Settings for Single Blog Posts.</p>', 'juliet'), array('p' => array('class' => 'option-description')));
		} else if($args['id'] == 'juliet_shop_section') {
			wp_kses(_e('<p class="option-description">Customize the Settings for WooCommerce.</p>', 'juliet'), array('p' => array('class' => 'option-description')));
		}
	}
}

/*****************************************************************************
 * Render Options Fields on the Page
****************************************************************************/
if ( !function_exists('juliet_render_option_field') ) {

	function juliet_render_option_field($args) {
		
		if(isset($args['type'])) {

			switch($args['type']) {
				case 'text':
					juliet_create_text_field($args);
					break;

				case 'checkbox':	
					juliet_create_checkbox($args);
					break;	

				case 'select':
					juliet_create_select($args);
					break;	

				case 'upload':
					juliet_options_create_image($args);
					break;
			}
		}
	}
}	

/*****************************************************************************
 * Option Field Type: Create Input Text Field
****************************************************************************/
if ( !function_exists('juliet_create_text_field') ) {

	function juliet_create_text_field($args) {

		$option_name = $args['option_name'];
		$options = get_option($option_name);
		$value = isset($options[$args['id']]) ? $options[$args['id']] : '';
		$input_name = $option_name . '[' . $args['id'] . ']'; 

		if($args['id'] == 'juliet_linked_in') { ?>
			<input type="text" id="<?php echo esc_attr($args['id']);?>" name="<?php echo esc_attr($input_name);?>" value="<?php echo esc_url($value); ?>" />
			<p class="url-note">Note: Use the full URL to your LinkedIn Profile.</p>
		<?php } else if($args['id'] == 'juliet_youtube') { ?>	
			<input type="text" id="<?php echo esc_attr($args['id']);?>" name="<?php echo esc_attr($input_name);?>" value="<?php echo esc_url($value); ?>" />
			<p class="url-note">Note: Use the full URL to your YouTube channel/page.</p>
		<?php } else { ?>
			<input type="text" id="<?php echo esc_attr($args['id']);?>" name="<?php echo esc_attr($input_name);?>" value="<?php echo esc_attr($value); ?>" />
		<?php } 
	}
}

/*****************************************************************************
 * Option Field Type: Create Checkbox
****************************************************************************/	
if ( !function_exists('juliet_create_checkbox') ) {

	function juliet_create_checkbox($args) {

		$option_name = $args['option_name'];
		$options = get_option($option_name);
		$value = isset($options[$args['id']]) ? $options[$args['id']] : ' '; 
		$input_name = $option_name . '[' . $args['id'] . ']'; ?>

		<input type="checkbox" class="juliet-option-checkbox" id="<?php echo esc_attr($args['id']);?>" name="<?php echo esc_attr($input_name);?>" value="1" <?php checked($value, 1, true);?>/>
		<span class="checkbox-toggle checkbox-toggle-on"></span>
		<span class="checkbox-toggle checkbox-toggle-off">OFF</span>
   	 
	<?php } 
}

/*****************************************************************************
 * Option Field Type: Create Select
****************************************************************************/	
if ( !function_exists('juliet_create_select') ) {

	function juliet_create_select($args) {

		$option_name = $args['option_name'];
		$options = get_option($option_name);
		$value = isset($options[$args['id']]) ? $options[$args['id']] : ' ';
		$options = $args['options']; 
		$input_name = $option_name . '[' . $args['id'] . ']'; ?>

		<select id="<?php echo esc_attr($args['id']);?>" name="<?php echo esc_attr($input_name);?>">

			<?php foreach($options as $option) {
				$selected = $value==$option['id']?' selected="selected"':''; ?>
				<option value="<?php echo esc_attr($option['id']);?>" <?php echo esc_attr($selected); ?>><?php echo esc_html($option['name']);?></option>
			<?php } ?>

		</select>

	<?php } 
}	

/*****************************************************************************
 * Option Field Type: Create Image
****************************************************************************/	
if( !function_exists('juliet_options_create_image') ) {

	function juliet_options_create_image($args) {

		$option_name = $args['option_name'];
		$height = $args['height'];
		$width = $args['width'];
		$options = get_option($args['option_name']);
		$value = isset($options[$args['id']]) ? $options[$args['id']] : '';	
		$input_name = $option_name . '[' . $args['id'] . ']'; ?>

		<input type="hidden" class="juliet-img-url" id="?php echo esc_attr($args['id']);?>" name="<?php echo esc_attr($input_name); ?>" value ="<?php echo esc_url($value); ?>" />
		
		<?php if(empty($value)) { ?>
			<input type="button" value="Select Image" class="juliet-option-upload-button"/>
		<?php } else { ?>
			<input type="button" value="Select Image" class="juliet-option-upload-button" style="display: none"/>
		<?php } ?>			
	
		<div class="upload_img_wrapper">
			<?php if(empty($value)) { ?>
				<img src="" class="upload_img_preview">
			<?php } else { ?>	
				<img class="upload_img_preview" src="<?php echo esc_url(juliet_resize_wp_image($value, $height, $width)); ?>"/>
			<?php } ?>		
			
			<input type="button" value="x" class="juliet-option-remove-upload-button" />
		</div>

	<?php }
}

/*****************************************************************************
 * Input Santization 
****************************************************************************/	
if ( !function_exists('juliet_validate_social_media') ) {

	function juliet_validate_social_media($input) {

	    $output = array();
	   
	    foreach($input as $key => $value) {
	        if( isset($input[$key])) { 
	            $output[$key] = wp_strip_all_tags($input[$key]);
	        } 
	    }
	     
	    return apply_filters( 'juliet_validate_social_media', $output, $input );
	}
}