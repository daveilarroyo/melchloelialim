<?php
/**
 * Update Style of Customizer Settings 
 *
 * @author Pix & Hue
 */

add_action( 'wp_head', 'juliet_customizer_css' );

function juliet_customizer_css() { ?>
	
	<style type="text/css">

		/* ----- Top Bar ----- */
		#juliet-top-bar, .juliet-top-search-bar #juliet-searchform input[type="text"] { background-color:<?php echo get_theme_mod( 'jul_top_bar_background_color' ); ?>; }
		.juliet-top-social a, .juliet-top-search-bar #juliet-searchform .fa-search { color:<?php echo get_theme_mod( 'jul_top_bar_text_color' ); ?>; }
		.juliet-top-search-bar ::-webkit-input-placeholder { color:<?php echo get_theme_mod( 'jul_top_bar_text_color' ); ?>; }
		.juliet-top-search-bar ::-moz-placeholder { color:<?php echo get_theme_mod( 'jul_top_bar_text_color' ); ?>; }
		.juliet-top-search-bar :-ms-input-placeholder { color:<?php echo get_theme_mod( 'jul_top_bar_text_color' ); ?>; }
		.juliet-top-search-bar :-moz-placeholder { color:<?php echo get_theme_mod( 'jul_top_bar_text_color' ); ?>; }
		.juliet-top-search-bar #searchform input[type="text"] { border-color:<?php echo get_theme_mod( 'jul_top_bar_text_color' ); ?>; }
		.juliet-top-social a:hover, .juliet-top-search-bar #juliet-searchform .fa-search:hover { color:<?php echo get_theme_mod( 'jul_top_bar_text_hover_color' ); ?>; }

		/*Cart Icon*/
		.juliet-cart-count:before { color:<?php echo get_theme_mod( 'jul_cart_icon_bg_color'); ?>; }
		.juliet-count-val { color:<?php echo get_theme_mod( 'jul_cart_icon_count_color'); ?>; }
		.juliet-count-val { background-color:<?php echo get_theme_mod( 'jul_cart_icon_count_bg_color'); ?>; }

		/* ----- Main Menu ----- */
		#juliet-nav-wrapper, #juliet-nav-wrapper .menu .sub-menu, #juliet-nav-wrapper .menu .children, .is-sticky #juliet-nav-wrapper { background-color:<?php echo get_theme_mod( 'jul_menu_background_color' ); ?>;}
		#juliet-nav-wrapper, #juliet-nav-wrapper .menu .sub-menu li, #juliet-nav-wrapper .menu .sub-menu, .is-sticky #juliet-nav-wrapper { border-color:<?php echo get_theme_mod( 'jul_menu_border_color' ); ?>;}
		#juliet-nav-wrapper .menu li a, #juliet-nav-wrapper ul.menu ul a, #juliet-nav-wrapper .menu ul ul a, .is-sticky #juliet-nav-wrapper .juliet-sticky-social-media a { color:<?php echo get_theme_mod( 'jul_menu_text_color' ); ?>; }
		#juliet-nav-wrapper .menu li a:hover, #juliet-nav-wrapper ul.menu ul a:hover, #juliet-nav-wrapper .menu ul ul a:hover, .is-sticky #juliet-nav-wrapper .juliet-sticky-social-media a:hover { color:<?php echo get_theme_mod( 'jul_menu_text_hover_color' ); ?>; }

		/* ----- Mobile Menu ----- */
		.slicknav_btn { background-color:<?php echo get_theme_mod( 'jul_mobile_menu_top_bar_color'); ?>; }
		.slicknav_menu .slicknav_icon-bar { background-color:<?php echo get_theme_mod( 'jul_mobile_menu_icon_color'); ?>; }
		.slicknav_menu, .slicknav_nav > li, .slicknav_nav .sub-menu > li { border-color: <?php echo get_theme_mod( 'jul_mobile_menu_border_color'); ?>; }
		.slicknav_menu { background-color:<?php echo get_theme_mod( 'jul_mobile_menu_dropdown_bg_color'); ?>; }
		.slicknav_nav a { color:<?php echo get_theme_mod( 'jul_mobile_menu_text_color'); ?>; }
		.slicknav_nav a:hover { color:<?php echo get_theme_mod( 'jul_mobile_menu_hover_text_color'); ?>; }
		
		/* ----- General Site Colors ----- */
		/*Accent Color*/
		.juliet-template-title, .juliet-page-header h1, .juliet-cat-highlighted, .juliet-archive-box span, .juliet-error-page h1,
		.woocommerce .page-title, .woocommerce-MyAccount-navigation ul li a:hover { color:<?php echo get_theme_mod( 'jul_gen_accent_color'); ?>; }
		input[type="submit"], .wpcf7 input[type="submit"], .juliet-comments-title, .comment-form input[type="submit"], .comment .author, .pingback .thecomment .author, 
		.juliet-blog-item .juliet-post-content, .juliet-post-excerpt-links a, .juliet-affiliate-title, .juliet-share-text, .juliet-blog-item.sticky:after, 
		.juliet-blog-item, .woocommerce button.button.alt, .woocommerce a.button.alt, .woocommerce input.button.alt, 
		.woocommerce #review_form #respond .form-submit input, .more-link { background-color:<?php echo get_theme_mod( 'jul_gen_accent_color'); ?>; }
		.single-juliet-post .juliet-post-header, .juliet-block-layout .juliet-post-entry, .single-juliet-post .juliet-post-entry-wrapper, .juliet-blog-item, 
		.juliet-full-width .juliet-blog-item.sticky .juliet-post-entry.juliet-no-post-img, .juliet-blog-item .juliet-post-entry.juliet-no-post-img, 
		.juliet-blog-item.sticky .juliet-post-entry.juliet-no-post-img, .juliet-post-entry.juliet-no-post-img, .juliet-blog-item.sticky:before,
		.juliet-full-width .juliet-blog-item .juliet-post-entry.juliet-no-post-img, .woocommerce-info { border-color:<?php echo get_theme_mod( 'jul_gen_accent_color'); ?>; }
		
		/*Border Color*/
		.juliet-post-author-box, .juliet-prev-post-link, .comment-form .form-submit, .comment-form input, .comment-form textarea, .juliet-related-title:before, 
		.juliet-post-excerpt-border, input, select, textarea, .wpcf7 input, .wpcf7 textarea, .wpcf7-submit-border, .juliet-custom-wpcf7 .wpcf7-submit-border,
		.juliet-post-excerpt-border:before, .juliet-post-excerpt-border:after, .juliet-item-related-box, .juliet-related-title:after, .juliet-related-title, 
		.juliet-post-entry-right, .juliet-affiliate-widget, .juliet-affiliate-item, .juliet-share-text-border, .juliet-error-page #juliet-searchform, 
		.juliet-post-entry.nothing #juliet-searchform, #juliet-searchform input[type="text"], .juliet-share-text-border:before, .juliet-share-text-border:after, 
		.more-link:after, .juliet-post-content ul.comments li, .juliet-page-content ul.comments li, .juliet-full-width .juliet-post-entry-left, 
		.woocommerce #reviews #comments ol.commentlist li, .woocommerce-tabs, .woocommerce div.product .woocommerce-tabs ul.tabs:before, 
		.woocommerce .quantity .qty, .woocommerce-MyAccount-navigation ul li, .woocommerce-MyAccount-navigation ul li, .woocommerce button.button.alt:after, 
		.woocommerce a.button.alt:after, .woocommerce input.button.alt:after, .woocommerce form.login, .woocommerce form.register, .woocommerce form.checkout_coupon, 
		.woocommerce div.product .woocommerce-tabs ul.tabs li, .woocommerce div.product .woocommerce-tabs ul.tabs li:last-of-type,  .select2-container .select2-choice,
		.woocommerce div.product .woocommerce-tabs ul.tabs  { border-color:<?php echo get_theme_mod( 'jul_border_color'); ?>; }	
		
		/*Link Color*/
		a, .juliet-about-widget a, .juliet-post-comment-count a:hover, .juliet-post-share-buttons a:hover, .juliet-woo-share-buttons a:hover, 
		.juliet-post-author-box .author-social:hover, .juliet-post-tags a:hover, .juliet-post-header h2 a:hover, .juliet-cat a:hover, .juliet-side-item-text a:hover, 
		.juliet-author-content h5 a:hover, .juliet-item-related h3 a:hover, .woocommerce .star-rating, .juliet-post-comments .reply a:hover, .comment .edit a:hover, 
		.pingback .thecomment .edit a:hover, .juliet-older a:hover, .juliet-newer a:hover, .juliet-post-pagination a:hover, .comment-author-url a:hover, 
		#juliet-searchform .fa-search:hover, .widget_mc4wp_form_widget a, .textwidget a, .product_meta .posted_in a:hover, .product_meta .tagged_as a:hover,
		.woocommerce ul.products li.product h3:hover { color:<?php echo get_theme_mod( 'jul_link_color'); ?>; }
		.woocommerce .widget_price_filter .ui-slider .ui-slider-handle { background-color:<?php echo get_theme_mod( 'jul_link_color'); ?>; }
		
		/* ----- Button Colors ----- */	

		/*Pinterest Button*/
		.juliet-pin-it-border { border-color:<?php echo get_theme_mod( 'jul_pin_it_button_border_color'); ?> }
		.juliet-pin-it-button {color:<?php echo get_theme_mod( 'jul_pin_it_button_text_color'); ?>; background-color:<?php echo get_theme_mod( 'jul_pin_it_button_bg_color'); ?>;}
		.juliet-pin-it-button:hover { color:<?php echo get_theme_mod( 'jul_pin_it_button_hover_color'); ?>; }
		
		/*Back to Top*/
		<?php $btt_hex = get_theme_mod( 'jul_back_to_top_bg_color');
		list($r_1, $g_1, $b_1) = sscanf($btt_hex, "#%02x%02x%02x"); ?>
		#juliet_back_to_top span { border-color:<?php echo get_theme_mod( 'jul_back_to_top_color'); ?>; color:<?php echo get_theme_mod( 'jul_back_to_top_color'); ?>; background:rgba(<?php echo esc_attr($r_1); ?>, <?php echo esc_attr($g_1); ?>, <?php echo esc_attr($b_1); ?>, .5); }


		/* ----- Featured Section ----- */
		.juliet-feat-title-border { border-color:<?php echo get_theme_mod( 'jul_feat_box_border_color'); ?>; }
		.juliet-feat-img-top-title { background-color:<?php echo get_theme_mod( 'jul_feat_box_bg_color'); ?>; color:<?php echo get_theme_mod( 'jul_feat_box_text_color'); ?>; }
		.juliet-feat-img-link .juliet-feat-img-top-title:hover { color:<?php echo get_theme_mod( 'jul_feat_box_text_hover_color'); ?>; }
		
		/* ----- Blog & Block Layout Colors ----- */
		.juliet-post-date a, .juliet-post-date a:hover, .juliet-post-bullet, .juliet-cat a, .juliet-cat { color:<?php echo get_theme_mod( 'jul_date_cat_color'); ?>; }
		.juliet-post-comment-count a, .juliet-post-comment-count .juliet-post-bullet, .juliet-post-share-buttons a { color:<?php echo get_theme_mod( 'jul_comm_sm_color'); ?>; }
		.juliet-post-tags, .juliet-post-tags a { color:<?php echo get_theme_mod( 'jul_tag_color'); ?>; }
		
		/* ----- Gallery Colors ----- */
		<?php $gal_hex = get_theme_mod( 'jul_gal_overlay_bg_color');
		list($r, $g, $b) = sscanf($gal_hex, "#%02x%02x%02x"); ?>
		.juliet-gallery-img-overlay { background:rgba(<?php echo esc_attr($r); ?>, <?php echo esc_attr($g); ?>, <?php echo esc_attr($b); ?>, .8); }
		.juliet-gallery-title-border { border-color:<?php echo get_theme_mod( 'jul_gal_overlay_box_border_color'); ?>; }
		.juliet-gallery-title h2 { color:<?php echo get_theme_mod( 'jul_gal_overlay_text_color'); ?>; background-color:<?php echo get_theme_mod( 'jul_gal_overlay_box_bg_color'); ?>;}
		.juliet-gallery-title h2:hover { color:<?php echo get_theme_mod( 'jul_gal_overlay_text_hover_color'); ?>; }
		
		/* ----- Sidebar Colors ----- */
		/*Widget BG Color*/
		.widget-title, .juliet-custom-list-widget, .widget_recent_entries, .widget_recent_comments, .widget_meta, .widget_archive, .widget_categories, 
		.widget_pages, .widget_nav_menu, .woocommerce-product-search input[type="submit"], .woocommerce.widget_product_categories, .woocommerce.widget_layered_nav, 
		.woocommerce .widget_price_filter .ui-slider .ui-slider-range { background-color:<?php echo get_theme_mod( 'jul_widget_bg_color'); ?>; }
		#juliet-sidebar .bx-wrapper .bx-controls-direction a { color:<?php echo get_theme_mod( 'jul_widget_bg_color'); ?>; }
		
		/*Widget Border Color*/
		.juliet_about_widget:after, .juliet-latest-products-widget, .juliet-latest-product-item, .juliet_recent_posts_widget:after, .widget_text, .widget_calendar, 
		#juliet-sidebar #juliet-searchform input[type="text"], #wp-calendar caption, .widget_rss, .widget_rss li, #juliet-sidebar #juliet-searchform, .widget_tag_cloud,
		.juliet-side-image:before, .juliet-side-image:after, .juliet-about-img:after, .juliet-about-img:before, .woocommerce.widget_layered_nav_filters, 
		.woocommerce .widget_rating_filter, .woocommerce .widget_shopping_cart, .woocommerce-page .widget_shopping_cart, .woocommerce.widget_products, .woocommerce.widget_recent_reviews, 
		.woocommerce.widget_top_rated_products, .woocommerce.widget_recently_viewed_products, .woocommerce ul.product_list_widget li, .woocommerce-product-search input,
		.woocommerce.widget_product_tag_cloud, .woocommerce .widget_price_filter, .woocommerce .widget_shopping_cart .total, .woocommerce.widget_shopping_cart .total { border-color:<?php echo get_theme_mod( 'jul_widget_border_color'); ?>; }
		
		/*Widget Underline Color*/
		p.juliet-custom-list-title, .widget_recent_entries ul li, .widget_meta ul li, .widget_archive ul li, .widget_categories ul li, .widget_pages ul li,
		.widget_nav_menu ul li, .widget_rss li, .widget_recent_comments ul li, .woocommerce.widget_product_categories li, .woocommerce.widget_layered_nav ul li {  border-color:<?php echo get_theme_mod( 'jul_widget_underline_color'); ?>; }
		
		/*Social Media*/
		#juliet-sidebar .juliet-social-widget { background-color:<?php echo get_theme_mod( 'jul_widget_social_media_bg_color'); ?>; }
		#juliet-sidebar .juliet-social-widget a { color:<?php echo get_theme_mod( 'jul_widget_social_media_color' ); ?>; }
		#juliet-sidebar .juliet-social-widget a:hover { color:<?php echo get_theme_mod( 'jul_widget_social_media_hover_color' ); ?>; }
		
		/* Tag Cloud */
		.tagcloud a { background-color:<?php echo get_theme_mod( 'jul_widget_tag_bg_color'); ?>; color:<?php echo get_theme_mod( 'jul_widget_tag_color' ); ?>; }
		.tagcloud a:hover { background-color:<?php echo get_theme_mod( 'jul_widget_tag_bg_hover_color'); ?>; color:<?php echo get_theme_mod( 'jul_widget_tag_hover_color' ); ?>; }
		
		/*MC4WP*/
		.widget_mc4wp_form_widget { background-color:<?php echo get_theme_mod( 'jul_widget_mailchimp_bg_color'); ?>; }
		.mc4wp-form input[type="submit"] {  background-color:<?php echo get_theme_mod( 'jul_widget_mailchimp_submit_bg_color'); ?>; color:<?php echo get_theme_mod( 'jul_widget_mailchimp_submit_text_color' ); ?>; }

		/* ----- Footer Colors ----- */
		#juliet-footer, #juliet-instagram-footer .null-instagram-feed p.clear { background-color:<?php echo get_theme_mod( 'jul_footer_bg_color' ); ?>; }
		#juliet-footer-copyright, #juliet-instagram-footer .null-instagram-feed a { color:<?php echo get_theme_mod ( 'jul_footer_text_color' ); ?>; }
		#juliet-footer-copyright a, .ph-marketing a { color:<?php echo get_theme_mod( 'jul_footer_link_color' ); ?>; }
		#juliet-footer-copyright a:hover, .ph-marketing a:hover { color:<?php echo get_theme_mod( 'jul_footer_link_hover_color' ); ?>; }

		@media only screen and (max-width: 545px) { 
			.woocommerce div.product .woocommerce-tabs ul.tabs li.active:last-of-type {
				border-color:<?php echo get_theme_mod( 'jul_border_color'); ?>;
			}
		}	

	</style>
    <?php
}
