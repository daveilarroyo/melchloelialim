<?php
/**
 * Regsiter Settings & Sections of the Wordpress Customizer 
 *
 * @author Pix & Hue
 */

add_action( 'customize_register', 'juliet_register_theme_customizer' );

function juliet_register_theme_customizer( $wp_customize ) {

	/********************************************/
	/** Top Bar Colors Section 
	*********************************************/
	$wp_customize->add_section( 'juliet_top_bar_color_section' , array(
		'title'      => 'Top Bar Colors',
		'description'=> '',
		'priority'   => 200,
	) );

	$wp_customize->add_setting(
		'jul_top_bar_background_color',
		array(
			'default'     => '#EFECEA',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_top_bar_text_color',
		array(
			'default'     => '#313131',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_top_bar_text_hover_color',
		array(
			'default'     => '#E9C2B1',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_cart_icon_bg_color',
		array(
			'default'     => '#313131',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_cart_icon_count_color',
		array(
			'default'     => '#FFFFFF',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_cart_icon_count_bg_color',
		array(
			'default'     => '#A7A9AC',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	//Add Controls for Settings
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'top_bar_background_color',
			array(
			    'label'      => 'Top Menu Background Color',
			    'section'    => 'juliet_top_bar_color_section',
			    'settings'   => 'jul_top_bar_background_color',
			    'priority'   => 1
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'top_bar_text_color',
			array(
			    'label'      => 'Top Menu Text Color',
			    'section'    => 'juliet_top_bar_color_section',
			    'settings'   => 'jul_top_bar_text_color',
			    'priority'   => 2,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'top_bar_text_hover_color',
			array(
			    'label'      => 'Top Menu Text Hover Color',
			    'section'    => 'juliet_top_bar_color_section',
			    'settings'   => 'jul_top_bar_text_hover_color',
			    'priority'   => 3,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'cart_icon_bg_color',
			array(
			    'label'      => 'Cart Icon BG Color',
			    'section'    => 'juliet_top_bar_color_section',
			    'settings'   => 'jul_cart_icon_bg_color',
			    'priority'   => 4,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'cart_icon_count_color',
			array(
			    'label'      => 'Cart Icon Count Color',
			    'section'    => 'juliet_top_bar_color_section',
			    'settings'   => 'jul_cart_icon_count_color',
			    'priority'   => 5,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'cart_icon_count_bg_color',
			array(
			    'label'      => 'Cart Icon Count BG Color',
			    'section'    => 'juliet_top_bar_color_section',
			    'settings'   => 'jul_cart_icon_count_bg_color',
			    'priority'   => 6,
			)
		)
	);


	/********************************************/
	/** Main Menu Color Section
	*********************************************/
	$wp_customize->add_section( 'juliet_menu_color_section' , array(
		'title'      => 'Main Menu Colors',
		'description'=> '',
		'priority'   => 201,
	) );

	$wp_customize->add_setting(
		'jul_menu_background_color',
		array(
			'default'     => '#FFFFFF',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_menu_border_color',
		array(
			'default'     => '#E5E5E5',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_menu_text_color',
		array(
			'default'     => '#313131',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_menu_text_hover_color',
		array(
			'default'     => '#E9C2B1',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	
	//Add Controls for Settings
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'menu_background_color',
			array(
			    'label'      => 'Main Menu Background Color',
			    'section'    => 'juliet_menu_color_section',
			    'settings'   => 'jul_menu_background_color',
			    'priority'   => 1
			)
		)
	);


	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'menu_border_color',
			array(
			    'label'      => 'Main Menu Border Color',
			    'section'    => 'juliet_menu_color_section',
			    'settings'   => 'jul_menu_border_color',
			    'priority'   => 2,
			)
		)
	);


	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'menu_text_color',
			array(
			    'label'      => 'Main Menu Text Color',
			    'section'    => 'juliet_menu_color_section',
			    'settings'   => 'jul_menu_text_color',
			    'priority'   => 3,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'menu_hover_color',
			array(
			    'label'      => 'Main Menu Text Hover Color',
			    'section'    => 'juliet_menu_color_section',
			    'settings'   => 'jul_menu_text_hover_color',
			    'priority'   => 4,
			)
		)
	);

	/********************************************/
	/** Mobile Menu Colors Section 
	*********************************************/
	$wp_customize->add_section( 'juliet_mobile_menu_color_section' , array(
		'title'      => 'Mobile Menu Colors',
		'description'=> '',
		'priority'   => 202,
	) );

	$wp_customize->add_setting(
		'jul_mobile_menu_top_bar_color',
		array(
			'default'     => '#FFFFFF',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_mobile_menu_border_color',
		array(
			'default'     => '#E5E5E5',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_mobile_menu_icon_color',
		array(
			'default'     => '#EFECEA',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_mobile_menu_dropdown_bg_color',
		array(
			'default'     => '#FFFFFF',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);


	$wp_customize->add_setting(
		'jul_mobile_menu_text_color',
		array(
			'default'     => '#313131',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_mobile_menu_hover_text_color',
		array(
			'default'     => '#E9C2B1',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	
	//Add Controls for Settings
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'mobile_menu_top_bar_color',
			array(
			    'label'      => 'Mobile Menu Top Bar Color',
			    'section'    => 'juliet_mobile_menu_color_section',
			    'settings'   => 'jul_mobile_menu_top_bar_color',
			    'priority'   => 1,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'mobile_menu_border_color',
			array(
			    'label'      => 'Mobile Menu Border Color',
			    'section'    => 'juliet_mobile_menu_color_section',
			    'settings'   => 'jul_mobile_menu_border_color',
			    'priority'   => 2,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'mobile_menu_icon_color',
			array(
			    'label'      => 'Mobile Menu Icon Color',
			    'section'    => 'juliet_mobile_menu_color_section',
			    'settings'   => 'jul_mobile_menu_icon_color',
			    'priority'   => 3,
			)
		)
	);


	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'mobile_menu_dropdown_bg_color',
			array(
			    'label'      => 'Mobile Menu Dropdown BG Color',
			    'section'    => 'juliet_mobile_menu_color_section',
			    'settings'   => 'jul_mobile_menu_dropdown_bg_color',
			    'priority'   => 4,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'mobile_menu_text_color',
			array(
			    'label'      => 'Mobile Menu Text Color',
			    'section'    => 'juliet_mobile_menu_color_section',
			    'settings'   => 'jul_mobile_menu_text_color',
			    'priority'   => 5,
			)
		)
	);


	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'mobile_menu_hover_text_color',
			array(
			    'label'      => 'Mobile Menu Hover Text Color',
			    'section'    => 'juliet_mobile_menu_color_section',
			    'settings'   => 'jul_mobile_menu_hover_text_color',
			    'priority'   => 6,
			)
		)
	);

	/********************************************/
	/** General Colors
	*********************************************/
	$wp_customize->add_section( 'juliet_general_color_section' , array(
		'title'      => 'General Site Colors',
		'description'=> '',
		'priority'   => 203,
	) );

	$wp_customize->add_setting(
		'jul_gen_accent_color',
		array(
			'default'     => '#EFECEA',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_border_color',
		array(
			'default'     => '#E5E5E5',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_link_color',
		array(
			'default'     => '#E9C2B1',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);


	//Add Controls for Settings
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'gen_accent_color',
			array(
			    'label'      => 'Accent Color',
			    'section'    => 'juliet_general_color_section',
			    'settings'   => 'jul_gen_accent_color',
			    'priority'   => 1,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'border_color',
			array(
			    'label'      => 'Border Color',
			    'section'    => 'juliet_general_color_section',
			    'settings'   => 'jul_border_color',
			    'priority'   => 2,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'link_color',
			array(
			    'label'      => 'Link Color',
			    'section'    => 'juliet_general_color_section',
			    'settings'   => 'jul_link_color',
			    'priority'   => 3,
			)
		)
	);

	/********************************************/
	/** Theme Button Colors
	*********************************************/
	$wp_customize->add_section( 'juliet_button_color_section' , array(
		'title'      => 'Button Colors',
		'description'=> '',
		'priority'   => 204,
	) );

	
	$wp_customize->add_setting(
		'jul_pin_it_button_bg_color',
		array(
			'default'     => '#FFFFFF',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_pin_it_button_border_color',
		array(
			'default'     => '#FFFFFF',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_pin_it_button_text_color',
		array(
			'default'     => '#313131',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_pin_it_button_hover_color',
		array(
			'default'     => '#E9C2B1',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_back_to_top_color',
		array(
			'default'     => '#313131',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_back_to_top_bg_color',
		array(
			'default'     => '#FDFDFD',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	//Add Controls for Settings
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'pin_it_button_bg_color',
			array(
			    'label'      => 'Pin It Button BG Color',
			    'section'    => 'juliet_button_color_section',
			    'settings'   => 'jul_pin_it_button_bg_color',
			    'priority'   => 1,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'pin_it_button_border_color',
			array(
			    'label'      => 'Pin It Button Border Color',
			    'section'    => 'juliet_button_color_section',
			    'settings'   => 'jul_pin_it_button_border_color',
			    'priority'   => 2,
			)    
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'pin_it_button_text_color',
			array(
			    'label'      => 'Pin It Button Text Color',
			    'section'    => 'juliet_button_color_section',
			    'settings'   => 'jul_pin_it_button_text_color',
			    'priority'   => 3,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'pin_it_button_hover_color',
			array(
			    'label'      => 'Pin It Button Hover Color',
			    'section'    => 'juliet_button_color_section',
			    'settings'   => 'jul_pin_it_button_hover_color',
			    'priority'   => 4,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'back_to_top_color',
			array(
			    'label'      => 'Back to Top Button Color',
			    'section'    => 'juliet_button_color_section',
			    'settings'   => 'jul_back_to_top_color',
			    'priority'   => 5,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'back_to_top_bg_color',
			array(
			    'label'      => 'Back to Top BG Color',
			    'section'    => 'juliet_button_color_section',
			    'settings'   => 'jul_back_to_top_bg_color',
			    'priority'   => 6,
			)
		)
	);

	/********************************************/
	/** Featured Section Colors
	*********************************************/
	$wp_customize->add_section( 'juliet_featured_color_section' , array(
		'title'      => 'Featured Section Colors',
		'description'=> '',
		'priority'   => 205,
	) );

	$wp_customize->add_setting(
		'jul_feat_box_bg_color',
		array(
			'default'     => '#FFFFFF',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_feat_box_border_color',
		array(
			'default'     => '#FFFFFF',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_feat_box_text_color',
		array(
			'default'     => '#313131',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_feat_box_text_hover_color',
		array(
			'default'     => '#E9C2B1',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	//Add Controls for Settings
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'feat_box_bg_color',
			array(
			    'label'      => 'Featured Box BG Color',
			    'section'    => 'juliet_featured_color_section',
			    'settings'   => 'jul_feat_box_bg_color',
			    'priority'   => 1,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'feat_box_border_color',
			array(
			    'label'      => 'Featured Box Border Color',
			    'section'    => 'juliet_featured_color_section',
			    'settings'   => 'jul_feat_box_border_color',
			    'priority'   => 2,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'feat_box_text_color',
			array(
			    'label'      => 'Featured Box Text Color',
			    'section'    => 'juliet_featured_color_section',
			    'settings'   => 'jul_feat_box_text_color',
			    'priority'   => 3,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'feat_box_text_hover_color',
			array(
			    'label'      => 'Featured Box Text Hover Color',
			    'section'    => 'juliet_featured_color_section',
			    'settings'   => 'jul_feat_box_text_hover_color',
			    'priority'   => 4,
			)
		)
	);

	/********************************************/
	/** Blog & Block Layout
	*********************************************/
	$wp_customize->add_section( 'juliet_blog_color_section' , array(
		'title'      => 'Blog + Block Layout Colors',
		'description'=> '',
		'priority'   => 206,
	) );

	$wp_customize->add_setting(
		'jul_date_cat_color',
		array(
			'default'     => '#808080',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_comm_sm_color',
		array(
			'default'     => '#808080',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);


	$wp_customize->add_setting(
		'jul_tag_color',
		array(
			'default'     => '#808080',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	//Add Controls for Settings
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'date_cat_color',
			array(
			    'label'      => 'Date + Category Color',
			    'section'    => 'juliet_blog_color_section',
			    'settings'   => 'jul_date_cat_color',
			    'priority'   => 1,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'comm_sm_color',
			array(
			    'label'      => 'Comment + Social Media Color',
			    'section'    => 'juliet_blog_color_section',
			    'settings'   => 'jul_comm_sm_color',
			    'priority'   => 2,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'tag_color',
			array(
			    'label'      => 'Tag Color',
			    'section'    => 'juliet_blog_color_section',
			    'settings'   => 'jul_tag_color',
			    'priority'   => 3,
			)
		)
	);


	/********************************************/
	/** Gallery Colors
	*********************************************/
	$wp_customize->add_section( 'juliet_gallery_color_section' , array(
		'title'      => 'Gallery Layout Colors',
		'description'=> '',
		'priority'   => 207,
	) );


	$wp_customize->add_setting(
		'jul_gal_overlay_bg_color',
		array(
			'default'     => '#EFECEA',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_gal_overlay_box_bg_color',
		array(
			'default'     => '#FFFFFF',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_gal_overlay_box_border_color',
		array(
			'default'     => '#FFFFFF',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_gal_overlay_text_color',
		array(
			'default'     => '#313131',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);	

	$wp_customize->add_setting(
		'jul_gal_overlay_text_hover_color',
		array(
			'default'     => '#A7A9AC',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	//Add Controls for Settings
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'gal_overlay_bg_color',
			array(
			    'label'      => 'Rollover BG Color',
			    'section'    => 'juliet_gallery_color_section',
			    'settings'   => 'jul_gal_overlay_bg_color',
			    'priority'   => 1,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'gal_overlay_box_bg_color',
			array(
			    'label'      => 'Rollover Box BG Color',
			    'section'    => 'juliet_gallery_color_section',
			    'settings'   => 'jul_gal_overlay_box_bg_color',
			    'priority'   => 2,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'gal_overlay_box_border_color',
			array(
			    'label'      => 'Rollover Box Border Color',
			    'section'    => 'juliet_gallery_color_section',
			    'settings'   => 'jul_gal_overlay_box_border_color',
			    'priority'   => 3,
			)
		)
	);


	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'gal_overlay_text_color',
			array(
			    'label'      => 'Rollover Text Color',
			    'section'    => 'juliet_gallery_color_section',
			    'settings'   => 'jul_gal_overlay_text_color',
			    'priority'   => 4,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'gal_overlay_text_hover_color',
			array(
			    'label'      => 'Rollover Text Hover Color',
			    'section'    => 'juliet_gallery_color_section',
			    'settings'   => 'jul_gal_overlay_text_hover_color',
			    'priority'   => 5,
			)
		)
	);

	/********************************************/
	/** Sidebar Colors
	*********************************************/
	$wp_customize->add_section( 'juliet_sidebar_color_section' , array(
		'title'      => 'Sidebar Colors',
		'description'=> '',
		'priority'   => 208,
	) );

	$wp_customize->add_setting(
		'jul_widget_bg_color',
		array(
			'default'     => '#EFECEA',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);


	$wp_customize->add_setting(
		'jul_widget_border_color',
		array(
			'default'     => '#E5E5E5',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_widget_underline_color',
		array(
			'default'     => '#A7A9AC',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_widget_social_media_bg_color',
		array(
			'default'     => '#EFECEA',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_widget_social_media_color',
		array(
			'default'     => '#313131',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_widget_social_media_hover_color',
		array(
			'default'     => '#A7A9AC',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_widget_tag_bg_color',
		array(
			'default'     => '#E3E3E3',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_widget_tag_color',
		array(
			'default'     => '#313131',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_widget_tag_bg_hover_color',
		array(
			'default'     => '#EFECEA',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_widget_tag_hover_color',
		array(
			'default'     => '#FFFFFF',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_setting(
		'jul_widget_mailchimp_bg_color',
		array(
			'default'     => '#EFECEA',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_widget_mailchimp_submit_bg_color',
		array(
			'default'     => '#313131',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_widget_mailchimp_submit_text_color',
		array(
			'default'     => '#FFFFFF',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	//Add Controls for Settings
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'widget_bg_color',
			array(
			    'label'      => 'Widget Background Color',
			    'section'    => 'juliet_sidebar_color_section',
			    'settings'   => 'jul_widget_bg_color',
			    'priority'   => 1,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'widget_border_color',
			array(
			    'label'      => 'Widget Border Color',
			    'section'    => 'juliet_sidebar_color_section',
			    'settings'   => 'jul_widget_border_color',
			    'priority'   => 2,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'widget_underline_color',
			array(
			    'label'      => 'Widget Underline Color',
			    'section'    => 'juliet_sidebar_color_section',
			    'settings'   => 'jul_widget_underline_color',
			    'priority'   => 3,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'widget_social_media_bg_color',
			array(
			    'label'      => 'Social Media BG Color',
			    'section'    => 'juliet_sidebar_color_section',
			    'settings'   => 'jul_widget_social_media_bg_color',
			    'priority'   => 4,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'widget_social_media_color',
			array(
			    'label'      => 'Social Media Icon Color',
			    'section'    => 'juliet_sidebar_color_section',
			    'settings'   => 'jul_widget_social_media_color',
			    'priority'   => 5,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'widget_social_media_hover_color',
			array(
			    'label'      => 'Social Media Icon Hover Color',
			    'section'    => 'juliet_sidebar_color_section',
			    'settings'   => 'jul_widget_social_media_hover_color',
			    'priority'   => 6,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'widget_tag_bg_color',
			array(
			    'label'      => 'Tag Cloud BG Color',
			    'section'    => 'juliet_sidebar_color_section',
			    'settings'   => 'jul_widget_tag_bg_color',
			    'priority'   => 7,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'widget_tag_color',
			array(
			    'label'      => 'Tag Cloud Text Color',
			    'section'    => 'juliet_sidebar_color_section',
			    'settings'   => 'jul_widget_tag_color',
			    'priority'   => 8,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'widget_tag_bg_hover_color',
			array(
			    'label'      => 'Tag Cloud BG Hover Color',
			    'section'    => 'juliet_sidebar_color_section',
			    'settings'   => 'jul_widget_tag_bg_hover_color',
			    'priority'   => 9,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'widget_tag_hover_color',
			array(
			    'label'      => 'Tag Cloud Text Hover Color',
			    'section'    => 'juliet_sidebar_color_section',
			    'settings'   => 'jul_widget_tag_hover_color',
			    'priority'   => 10,
			)
		)
	);


	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'widget_mailchimp_bg_color',
			array(
			    'label'      => 'Mailchimp Background Color',
			    'section'    => 'juliet_sidebar_color_section',
			    'settings'   => 'jul_widget_mailchimp_bg_color',
			    'priority'   => 11,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'widget_mailchimp_submit_bg_color',
			array(
			    'label'      => 'Mailchimp Submit BG Color',
			    'section'    => 'juliet_sidebar_color_section',
			    'settings'   => 'jul_widget_mailchimp_submit_bg_color',
			    'priority'   => 12,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'widget_mailchimp_submit_text_color',
			array(
			    'label'      => 'Mailchimp Submit Text Color',
			    'section'    => 'juliet_sidebar_color_section',
			    'settings'   => 'jul_widget_mailchimp_submit_text_color',
			    'priority'   => 13,
			)
		)
	);

	/********************************************/
	/** Footer Colors
	*********************************************/
	$wp_customize->add_section( 'juliet_footer_color_section' , array(
		'title'      => 'Footer Colors',
		'description'=> '',
		'priority'   => 209,
	) );

	$wp_customize->add_setting(
		'jul_footer_bg_color',
		array(
			'default'     => '#EFECEA',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_footer_text_color',
		array(
			'default'     => '#313131',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_footer_link_color',
		array(
			'default'     => '#313131',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_setting(
		'jul_footer_link_hover_color',
		array(
			'default'     => '#A7A9AC',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	//Add Controls for Settings
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_bg_color',
			array(
			    'label'      => 'Footer Background Color',
			    'section'    => 'juliet_footer_color_section',
			    'settings'   => 'jul_footer_bg_color',
			    'priority'   => 1,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_text_color',
			array(
			    'label'      => 'Footer Text Color',
			    'section'    => 'juliet_footer_color_section',
			    'settings'   => 'jul_footer_text_color',
			    'priority'   => 2,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_link_color',
			array(
			    'label'      => 'Footer Link Color',
			    'section'    => 'juliet_footer_color_section',
			    'settings'   => 'jul_footer_link_color',
			    'priority'   => 3,
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_link_hover_color',
			array(
			    'label'      => 'Footer Link Hover Color',
			    'section'    => 'juliet_footer_color_section',
			    'settings'   => 'jul_footer_link_hover_color',
			    'priority'   => 4,
			)
		)
	);
}