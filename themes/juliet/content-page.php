<?php 

/**
* Template used for Single Pages 
* Used by: page.php & page-fullwidth.php
*/

//Theme Options
global $juliet_gen_theme_options;
$juliet_post_options = get_option('juliet_blog_options');
$juliet_gen_theme_options['hide_share_buttons'] = isset($juliet_post_options['juliet_blog_hide_share_buttons']) ? $juliet_post_options['juliet_blog_hide_share_buttons'] : false;
$juliet_gen_theme_options['hide_comm_count'] = isset($juliet_post_options['juliet_blog_hide_comment_count']) ? $juliet_post_options['juliet_blog_hide_comment_count'] : false; 
$juliet_gen_theme_options['aff_type'] =''; $juliet_gen_theme_options['aff_image']=''; $juliet_gen_theme_options['hide_post_excerpt'] = true; 
$juliet_gen_theme_options['enable_pinterest'] = isset($juliet_post_options['juliet_blog_enable_pinterest']) ? $juliet_post_options['juliet_blog_enable_pinterest'] : false;
$juliet_no_footer = $juliet_gen_theme_options['hide_share_buttons'] && ($juliet_gen_theme_options['hide_comm_count'] || !comments_open()) ? 'juliet-single-page-no-footer' : '';
$juliet_page_title = $juliet_gen_theme_options['page_title']; ?>


<article id="post-<?php the_ID(); ?>" <?php post_class($juliet_no_footer); ?>>

	<?php if($juliet_page_title) { ?>
		<div class="juliet-page-header">	
			<h1><?php the_title(); ?></h1>
		</div>
	<?php } ?>	

	<div class="juliet-page-content">	
		<?php if(has_post_thumbnail()) { ?>
			<div class="juliet-page-img">
				<?php if($juliet_gen_theme_options['enable_pinterest']) {
					echo juliet_page_pin_it_button(get_the_post_thumbnail($post->ID, 'juliet-full-thumb'), get_post_thumbnail_id()); 
				} else {
					the_post_thumbnail('juliet-full-thumb'); 
				} ?>
			</div>
		<?php } ?>
	
		<?php the_content(); ?>

		<?php wp_link_pages( array(
			'before' => '<div class="juliet-wp-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'juliet' ) . '</span>',
			'after' => '</div>',
		)); ?>

		<!--Display Page Footer -->
		<?php locate_template( array('inc/templates/blog_templates/post_footer.php'), true, true ); ?>

		<!--Display Comments-->
		<?php if(comments_open()) { ?>
			<h5 class="juliet-comments-title <?php echo esc_attr($juliet_no_footer); ?>"><?php esc_html_e('Comments', 'juliet'); ?></h5>
			<?php comments_template( '', true );  
		} ?>	

	</div>	

</article>