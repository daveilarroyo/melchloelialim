<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<meta charset="<?php esc_attr(bloginfo( 'charset' )); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="<?php echo esc_url('http://gmpg.org/xfn/11'); ?>" />
	<link rel="pingback" href="<?php esc_url(bloginfo( 'pingback_url' )); ?>" />

	<?php $juliet_general_options = get_option('juliet_general_theme_options');
	$juliet_shop_options = get_option('juliet_shop_options');

	if (!function_exists( 'wp_site_icon' ) || !has_site_icon()) { ?>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri() . '/images/defaults/pixandhue_icon.png'; ?>" />
	<?php } ?>
	
	<?php wp_head(); ?>

</head>	

<?php $juliet_site_logo = isset($juliet_general_options['juliet_site_logo']) ? $juliet_general_options['juliet_site_logo'] : false; 
$juliet_menu_position = isset($juliet_general_options['juliet_menu_position']) && $juliet_general_options['juliet_menu_position'] == true ? 'sticky-nav' : ''; 
$juliet_back_to_top = isset($juliet_general_options['juliet_back_to_top']) ? $juliet_general_options['juliet_back_to_top'] : false; 
$juliet_top_bar = isset($juliet_general_options['juliet_top_bar']) ? $juliet_general_options['juliet_top_bar'] : false; 
$juliet_cart_icon = isset($juliet_shop_options['juliet_enable_woo_cart_icon']) ? $juliet_shop_options['juliet_enable_woo_cart_icon'] : false; 
$juliet_disable_loader = isset($juliet_general_options['juliet_disable_loading_gif']) ? $juliet_general_options['juliet_disable_loading_gif'] : false; ?>

<body <?php body_class(); ?>>
	
	<?php if(!$juliet_disable_loader) { ?>
		<div class="juliet-loader"></div>
	<?php } ?>	
	<div id="juliet-main-container" >
		<div class="juliet-page-wrapper">

			<div class="juliet-header-wrapper <?php if($juliet_menu_position) { echo 'nav-fixed'; } ?>">

				<header id="juliet-header">
				
					<?php if($juliet_top_bar) { ?>

						<div id="juliet-top-bar">
							<div class="juliet-top-nav">
								<?php wp_nav_menu( array( 'theme_location' => 'top-nav' ) ); ?>
							</div>
							<div class="juliet-top-social">
								<?php juliet_top_bar_social_media(); ?>
							</div>	

							<?php if ( $juliet_cart_icon && in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) { ?>
								<div class="juliet-top-cart-icon">
									<?php $juliet_count = WC()->cart->cart_contents_count; ?>
									<a class="juliet-cart-count" href="<?php echo esc_url(WC()->cart->get_cart_url()); ?>" title="<?php esc_html_e( 'View your shopping cart', 'juliet' ); ?>"><?php if ( $juliet_count >= 0 ) echo '<span class="juliet-count-val">' . $juliet_count . '</span>'; ?></a>
								</div>	
							<?php } ?>	

							<div class="juliet-top-search-bar">
								<?php get_search_form(); ?>
							</div>	

						</div>	

					<?php } ?>

					<div class="juliet-container">
						<div class="juliet-site-logo">
							
							<!--Site Logo -->
							<?php if($juliet_site_logo) { 

								if(is_front_page()) { ?>
									<h1><a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo esc_url($juliet_site_logo); ?>" alt="<?php esc_attr(bloginfo( 'name' )); ?>" /></a></h1>
								<?php } else { ?>
									<h2><a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo esc_url($juliet_site_logo); ?>" alt="<?php esc_attr(bloginfo( 'name' )); ?>" /></a></h2>
								<?php }

							} else { 
								if(is_front_page()) { ?>
									<h1><a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo get_template_directory_uri() . '/images/defaults/juliet_logo.png'; ?>" alt="<?php esc_attr(bloginfo( 'name' )); ?>" /></a></h1>
								<?php } else { ?>	
									<h2><a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo get_template_directory_uri() . '/images/defaults/juliet_logo.png'; ?>" alt="<?php esc_attr(bloginfo( 'name' )); ?>" /></a></h2>
								<?php } 
							}?>

						</div>	
					</div>

					<!--Main Menu-->
					<div id="juliet-nav-wrapper" class="<?php echo esc_attr($juliet_menu_position); ?>">
						<?php wp_nav_menu( array('container' => false, 'theme_location' => 'main-menu', 'menu_class' => 'menu' ) ); ?>

						<?php if($juliet_top_bar) {

							if ( $juliet_cart_icon && in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) { ?>
								<div class="juliet-top-cart-icon juliet-sticky">
									<?php $juliet_count = WC()->cart->cart_contents_count; ?>
									<a class="juliet-cart-count" href="<?php echo esc_url(WC()->cart->get_cart_url()); ?>" title="<?php esc_html_e( 'View your shopping cart', 'juliet' ); ?>"><?php if ( $juliet_count >= 0 ) echo '<span class="juliet-count-val">' . $juliet_count . '</span>'; ?></a>
								</div>	
							<?php } ?>	

							<div class="juliet-sticky-social-media">
								<?php juliet_top_bar_social_media(); ?>
							</div>	
						<?php } ?>	

					</div>

					<!--Mobile Menu-->
					<div id="juliet-menu-mobile" class="<?php echo esc_attr($juliet_menu_position); ?>"></div>

				</header>
 
				<?php if($juliet_back_to_top) { ?>

					<a id='juliet_back_to_top' href='#'>
						<span class="fa-stack">
							<i class="fa fa-arrow-up" style=""></i>
						</span>
					</a>

				<?php } ?>		