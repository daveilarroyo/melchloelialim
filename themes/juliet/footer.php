<?php $juliet_options = get_option('juliet_general_theme_options');
$juliet_footer_text = isset($juliet_options['juliet_copyright_text']) ? $juliet_options['juliet_copyright_text'] : ''; ?>

			</div> <!--End #juliet-content-container-->

		</div> <!--End #page-wrapper-->

		<footer id="juliet-footer">
			
			<div id="juliet-instagram-footer">

				<?php if(is_active_sidebar('juliet-instagram-sidebar')) { ?>

					<div class="juliet-instagram-title"><?php esc_html_e('FOLLOW ME ON INSTAGRAM @MELCHLOELIALIM', 'juliet'); ?></div>

				<?php } ?>	

				<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('juliet-instagram-sidebar') ) ?>


			</div>	

			<div id="juliet-footer-copyright">

				<div class="juliet-container">

					<?php if(!isset($juliet_footer_text) || trim($juliet_footer_text) == '') { ?>

						<p class="ph-copyright">&copy; 2017 - Pix &amp; Hue. All Rights Reserved. <span class="ph-marketing">Designed &amp; Developed by <a href="<?php echo esc_url('http://pixandhue.com'); ?>">Pix &amp; Hue.</a></span></p>

					<?php } else { ?>

						<p class="ph-copyright">&copy; <?php echo date('Y',time());?> &mdash; <?php echo wp_kses_post($juliet_footer_text); ?></p>

					<?php } ?>

				</div>	
	 	
			</div>
			
		</footer>
		
	</div> <!--End #juliet-main-container-->

	<?php wp_footer(); ?>
</body>
</html>