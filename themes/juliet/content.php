<?php 
/**
* Template for a Single Blog Post
* Used by: single.php
*/

//Theme Options - Post Settings
global $juliet_gen_theme_options;
$juliet_post_options = get_option('juliet_post_options');
$juliet_hide_post_tags = isset($juliet_post_options['juliet_hide_tags']) ? $juliet_post_options['juliet_hide_tags'] : false;
$juliet_hide_pagination = isset($juliet_post_options['juliet_hide_pagination']) ? $juliet_post_options['juliet_hide_pagination'] : false;
$juliet_hide_related_posts = isset($juliet_post_options['juliet_hide_related_posts']) ? $juliet_post_options['juliet_hide_related_posts'] : false;
$juliet_hide_author = isset($juliet_post_options['juliet_hide_author']) ? $juliet_post_options['juliet_hide_author'] : false;
$juliet_top_comm = $juliet_hide_author && $juliet_hide_pagination ? 'juliet-top-comm' : '';
$juliet_comm_no_author = $juliet_hide_author ? 'juliet-comm-no-author' : '';
$juliet_gen_theme_options['hide_share_buttons'] = isset($juliet_post_options['juliet_hide_share_buttons']) ? $juliet_post_options['juliet_hide_share_buttons'] : false;
$juliet_gen_theme_options['hide_comm_count'] = isset($juliet_post_options['juliet_hide_comment_count']) ? $juliet_post_options['juliet_hide_comment_count'] : false;
$juliet_gen_theme_options['hide_feat_image'] = isset($juliet_post_options['juliet_hide_featured_image']) ? $juliet_post_options['juliet_hide_featured_image'] : false;
$juliet_gen_theme_options['enable_pinterest'] = isset($juliet_post_options['juliet_enable_pinterest']) ? $juliet_post_options['juliet_enable_pinterest'] : false;
$juliet_gen_theme_options['block_layout'] = false; $juliet_gen_theme_options['sidebar'] = false; 
$juliet_gen_theme_options['hide_post_excerpt'] = false; $juliet_gen_theme_options['aff_type'] = false; $juliet_gen_theme_options['aff_image'] = false;
$juliet_no_bottom_elem = $juliet_hide_author && $juliet_hide_pagination && ($juliet_gen_theme_options['hide_comm_count'] || !comments_open()) && $juliet_hide_related_posts ? 'juliet-no-bottom-elem' : '';
$juliet_no_footer = ($juliet_hide_post_tags || !has_tag()) && $juliet_gen_theme_options['hide_share_buttons'] && ($juliet_gen_theme_options['hide_comm_count'] || !comments_open()) ? 'juliet-no-single-footer' : '';

//Custom Post Types
$juliet_classes = get_post_class();
$juliet_wp_custom_type = in_array('tag-post-formats', $juliet_classes) ? true : false;
$juliet_custom_type = has_post_format('gallery') || has_post_format('video') || has_post_format('audio') ? true : false;
$juliet_gen_theme_options['post_img_type'] = (!$juliet_custom_type || ($juliet_custom_type && $juliet_wp_custom_type)) && ($juliet_gen_theme_options['hide_feat_image'] || !has_post_thumbnail()) ? 'juliet-no-post-img' : ''; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('single-juliet-post'); ?>>

	<!--Post Header-->
	<?php locate_template( array('inc/templates/blog_templates/post_header.php'), true, true); ?>

	<div class="juliet-post-content">	
		
		<!--Post Media -->
		<?php locate_template( array('inc/templates/blog_templates/post_media.php'), true, true); ?>

		<div class="juliet-post-entry-wrapper <?php echo esc_attr($juliet_no_bottom_elem); ?>">
			<div class="juliet-post-entry <?php echo esc_attr($juliet_gen_theme_options['post_img_type']) . ' ' . esc_attr($juliet_no_footer); ?>">
				<div class="juliet-post-content"><?php the_content(); ?></div>

				<?php wp_link_pages( array(
					'before' => '<div class="juliet-wp-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'juliet' ) . '</span>',
					'after' => '</div>',
				)); ?>

				<!--Display Tags (Optional)-->
				<?php if(!$juliet_hide_post_tags) {
					if(has_tag()) { ?>
						<div class="juliet-post-tags">
							<span class="juliet-post-tags-icon"><i class="fa fa-tags"></i></span><?php the_tags('', ', ', '<br />'); ?>
						</div>
					<?php }
				} ?>	

				<!--Display Post Footer -->
				<?php locate_template( array('inc/templates/blog_templates/post_footer.php'), true, true); ?>
			
			</div>
		</div>

		<!--Post Pagination (Optional)-->
		<?php if(!$juliet_hide_pagination) { ?>
			<div class="juliet-post-pagination">
				<?php if(get_previous_post()) { ?> 
					<span class="juliet-prev-post-link"><?php previous_post_link('%link', wp_kses(__('<i class="fa fa-angle-left"></i>Previous Post', 'juliet'), array('i' => array('class' => 'fa fa-angle-left')))); ?></span>
				<?php } ?>	

				<?php if(get_next_post()) { ?> 
					<span class="juliet-next-post-link"><?php next_post_link( '%link', wp_kses(__('Next Post<i class="fa fa-angle-right"></i>', 'juliet'), array('i' => array('class' => 'fa fa-angle-right')))); ?></span>
				<?php } ?>		
			</div>	
		<?php } ?>	

		<!--Display Author (Optional)-->
		<?php if(!$juliet_hide_author) { ?>
			<div class="juliet-post-author-box">
				<?php locate_template(array( 'inc/templates/about_author.php' ), true, true ); ?>
			</div>		
		<?php } ?>			

		<!--Display Comments (Optional)-->
		<?php if(comments_open()) { ?>
			<h5 class="juliet-comments-title <?php echo esc_attr($juliet_top_comm) . ' ' . esc_attr($juliet_comm_no_author); ?>"><?php esc_html_e('Comments', 'juliet'); ?></h5>
			<?php comments_template( '', true );  
		} ?>
		
		<!--Display Related Posts (Optional)-->
		<?php if(!$juliet_hide_related_posts) { ?>
			<div class="juliet-related-posts">
				<?php locate_template(array( 'inc/templates/related_posts.php' ), true, true ); ?>
			</div>	
		<?php } ?>
		
	</div>
	
</article>