<?php

/* Template Name: Full-Width */

//Theme Options
$juliet_meta = juliet_get_post_meta(get_the_ID(), array('juliet-promo-feature-type', 'juliet-standard-page-title'));
$juliet_below_feat_type = $juliet_meta['juliet-promo-feature-type'];
$juliet_below_feature = isset($juliet_below_feat_type) && ($juliet_below_feat_type == 'juliet-featured-slider' || $juliet_below_feat_type  == 'juliet-featured-image' || $juliet_below_feat_type == 'juliet-triple-images') ? 'juliet_below_feature' : ''; 
$juliet_gen_theme_options['page_title'] = isset($juliet_meta['juliet-standard-page-title']) && $juliet_meta['juliet-standard-page-title'] == 'juliet-title' ? true : false;
$juliet_gen_theme_options['sidebar'] = false;

get_header(); 

//Load the Featured Section (Single Image/Slider)
locate_template(array( 'inc/featured/featured.php' ), true, true ); ?>
	
</div>	<!-- End juliet-header-wrapper -->

<div id="juliet-content-container" class="<?php echo esc_attr($juliet_below_feature); ?>">

	<div class="juliet-container">

		<div id="juliet-content" class="juliet-full-width">	

			<?php if (have_posts()) { 

				while (have_posts()) { 

					the_post();
		
					get_template_part('content', 'page'); 	
				}
		
			} ?>

		</div> 	<!--End juliet-content -->

	</div> 	<!--End juliet-container -->	

	<?php get_footer(); ?>				