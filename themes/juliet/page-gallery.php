<?php

/* Template Name: Gallery Layout */

//Theme Options
$juliet_general_options = get_option('juliet_general_theme_options');
$juliet_left_sidebar = isset($juliet_general_options['juliet_left_sidebar']) ? $juliet_general_options['juliet_left_sidebar'] : false;

//Page Meta
$juliet_meta = juliet_get_post_meta(get_the_ID(), array('blog_cat','juliet-blog-page-width', 'juliet-standard-page-title','juliet-gal-overlay', 'juliet-promo-feature-type'));
$juliet_gen_theme_options['sidebar'] = isset($juliet_meta['juliet-blog-page-width']) && $juliet_meta['juliet-blog-page-width'] == 'juliet-sidebar' ? true : false;
$juliet_gen_theme_options['add_pin_it'] = isset($juliet_meta['juliet-gal-overlay']) && $juliet_meta['juliet-gal-overlay'] == 'juliet-gal-pin-button' ? true : false; 
$juliet_gen_theme_options['gal_overlay'] = isset($juliet_meta['juliet-gal-overlay']) && $juliet_meta['juliet-gal-overlay'] == 'juliet-gal-hover-overlay' ? true : false; 
$juliet_page_title = isset($juliet_meta['juliet-standard-page-title']) && $juliet_meta['juliet-standard-page-title'] == 'juliet-title' ? true : false;
$juliet_below_feat_type = $juliet_meta['juliet-promo-feature-type'];
$juliet_below_feature = isset($juliet_below_feat_type) && ($juliet_below_feat_type == 'juliet-featured-slider' || $juliet_below_feat_type  == 'juliet-featured-image' || $juliet_below_feat_type == 'juliet-triple-images') ? 'juliet_below_feature' : '';
$juliet_cat = isset($juliet_meta['blog_cat']) ? $juliet_meta['blog_cat'] : 'juliet-all-cats';
$juliet_cat = is_numeric($juliet_cat) ? get_category($juliet_cat)->slug : $juliet_cat;
$juliet_theme_data->count = 0;

//Layout Class Name
$juliet_content_classes;
if($juliet_gen_theme_options['sidebar'] && $juliet_left_sidebar) {
	$juliet_content_classes = 'juliet-default-width juliet-content-right';
} else if ($juliet_gen_theme_options['sidebar']) {
	$juliet_content_classes = 'juliet-default-width';
} else {
	$juliet_content_classes = 'juliet-full-width';
} ?>

<?php get_header(); 

//Load the Featured Section (Single Image/Slider)
locate_template(array( 'inc/featured/featured.php' ), true, true ); ?>
	
</div>	<!-- End juliet-header-wrapper -->

<div id="juliet-content-container" class="<?php echo esc_attr($juliet_below_feature); ?>">

	<div class="juliet-container">

		<div id="juliet-content" class="<?php echo esc_attr($juliet_content_classes); ?>">

			<?php if($juliet_page_title) { ?>		
				<h1 class="juliet-template-title"><?php the_title(); ?></h1>
			<?php } ?>	

			<div class="juliet-gallery">

				<?php if(is_front_page()) {
					$paged = (get_query_var('page')) ? absint(get_query_var('page')) : 1;
				} else {
					$paged = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;
				}

				if($juliet_cat == 'juliet-all-cats'){	
					$args = array(
						'post_type' => 'post',
						'paged' => $paged,
						'ignore_sticky_posts' => 1,
					);	
				} else {
					$args = array(
						'post_type' => 'post',
						'category_name' => $juliet_cat,
						'paged' => $paged,
						'ignore_sticky_posts' => 1,
					);	
				}

				// The Query
				$wp_query = new WP_Query( $args );

				// The Loop
				if ( $wp_query->have_posts() ) {
							
					while ( $wp_query->have_posts() ) {
								
						$wp_query->the_post();

						$juliet_theme_data->count++;

						get_template_part('content', 'gallery'); 
						
					} ?>
				
			</div>	<!--End Gallery-->	

					<?php juliet_pagination();
						
					wp_reset_postdata(); 

				} ?>

		</div> 	<!--End juliet-content -->

		<?php if($juliet_gen_theme_options['sidebar']) {
			get_sidebar(); 
		} ?>

	</div> 	<!--End juliet-container -->

	<?php get_footer(); ?>