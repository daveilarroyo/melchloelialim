<?php 
/**
* Template to display Related Posts
*  at bottom of single post.
*
* @author Pix & Hue
*/

$orig_post = $post;
global $post;

$juliet_post_options = get_option('juliet_post_options');
$juliet_enable_sidebar = isset($juliet_post_options['juliet_enable_post_sidebar']) ? $juliet_post_options['juliet_enable_post_sidebar'] : false;
$juliet_categories = get_the_category($post->ID);

if ($juliet_categories) {

	$category_ids = array();

	foreach($juliet_categories as $individual_category) $category_ids[] = $individual_category->term_id;

	if($juliet_enable_sidebar) {
		$args = array(
			'category__in'     => $category_ids,
			'post__not_in'     => array($post->ID),
			'posts_per_page'   => 3, // Number of related posts that will be shown.
			'ignore_sticky_posts' => 1,
			'orderby' => 'rand'
		);
	} 
	else {

		$args = array(
			'category__in'     => $category_ids,
			'post__not_in'     => array($post->ID),
			'posts_per_page'   => 4, // Number of related posts that will be shown.
			'ignore_sticky_posts' => 1,
			'orderby' => 'rand'
		);
	}
	
	$my_query = new wp_query( $args );
	if( $my_query->have_posts() ) { ?>
		<div class="juliet-post-related"><h4 class="juliet-related-title"><?php esc_html_e('You Might Also Like', 'juliet'); ?></h4>
			<div class="juliet-item-related-box">
				<?php while( $my_query->have_posts() ) {
					$my_query->the_post();?>
						<div class="juliet-item-related">
							<a href="<?php echo esc_url(get_permalink()); ?>">
								<?php if(has_post_thumbnail()) { 
									$image_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'juliet-misc-thumb' ); ?>
									<div class="juliet-item-related-img" style="background-image:url(<?php echo esc_url($image_src[0]); ?>)"></div>
								<?php } else { ?>
									<div class="juliet-item-related-img"></div>
								<?php } ?>		
							</a>
							<h3><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></h3>
						</div>
				<?php
				} ?>
			</div>
		</div>
	<?php }
}
$post = $orig_post;
wp_reset_postdata();

?>