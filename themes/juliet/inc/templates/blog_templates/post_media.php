<?php

/**
* The template used to display the post media (i.e. featured image) on page templates and single posts
* Used by: content-blog.php, content-block.php, and content.php
*
* @author Pix & Hue
*/

//Theme Options
global $juliet_gen_theme_options;
$juliet_post_img_type = $juliet_gen_theme_options['post_img_type'];
$juliet_block_layout = $juliet_gen_theme_options['block_layout'];
$juliet_enable_pinterest = $juliet_gen_theme_options['enable_pinterest'];
$juliet_hide_feat_image = $juliet_gen_theme_options['hide_feat_image'];
$allowed_html = array(
	'iframe' => array(
		'src'             => array(),
		'height'          => array(),
		'width'           => array(),
		'frameborder'     => array(),
		'allowfullscreen' => array(),
		'scrolling' 	  => array(),
		'seamless'		  => array(),
	),	
); ?>

<div class="juliet-post-media <?php echo esc_attr($juliet_post_img_type); ?>">	
	
	<?php 
	//Gallery Post
	$juliet_classes = get_post_class(); $wp_gallery = false;
	if(in_array('tag-gallery', $juliet_classes) || in_array('tag-tiled', $juliet_classes)) {
		$wp_gallery = true;
	}
	if(has_post_format('gallery') && !$juliet_block_layout && !$wp_gallery) {
		$images = get_post_meta( $post->ID, '_format_gallery_images', true ); 
		if($images) { ?>
			<div class="juliet-post-img">
				<div class="juliet-slide-container">
					<ul class="bxslider">	
					<?php foreach($images as $image) { 
						$the_image = wp_get_attachment_image_src( $image, 'juliet-full-thumb' ); 
						$image_id = juliet_get_the_attachment_id($the_image[0]);
						if($juliet_enable_pinterest) { ?>
							<li><?php echo wp_kses_post(juliet_page_pin_it_button('<img src="' . esc_url($the_image[0]) . '" alt="' . get_the_title() . '"/>', $image_id)); ?></li>
						<?php } else { ?> 
							<li><img src="<?php echo esc_url($the_image[0]); ?>" alt="<?php echo esc_attr(get_the_title()); ?>"/></li>
						<?php }
					} ?>
					</ul>
				</div>
			</div>
		<?php }
	} 
	
	//Video Post
	else if(has_post_format('video') && !$juliet_block_layout) { ?>
		<div class="juliet-post-img">
			<?php $sp_video = get_post_meta( $post->ID, '_format_video_embed', true );
			if(wp_oembed_get( $sp_video )) {
				echo wp_oembed_get($sp_video);
			}
			else {
				echo wp_kses(html_entity_decode($sp_video), $allowed_html); 
			} ?>
		</div>	
	<?php } 

	//Audio Post 
	else if(has_post_format('audio') && !$juliet_block_layout) { ?>
		<div class="juliet-post-img">
			<?php $sp_audio = get_post_meta( $post->ID, '_format_audio_embed', true ); 
			if(wp_oembed_get( $sp_audio )) {
				echo wp_oembed_get($sp_audio); 
			}	
			else {
				echo wp_kses(html_entity_decode($sp_audio), $allowed_html);
			}?>
		</div>

	<?php }

	else { 
		//Display Featured Image (optional)
		if(has_post_thumbnail()) { ?>
			<?php if(!$juliet_hide_feat_image) { ?>
				<div class="juliet-post-img">
					<?php if($juliet_enable_pinterest) {  
						if(is_single()) {
							echo juliet_page_pin_it_button(get_the_post_thumbnail($post->ID, 'juliet-full-thumb'), get_post_thumbnail_id()); 
						} else {
							echo juliet_page_pin_it_button('<a href="' . esc_url(get_permalink()) .'">' . get_the_post_thumbnail($post->ID, 'juliet-full-thumb') . '</a>', get_post_thumbnail_id()); 
						}	
					} else { ?>
						<a href="<?php echo esc_url(get_permalink()); ?>"><?php the_post_thumbnail('juliet-full-thumb'); ?></a>
					<?php } ?>
				</div>
			<?php } 
		} 
	} ?>

</div>	