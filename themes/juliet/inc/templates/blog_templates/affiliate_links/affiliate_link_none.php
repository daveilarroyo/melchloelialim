<?php 

/**
* The template for post content/post excerpt if no affiliate links are used
* Used by: content-blog.php & content-block.php
*
* @author Pix & Hue
*/

//Theme Options
global $juliet_gen_theme_options;
$juliet_hide_post_excerpt = $juliet_gen_theme_options['hide_post_excerpt'];
$juliet_no_post_footer = $juliet_gen_theme_options['no_post_footer'];
$juliet_excerpt_size = $juliet_gen_theme_options['excerpt_size'];
$juliet_layout = isset($juliet_gen_theme_options['layout_type']) ? $juliet_gen_theme_options['layout_type'] : '';

//Post Header
locate_template( array('inc/templates/blog_templates/post_header.php'), true, false); 

if(!$juliet_hide_post_excerpt || $juliet_layout == 'block') { 

	//Post Excerpt
	if($post->post_content!="") {

		if(has_excerpt()) { ?>
			<div class="juliet-post-excerpt <?php echo esc_attr($juliet_no_post_footer);?>"><?php echo juliet_custom_excerpt(get_the_excerpt(), $juliet_excerpt_size); ?></div>
		<?php } else { ?>	
			<div class="juliet-post-excerpt <?php echo esc_attr($juliet_no_post_footer);?>"><?php echo juliet_custom_excerpt(get_the_content(), $juliet_excerpt_size); ?></div>
		<?php }
	} 

	//Post Footer
	locate_template( array('inc/templates/blog_templates/post_footer.php'), true, false); 

} else { ?>
	
	<div class="juliet-post-single-content">
		<?php the_content(); ?>
	</div>	

	<?php wp_link_pages( array(
		'before' => '<div class="juliet-wp-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'juliet' ) . '</span>',
		'after' => '</div>',
	)); ?>

	<?php if(has_tag()) { ?>
		<div class="juliet-post-tags">
			<span class="juliet-post-tags-icon"><i class="fa fa-tags"></i></span><?php the_tags('', ', ', '<br />'); ?>
		</div>
	<?php }

	locate_template( array('inc/templates/blog_templates/post_footer.php'), true, false);  

} ?>