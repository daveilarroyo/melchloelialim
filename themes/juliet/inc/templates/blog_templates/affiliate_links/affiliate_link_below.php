<?php 

/**
* The template for affiliate links displayed below post excerpt
* Used by: content-blog.php & content-block.php
*
* @author Pix & Hue
*/

//Theme Options
global $juliet_gen_theme_options;
$juliet_options = get_option('juliet_general_theme_options');
$juliet_affiliate_text = isset($juliet_options['juliet_affiliate_text']) ? $juliet_options['juliet_affiliate_text'] : '';
$juliet_excerpt_size = $juliet_gen_theme_options['excerpt_size'];
$juliet_aff_code = $juliet_gen_theme_options['aff_code'];
$juliet_no_post_footer = $juliet_gen_theme_options['no_post_footer'];

//Post Header
locate_template(array('inc/templates/blog_templates/post_header.php'), true, false);

//Post Excerpt
if($post->post_content!="") { 
	if(has_excerpt()) { ?>
		<div class="juliet-post-excerpt <?php echo esc_attr($juliet_no_post_footer);?>"><?php echo juliet_custom_excerpt(get_the_excerpt(), $juliet_excerpt_size); ?></div>
	<?php } else { ?>
		<div class="juliet-post-excerpt <?php echo esc_attr($juliet_no_post_footer);?>"><?php echo juliet_custom_excerpt(get_the_content(), $juliet_excerpt_size); ?></div>
	<?php }
} 

//Post Footer
locate_template(array('inc/templates/blog_templates/post_footer.php'), true, false);

//Affiliate Code 
if($juliet_aff_code != "") { ?>
	
	<?php if(!isset($juliet_affiliate_text) || trim($juliet_affiliate_text) == '') { ?>
		<h6 class="juliet-affiliate-title"><?php esc_html_e( 'Shop the Look', 'juliet' ); ?></h6>
	<?php } else { ?>
		<h6 class="juliet-affiliate-title"><?php echo wp_kses_post($juliet_affiliate_text); ?></h6>
	<?php } ?>
	<div class="juliet-affiliate-widget">
		<?php echo html_entity_decode(juliet_filter_affiliate_code($juliet_aff_code), ENT_QUOTES); ?>	
	</div>
<?php } ?>	