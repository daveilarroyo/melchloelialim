<?php 

/**
* The template for affiliate links displayed next to post excerpts
* Used by: content-blog.php
*
* @author Pix & Hue
*/

//Theme Options
global $juliet_gen_theme_options;
$juliet_options = get_option('juliet_general_theme_options');
$juliet_affiliate_text = isset($juliet_options['juliet_affiliate_text']) ? $juliet_options['juliet_affiliate_text'] : '';
$juliet_no_post_footer = $juliet_gen_theme_options['no_post_footer'];
$juliet_side_excerpt_size = $juliet_gen_theme_options['side_excerpt_size'];
$juliet_aff_image = $juliet_gen_theme_options['aff_image'];
$juliet_sidebar = $juliet_gen_theme_options['sidebar'];
$juliet_aff_url = $juliet_gen_theme_options['aff_url'];
$juliet_link = $juliet_gen_theme_options['link'];


if($juliet_sidebar) { ?>
	<?php locate_template( array('inc/templates/blog_templates/post_header.php'), true, false);  ?>
<?php } else { ?>
	<div class="juliet-mobile-post-header">	
		<?php locate_template( array('inc/templates/blog_templates/post_header.php'), true, false); ?>
	</div>		
<?php } ?>

<div class="juliet-post-entry-left">

	<div class="juliet-post-entry-left-content">	

		<?php if(!$juliet_sidebar) { 
			locate_template( array('inc/templates/blog_templates/post_header.php'), true, false);
		}	

		if($post->post_content!="") { 
			if(has_excerpt()) { ?>
				<div class="juliet-post-excerpt <?php echo esc_attr($juliet_no_post_footer);?>"><?php echo esc_html(juliet_custom_excerpt(get_the_excerpt(), $juliet_side_excerpt_size)); ?></div>
			<?php } else { ?>
				<div class="juliet-post-excerpt <?php echo esc_attr($juliet_no_post_footer);?>"><?php echo esc_html(juliet_custom_excerpt(get_the_content(), $juliet_side_excerpt_size)); ?></div>
			<?php } ?>	
		<?php }
	
		locate_template( array('inc/templates/blog_templates/post_footer.php'), true, false);?>

	</div>	

</div>	

<div class="juliet-post-entry-right">

	<?php if(!isset($juliet_affiliate_text) || trim($juliet_affiliate_text) == '') { ?>
		<h6 class="juliet-affiliate-title"><?php esc_html_e( 'Shop the Look', 'juliet' ); ?></h6>
	<?php } else { ?>
		<h6 class="juliet-affiliate-title"><?php echo wp_kses_post($juliet_affiliate_text); ?></h6>
	<?php } ?>	

	<?php if($juliet_aff_url) { ?>
		<a href="<?php echo esc_url($juliet_aff_url); ?>" target="_blank">
		<?php $juliet_link = true;
	} 

	if($juliet_aff_image) { ?>	
		<div class="juliet-affiliate-item"><img src="<?php echo esc_url($juliet_aff_image); ?>" alt="<?php echo esc_attr(the_title()); ?>"></div>
	<?php } ?>
		
	<?php if($juliet_link) { ?>
		</a>
	<?php } ?>

</div>