<?php

/**
* The template used to display the post header on page templates and single posts
* Used by: content-blog.php, content-block.php, and content.php
*
* @author Pix & Hue
*/

//Theme Options
$juliet_post_options = is_single() ? get_option('juliet_post_options') : get_option('juliet_blog_options');
$juliet_hide_date_type = is_single() ? 'juliet_hide_date' : 'juliet_blog_hide_date';
$juliet_hide_cat_type = is_single() ? 'juliet_hide_cat' : 'juliet_blog_hide_cat';
$juliet_hide_post_date = isset($juliet_post_options[$juliet_hide_date_type]) ? $juliet_post_options[$juliet_hide_date_type] : false;
$juliet_hide_post_cat = isset($juliet_post_options[$juliet_hide_cat_type]) ? $juliet_post_options[$juliet_hide_cat_type] : false;  ?>

<div class="juliet-post-header">

	<?php if(is_single()) { ?>
		<h1><?php the_title(); ?></h1>
	<?php } else { ?>
		<h2><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></h2>
	<?php } ?>

	<!--Display Date (optional)-->
	<?php if(!$juliet_hide_post_date) {  ?>
		<div class="juliet-post-date"><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_time( get_option('date_format') ); ?></a></div>
	<?php } ?>

	<?php if(!$juliet_hide_post_date && !$juliet_hide_post_cat) { ?>
		<span class="juliet-post-bullet"><i class="fa fa-circle"></i></span>
	<?php } ?>

	<!--Display Categories (optional)-->
	<?php if(!$juliet_hide_post_cat && has_category()) {  ?>
		<div class="juliet-cat"><?php the_category(', ');?></div>
	<?php } ?>

</div>