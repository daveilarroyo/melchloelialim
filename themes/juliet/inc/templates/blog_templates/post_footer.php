<?php

/**
* The template used to display the post footer on page templates and single posts
* Used by: content-blog.php, content-block.php, and content.php
*
* @author Pix & Hue
*/

//Theme Options
global $juliet_gen_theme_options;
$juliet_sidebar = $juliet_gen_theme_options['sidebar'];
$juliet_hide_comm_count = $juliet_gen_theme_options['hide_comm_count'];
$juliet_hide_share_buttons = $juliet_gen_theme_options['hide_share_buttons'];
$juliet_hide_post_excerpt = $juliet_gen_theme_options['hide_post_excerpt'];
$juliet_aff_type = $juliet_gen_theme_options['aff_type'];
$juliet_aff_image = $juliet_gen_theme_options['aff_image'];
$juliet_layout = isset($juliet_gen_theme_options['layout_type']) ? $juliet_gen_theme_options['layout_type'] : '';
$juliet_single_footer_element = '';
if ((($juliet_hide_comm_count || !comments_open()) && !$juliet_hide_share_buttons) || ((!$juliet_hide_comm_count || comments_open()) && $juliet_hide_share_buttons)) {
	if(!$juliet_hide_comm_count && comments_open()) {
		$juliet_single_footer_element = 'juliet-single-element juliet-single-comm-count';
	} else {
		$juliet_single_footer_element = 'juliet-single-element';
	}
} 

if((!$juliet_hide_comm_count && comments_open()) || !$juliet_hide_share_buttons) { ?>

	<div class="juliet-post-footer <?php echo esc_attr($juliet_single_footer_element); ?>">

		<?php if (($juliet_single_footer_element == 'juliet-single-element' || $juliet_single_footer_element == 'juliet-single-element juliet-single-comm-count') || $juliet_sidebar && ($juliet_aff_type == 'juliet-aff-link') && $juliet_aff_image !="" && !$juliet_hide_post_excerpt)  { ?>
			
			<div class="juliet-post-excerpt-links <?php echo esc_attr($juliet_single_footer_element); ?>">
				<?php if(($juliet_layout != 'block' && !$juliet_hide_post_excerpt && $post->post_type != 'page' && !is_single()) || ($juliet_layout == 'block' && $post->post_type != 'page' && !is_single())) { ?>
					<span class="juliet-post-excerpt-border"><a href="<?php echo esc_url(get_permalink()); ?>" class="excerpt-more-link"><?php esc_html_e( 'continue reading', 'juliet' ); ?></a></span>	
				<?php } else if ($post->post_type == 'page' && !$juliet_hide_share_buttons && ($juliet_hide_post_excerpt || is_search())) { ?>
						<span class="juliet-share-text-border"><span class="juliet-share-text"><?php esc_html_e( 'Share This Page', 'juliet' ); ?></span></span>
				<?php } else if (!$juliet_hide_share_buttons && $post->post_type != 'page') { ?>
						<span class="juliet-share-text-border"><span class="juliet-share-text"><?php esc_html_e( 'Share This Post', 'juliet' ); ?></span></span>
				<?php } ?>
			</div>

			<!--Display Comment Count (optional)-->
			<?php if(!$juliet_hide_comm_count && comments_open()) { ?>
				<div class="juliet-post-comment-count <?php echo esc_attr($juliet_single_footer_element); ?>">
					<?php comments_popup_link( 
						wp_kses(__('Comments <span class="juliet-post-bullet"><i class="fa fa-circle"></i></span><span>0</span>', 'juliet'), array('span' => array('class' => 'juliet-post-bullet'), 'i' => array('class' => 'fa fa-circle'))), 
						wp_kses(__('Comment <span class="juliet-post-bullet"><i class="fa fa-circle"></i></span><span>1</span>', 'juliet'), array('span' => array('class' => 'juliet-post-bullet'), 'i' => array('class' => 'fa fa-circle'))), 
						wp_kses(__('Comments <span class="juliet-post-bullet"><i class="fa fa-circle"></i></span><span>%</span>', 'juliet'), array('span' => array('class' => 'juliet-post-bullet'), 'i' => array('class' => 'fa fa-circle'))), '', ''); ?>
				</div>		
			<?php } ?>	

			<!--Display Share Buttons (optional)-->
			<?php if(!$juliet_hide_share_buttons) { ?>
				<div class="juliet-post-share-buttons <?php echo esc_attr($juliet_single_footer_element); ?>"><?php echo wp_kses_post(juliet_share_buttons()); ?></div>
			<?php } 

		} 

		else { ?>
		
			<!--Display Comment Count (optional)-->
			<?php if(!$juliet_hide_comm_count && comments_open()) { ?>
				<div class="juliet-post-comment-count">
					<?php comments_popup_link( 
						wp_kses(__('Comments <span class="juliet-post-bullet"><i class="fa fa-circle"></i></span><span>0</span>', 'juliet'), array('span' => array('class' => 'juliet-post-bullet'), 'i' => array('class' => 'fa fa-circle'))), 
						wp_kses(__('Comment <span class="juliet-post-bullet"><i class="fa fa-circle"></i></span><span>1</span>', 'juliet'), array('span' => array('class' => 'juliet-post-bullet'), 'i' => array('class' => 'fa fa-circle'))), 
						wp_kses(__('Comments <span class="juliet-post-bullet"><i class="fa fa-circle"></i></span><span>%</span>', 'juliet'), array('span' => array('class' => 'juliet-post-bullet'), 'i' => array('class' => 'fa fa-circle'))), '', ''); ?>
				</div>		
			<?php } ?>	

			<div class="juliet-post-excerpt-links">
				<?php if(($juliet_layout != 'block' && !$juliet_hide_post_excerpt && !is_single() && $post->post_type != 'page') || ($juliet_layout == 'block' && $post->post_type != 'page' && !is_single())) { ?>
					<span class="juliet-post-excerpt-border">
						<a href="<?php echo esc_url(get_permalink()); ?>" class="excerpt-more-link"><?php esc_html_e( 'continue reading', 'juliet' ); ?></a>
					</span>		
				<?php } else { 
					if($post->post_type != 'post') { ?>
						<span class="juliet-share-text-border"><span class="juliet-share-text"><?php esc_html_e( 'Share This Page', 'juliet' ); ?></span></span>
					<?php } else { ?>
						<span class="juliet-share-text-border"><span class="juliet-share-text"><?php esc_html_e( 'Share This Post', 'juliet' ); ?></span></span>
					<?php }
			 	} ?>
			 </div>	

			 <!--Display Comment Count (Mobile)-->
			<?php if(!$juliet_hide_comm_count && comments_open()) { ?>
				<div class="juliet-post-comment-count juliet-post-mobile-count">
					<?php comments_popup_link( 
						wp_kses(__('Comments <span class="juliet-post-bullet"><i class="fa fa-circle"></i></span><span>0</span>', 'juliet'), array('span' => array('class' => 'juliet-post-bullet'), 'i' => array('class' => 'fa fa-circle'))), 
						wp_kses(__('Comment <span class="juliet-post-bullet"><i class="fa fa-circle"></i></span><span>1</span>', 'juliet'), array('span' => array('class' => 'juliet-post-bullet'), 'i' => array('class' => 'fa fa-circle'))), 
						wp_kses(__('Comments <span class="juliet-post-bullet"><i class="fa fa-circle"></i></span><span>%</span>', 'juliet'), array('span' => array('class' => 'juliet-post-bullet'), 'i' => array('class' => 'fa fa-circle'))), '', ''); ?>
				</div>		
			<?php } ?>	

			<!--Display Share Buttons (optional)-->
			<?php if(!$juliet_hide_share_buttons) { ?>
				<div class="juliet-post-share-buttons"><?php echo wp_kses_post(juliet_share_buttons()); ?></div>
			<?php } 

		} ?>		
	</div>	
<?php } else if ((!$juliet_hide_post_excerpt && ($juliet_hide_comm_count || !comments_open()) && $juliet_hide_share_buttons) || $juliet_layout == 'block') { 

	if(!is_single() && $post->post_type == 'post') { ?>
		<div class="juliet-post-footer juliet-no-footer-links">
			<div class="juliet-post-excerpt-links">
				<span class="juliet-post-excerpt-border"><a href="<?php echo esc_url(get_permalink()); ?>" class="excerpt-more-link"><?php esc_html_e( 'continue reading', 'juliet' ); ?></a></span>
			</div>	
		</div>	
	<?php }	
} ?>




