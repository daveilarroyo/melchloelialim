<?php
/**
* Template for a Featured Image
* Used by featured.php 
* Based on selection in page meta box.
*
* @author Pix & Hue
*/

//Get the Feat Image Feature ID & related meta data
$juliet_link = false;
$juliet_meta = juliet_get_post_meta(get_the_ID(), array('juliet-feat-image-select')); 
$juliet_feat_id = $juliet_meta['juliet-feat-image-select'];
$juliet_feat_img = juliet_get_post_meta($juliet_feat_id, array('juliet_feat_url', 'juliet_feat_title', 'juliet_feat_link', 'juliet_feat_open_option', 'juliet_feat_background_color', ));

/*******************************************************************************
* Image Link
*******************************************************************************/
if($juliet_feat_img['juliet_feat_link']) { ?>
	<a class="juliet-feat-img-link" href="<?php echo esc_url($juliet_feat_img['juliet_feat_link']); ?>" <?php if($juliet_feat_img['juliet_feat_open_option'] == 'new') { ?> target="_blank" <?php } ?>>
	<?php $juliet_link = true;
}

	/*******************************************************************************
	* Render the Feat Image - Set the Background Image or Color (prioritzing image)
	*******************************************************************************/
	if($juliet_feat_img['juliet_feat_url']) { ?>
		<div class="juliet-feat-item" style="background-image:url(<?php echo esc_url($juliet_feat_img['juliet_feat_url']); ?>)">
	<?php } 
	else if($juliet_feat_img['juliet_feat_background_color']) { ?>
		<div class="juliet-feat-item" style="background:<?php echo esc_attr($juliet_feat_img['juliet_feat_background_color']); ?>">
	<?php }
	else { ?>
		<div class="juliet-feat-item">
	<?php } ?>
		<div class="juliet-feat-img-text-overlay">
			<?php if($juliet_feat_img['juliet_feat_title']) {  ?>
				<div class="juliet-feat-title-border">
					<h2 class="juliet-feat-img-top-title"><?php echo esc_html($juliet_feat_img['juliet_feat_title']); ?></h2>
				</div>
			<?php } ?>
		</div>	
	</div>

<?php if($juliet_link) { ?>
	</a>
<?php } ?>