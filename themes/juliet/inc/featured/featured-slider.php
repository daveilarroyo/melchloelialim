<?php
/**
* Template for a Featured Slider
* Used by featured.php 
* Based on selection in Page meta box.
*
* @author Pix & Hue
*/

//Get the Feat Slider ID & related meta data
$juliet_meta = juliet_get_post_meta(get_the_ID(), array('juliet-feat-slider-select')); 
$juliet_feat_id = $juliet_meta['juliet-feat-slider-select'];
$juliet_feat_slider = juliet_get_post_meta($juliet_feat_id, array('juliet_feat_content_cat', 'juliet_feat_slider_num', 'juliet_feat_slider_type', 'juliet_feat_post_ids')); 
$juliet_featured_cat = isset($juliet_feat_slider['juliet_feat_content_cat']) ? $juliet_feat_slider['juliet_feat_content_cat'] : 'juliet-slider-all-cats';
$juliet_featured_cat = is_numeric($juliet_featured_cat) ? get_category($juliet_featured_cat)->slug : $juliet_featured_cat; ?>

<div class="juliet-slide-container">
	<div class="bxslider">

		<?php $args; $juliet_featured_posts;

		if($juliet_feat_slider['juliet_feat_slider_type'] == 'cat-slides') {
			if($juliet_featured_cat == 'juliet-slider-all-cats') { 
				$args = array('posts_per_page' => $juliet_feat_slider['juliet_feat_slider_num'], 'ignore_sticky_posts' => 1 );
			} else {
				$args = array( 'category_name' => $juliet_featured_cat, 'posts_per_page' => $juliet_feat_slider['juliet_feat_slider_num'], 'ignore_sticky_posts' => 1 );
			}
		} else if ($juliet_feat_slider['juliet_feat_slider_type'] == 'man-slides') {
			$juliet_featured_posts = explode(',', $juliet_feat_slider['juliet_feat_post_ids']);
			$args = array('post_type' => array('post', 'page'), 'post__in' => $juliet_featured_posts, 'orderby' => 'post__in', 'ignore_sticky_posts' => 1 );
		}

		$feat_query = new WP_Query( $args );

		if ($feat_query->have_posts()) { 

			while ($feat_query->have_posts()) {

			 	$feat_query->the_post(); ?>

			 	<div class="juliet-feat-slide">
					<a class="juliet-feat-img-link" href="<?php echo esc_url(get_permalink()); ?>">
						<?php $post_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'juliet-featured-thumb' ); 
						if($post_image) { ?>
							<div class="juliet-feat-item" style="background-image:url(<?php echo esc_url($post_image[0]); ?>)">
						<?php } else { ?>
							<div class="juliet-feat-item">
						<?php } 
								if(get_the_title() != '') { ?>		
									<div class="juliet-feat-img-text-overlay">
										<div class="juliet-feat-title-border">
											<h2 class="juliet-feat-img-top-title"><?php the_title(); ?></h2>
										</div>	
									</div>	
								<?php } ?>
							</div>
					</a>
				</div>		

			<?php } ?>	
			
		<?php } ?>

		<?php wp_reset_postdata(); ?>

	</div>
</div>