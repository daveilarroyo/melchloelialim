<?php 
/**
* Template for Triple Images
* Based on selection in Page meta box.
*
* @author Pix & Hue
*/

//Get the ID for the Set of Triple Images
$juliet_meta = juliet_get_post_meta(get_the_ID(), array('juliet-trip-images-select'));
$juliet_images_id = $juliet_meta['juliet-trip-images-select'];
$juliet_triple_images = juliet_get_post_meta($juliet_images_id, array('juliet_url_one', 'juliet_link_one', 'juliet_open_option_one',
														'juliet_url_two', 'juliet_link_two', 'juliet_open_option_two',
														'juliet_url_three','juliet_link_three', 'juliet_open_option_three'));

//Create the three images
$juliet_number;
for ($i = 1; $i <= 3; $i++) {

	$juliet_link = false;

	if($i == 1) {
		$juliet_number = 'one';
	} else if($i == 2) {
		$juliet_number = 'two';
	} else if($i == 3) {
		$juliet_number = 'three';
	}

	//Set the Link 
	if($juliet_triple_images['juliet_link_' . $juliet_number]) { ?>
		<a class="juliet-triple-images-link" <?php if($juliet_triple_images['juliet_open_option_' . $juliet_number] == 'new') { ?> target="_blank" <?php } ?> href="<?php echo esc_url($juliet_triple_images['juliet_link_' . $juliet_number]); ?>">
		<?php $juliet_link = true;
	}

	//Make the Box
	if($juliet_triple_images['juliet_url_' . $juliet_number]) { ?>
		<div class="juliet-the-triple-image <?php  echo esc_attr($juliet_number); ?>" style="background-image:url(<?php echo esc_url($juliet_triple_images['juliet_url_' . $juliet_number]); ?>)">
	<?php } else { ?>
		<div class="juliet-the-triple-image <?php echo esc_attr($juliet_number); ?>">
	 <?php } ?>
	</div>

	<?php //If Link, close the <a> tag
	if($juliet_link) { ?>
		</a>
	<?php } 
} ?>