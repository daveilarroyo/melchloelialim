<?php

/**
* Based on selection in Page meta box
* Calls Featured Image file or Featured Slider file.
*
* @author Pix & Hue
*/

//Get the Promo Feature Type
$juliet_post_id = is_home()?4:get_the_ID();
$juliet_meta = juliet_get_post_meta($juliet_post_id, array('juliet-promo-feature-type'));
$juliet_feature_type = $juliet_meta['juliet-promo-feature-type'];

if($juliet_feature_type == 'juliet-featured-image' || $juliet_feature_type == 'juliet-featured-slider' || $juliet_feature_type == 'juliet-triple-images') { ?>

	<div id="juliet-featured-section" class="<?php echo esc_attr($juliet_feature_type); ?>">

			<?php if($juliet_feature_type === 'juliet-featured-image') {
				locate_template(array( 'inc/featured/featured-image.php' ), true, true ); 
			} else if($juliet_feature_type === 'juliet-featured-slider') {
				locate_template(array( 'inc/featured/featured-slider.php' ), true, true ); 
			} else if($juliet_feature_type === 'juliet-triple-images') {
				locate_template(array( 'inc/featured/featured-triple-images.php' ), true, true ); 
			} ?>

	</div>

<?php } ?>