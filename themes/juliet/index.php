<?php 

//Theme Options
$juliet_general_options = get_option('juliet_general_theme_options');
$juliet_left_sidebar = isset($juliet_general_options['juliet_left_sidebar']) ? $juliet_general_options['juliet_left_sidebar'] : false;
$juliet_content_classes = $juliet_left_sidebar ? 'juliet-default-width juliet-content-right' : 'juliet-default-width';
$juliet_gen_theme_options['sidebar'] = true; ?>

<?php get_header(); 

//Load the Featured Section (Single Image/Slider)
locate_template(array( 'inc/featured/featured.php' ), true, true ); ?>




</div>	<!-- End juliet-header-wrapper -->

<div id="juliet-content-container">

	<div class="juliet-container">

		<div id="juliet-content" class="<?php echo esc_attr($juliet_content_classes); ?>">	

			<?php if (have_posts()) {

				while (have_posts()) {

				 	the_post();
					
					get_template_part('content', 'blog'); 
					
					if(!$wp_query->current_post)
						get_template_part('content', 'featured'); 
				}
				
				//Pagination 
				juliet_pagination();	
		
			} ?>

		</div> 	<!--End juliet-content -->

		<?php get_sidebar(); ?>		
	
	</div> 	<!--End juliet-container -->
		
	<?php get_footer(); ?>				