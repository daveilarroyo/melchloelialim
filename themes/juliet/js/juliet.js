/**
 * This file contains the main JavaScript functionality for the theme.
 * 
 * @author Pix & Hue
 */

jQuery(document).ready(function($) {

	"use strict";

	/* -- Init Theme Scripts  -- */
	juliet_back_to_top();
	juliet_back_button_show_hide();
	juliet_gallery_animation();
	juliet_loading_gif();
	juliet_pin_button_animation('.juliet-pin-it-wrapper');

	/* -- Sticky Menu  -- */
  	$("#juliet-nav-wrapper.sticky-nav").sticky({
  		topSpacing:0,
  		zIndex:9999,
  	});

  	$("#juliet-menu-mobile.sticky-nav").sticky({
  		topSpacing:0,
  		zIndex:9999,
  	});

	/* -- Mobile Menu  -- */
	$('#juliet-nav-wrapper .menu').slicknav({
		prependTo:'#juliet-menu-mobile',
		label:'',
		allowParentLinks: true,
		afterOpen: function(trigger) { 
			var mobile_menu = $('#juliet-menu-mobile-sticky-wrapper').children(':first');
			mobile_menu.addClass(' juliet-mobile-sticky-open');
		},
		afterClose: function(trigger) {
			var mobile_menu_wrapper = $('#juliet-menu-mobile-sticky-wrapper');
			mobile_menu_wrapper.children(':first').removeClass(' juliet-mobile-sticky-open');
			mobile_menu_wrapper.css('height', '49px');
		}
	});	

	/* -- BXslider - Feat Slider  -- */
	var one_feat_slide = ($('#juliet-featured-section .bxslider').children().length < 2);
	$('#juliet-featured-section .bxslider').bxSlider({
		auto: (!one_feat_slide),
		controls: (!one_feat_slide),
		pager: false,
		pause: 5000,
		touchEnabled: (!one_feat_slide),
		slideMargin: 5,
		nextText: '<i class="fa fa-angle-right"></i>',
		prevText: '<i class="fa fa-angle-left"></i>',
		onSliderLoad: function(){
			$(".juliet-slide-container").css("visibility", "visible");
		}
	});

	/* -- Gallery Post - Slider  -- */
	$('.juliet-post-img .bxslider').bxSlider({
	  adaptiveHeight: true,
	  auto: true,  
	  captions: true,
	  mode: 'fade',
	  pager: false,
	  nextText: '<i class="fa fa-angle-right"></i>',
	  prevText: '<i class="fa fa-angle-left"></i>',
	  onSliderLoad: function(){
			$(".juliet-slide-container").css("visibility", "visible");
		}
	});

	/* -- Instagram Widget in Sidebar - Slider -- */
	var one_insta_slide = ($('#juliet-sidebar .instagram-pics').children().length < 2);
	$('#juliet-sidebar .instagram-pics').bxSlider({
		adaptiveHeight: false,
		auto: (!one_insta_slide),
		captions: false, 
		controls: (!one_insta_slide),
		touchEnabled: (!one_insta_slide),
		mode: 'fade',
		pager: false,
		slideMargin: 3,
		nextText: '<i class="fa fa-angle-right"></i>',
	  	prevText: '<i class="fa fa-angle-left"></i>',
	});

	/* -- Initialize Fit Vids -- */
	$(".juliet-container").fitVids();
});


/*************************************
 * PIN IT Button Animation
**************************************/	
var $j = jQuery.noConflict();

function juliet_pin_button_animation(container) {
	"use strict";	

	var touch_screen = 'ontouchstart' in window;

	$j(container).each(function()  {

		var wp_caption_bool = $j(this).children(':first').hasClass('wp-caption');
		if(wp_caption_bool) {
			var wp_caption = $j(this).children(':first');
		}

		if((touch_screen && (!($j(this).children(':first').is('a'))) && !wp_caption_bool) || (touch_screen && wp_caption_bool && !wp_caption.children(':first').is('a')) || !touch_screen) {

			if($j(this).children(':first').hasClass('alignleft') || $j(this).children(':first').children(':first').hasClass('alignleft')) {
				$j(this).addClass(' juliet-left-pin-image');
			} else if ($j(this).children(':first').hasClass('alignright') || $j(this).children(':first').children(':first').hasClass('alignright')) {
				$j(this).addClass(' juliet-right-pin-image');
			} else if ($j(this).children(':first').hasClass('aligncenter') || $j(this).children(':first').children(':first').hasClass('aligncenter')) {
				$j(this).wrap('<div class="juliet-center-pin-wrapper"></div>');
			} else if($j(this).children(':first').hasClass('alignnone') || $j(this).children(':first').children(':first').hasClass('alignnone')) {
				$j(this).addClass(' juliet-alignnone-pin-image');
			} 

			if($j(this).children(':first').hasClass('wp-caption')) {
				$j(this).addClass(' juliet-pin-image-caption');
				$j(this).children(':first').children(':first').next().children(':first').remove();
			}

			$j(this).on('mouseover', function() {
				$j(this).find('.juliet-pin-it-border').fadeIn();
			});

			$j(this).on('mouseleave', function() {
				$j(this).find('.juliet-pin-it-border').fadeOut();
			});
		}	

	});  
}

/*************************************
 * Gallery Animation
**************************************/	
function juliet_gallery_animation() {
	"use strict";	

	var touch_screen = 'ontouchstart' in window;

	$j('.juliet-gallery-item').on('mouseover', function() {

		var gal_overlay = $j(this).find('.juliet-gallery-img-overlay.juliet-rollover');
		var gal_title = gal_overlay.find('.juliet-gallery-title-border');

		gal_overlay.fadeIn(500).css('display', 'table'); 
		gal_title.slideDown(600);
	});	

	$j('.juliet-gallery-item').on('mouseleave', function() {
		var gal_overlay = $j(this).find('.juliet-gallery-img-overlay.juliet-rollover');
		var gal_title = gal_overlay.find('.juliet-gallery-title-border');

		gal_overlay.fadeOut('slow');
		gal_title.fadeOut(700);  

	});

	$j('.juliet-gallery-item').on('click', function() {

		if(touch_screen) {
			var gal_overlay = $j(this).find('.juliet-gallery-img-overlay.juliet-rollover');
			var gal_title = gal_overlay.find('.juliet-gallery-title-border');
		
			if(gal_overlay.is(':visible')) {
				gal_overlay.fadeOut('slow');
				gal_title.fadeOut(700);  
			}
		}	
	});	
}

/*************************************
 * Loading Gif
**************************************/	
function juliet_loading_gif() {
	"use strict";

	$j(window).load(function() {
		$j(".juliet-loader").fadeOut("slow");
	});	
}

/*************************************
 * Back To Top Button
**************************************/	
function juliet_back_button_show_hide(){
	"use strict";

	$j(window).scroll(function () {
		var b = $j(this).scrollTop();
		var c = $j(this).height();
		var d;
		if (b > 0) { d = b + c / 2; } else { d = 1; }
		if (d < 1e3) { juliet_to_top_button("off"); } else { juliet_to_top_button("on"); }
	});
}

function juliet_to_top_button(a) {
	"use strict";

	var b = $j("#juliet_back_to_top");
	b.removeClass("off on");
	if (a === "on") { b.addClass("on"); } else { b.addClass("off"); }
}

//Scroll to Top
function juliet_back_to_top(){
	"use strict";

	$j(document).on('click','#juliet_back_to_top',function(e){
		e.preventDefault();
		
		$j('body,html').animate({scrollTop: 0}, $j(window).scrollTop()/3, 'linear');
	});
}