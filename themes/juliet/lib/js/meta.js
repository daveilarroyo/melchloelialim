/*
 * This files contains the js functionality for page meta boxes.
 *
 * @author Pix & Hue
 */

jQuery(document).ready(function($) {

	"use strict";

	/*****************************************************************************
 	* Init Meta Functions
	***************************************************************************/
	var std_page_template = {
		template_pages: ['default'],
		to_show:['#juliet-page-feature', '#juliet-standard-layout', '#juliet-pin-settings'],
		to_hide: ['#juliet-blog-selection', '#juliet-block-selection', '#juliet-gal-selection'],
	};

	var full_width_template = {
		template_pages: ['page-fullwidth.php'],
		to_show:['#juliet-page-feature', '#juliet-standard-layout', '#juliet-pin-settings'],
		to_hide: ['#juliet-blog-selection', '#juliet-block-selection', '#juliet-gal-selection'],
	};

	var blog_template = {
		template_pages: ['page-blog.php'],
		to_show:['#juliet-page-feature', '#juliet-blog-selection'],
		to_hide: ['#juliet-block-selection', '#juliet-gal-selection', '#juliet-pin-settings'],
	};

	var block_template = {
		template_pages: ['page-block.php'],
		to_show:['#juliet-block-selection', '#juliet-page-feature'],
		to_hide: ['#juliet-blog-selection', '#juliet-gal-selection', '#juliet-pin-settings'],
	};

	var gal_template = {
		template_pages: ['page-gallery.php'],
		to_show:['#juliet-blog-selection', '#juliet-gal-selection', '#juliet-page-feature'],
		to_hide: ['#juliet-block-selection', '#juliet-pin-settings'],
	};

	var feature_wrapper_id = '#juliet-post-meta-box',
		upload_button = '.juliet-upload-button',
		delete_upload_button = '.juliet-remove-upload-button';	

	juliet_meta_intro_feature_scripts();
	juliet_post_meta_animate_scripts();
	juliet_meta_animate_options(std_page_template);
	juliet_meta_animate_options(full_width_template);
	juliet_meta_animate_options(blog_template);
	juliet_meta_animate_options(block_template);
	juliet_meta_animate_options(gal_template);
	juliet_hide_custom_categories();

	//Add Event Handlers - Images (Upload & Delete)
	$(feature_wrapper_id).delegate(upload_button, 'click', juliet_upload_meta_media);
	$(feature_wrapper_id).delegate(delete_upload_button, 'click', juliet_delete_uploaded_meta_img);
	juliet_meta_media_is_deleted();
		
});		

/*****************************************************************************
	* Meta Box Page Animate Feature Options
****************************************************************************/
var $j = jQuery.noConflict();	

function juliet_meta_intro_feature_scripts() {

	"use strict";

	var promo_type = '#juliet-promo-feature-type',
		feat_img_select = '#juliet-feat-image-select',
		feat_slider_select = '#juliet-feat-slider-select',
		trip_image_select = '#juliet-trip-images-select';

	//On Load
	if($j(promo_type).val() == 'juliet-featured-image') {
		$j(feat_img_select).parent().show();
		$j(feat_slider_select).parent().hide();
		$j(trip_image_select).parent().hide();
	} else if($j(promo_type).val() == 'juliet-featured-slider') {
		$j(feat_slider_select).parent().show();
		$j(feat_img_select).parent().hide();	
		$j(trip_image_select).parent().hide();	
	} else if($j(promo_type).val() == 'juliet-triple-images') {
		$j(trip_image_select).parent().show();	
		$j(feat_slider_select).parent().hide();
		$j(feat_img_select).parent().hide();		
	} else {
		$j(feat_img_select).parent().hide();
		$j(feat_slider_select).parent().hide();
		$j(trip_image_select).parent().hide();	
	}

	//On Change
	$j(promo_type).on('change', function() {
		if($j(this).val() === 'juliet-featured-image') {
			$j(feat_slider_select).parent().hide();
			$j(trip_image_select).parent().hide();
			$j(feat_img_select).parent().fadeIn();			
		} else if($j(this).val() === 'juliet-featured-slider') {
			$j(feat_img_select).parent().hide();
			$j(trip_image_select).parent().hide();
			$j(feat_slider_select).parent().fadeIn();
		} else if($j(this).val() === 'juliet-triple-images') {
			$j(feat_slider_select).parent().hide();
			$j(feat_img_select).parent().hide();
			$j(trip_image_select).parent().fadeIn();
		} else if($j(this).val() === 'juliet-no-intro-feature') {
			$j(feat_img_select).parent().fadeOut();
			$j(feat_slider_select).parent().fadeOut();
			$j(trip_image_select).parent().fadeOut();
		}
	});
}

/*****************************************************************************
	* Meta Box Post Animate Feature Options
****************************************************************************/
function juliet_post_meta_animate_scripts() {

	"use strict";

	var aff_type = '#juliet-affiliate-type',
		aff_link = '#juliet-affiliate-content',
		aff_code = '#juliet-affiliate-code-section';

	//On Load
	if($j(aff_type).val() == 'juliet-aff-link') {
		$j(aff_code).hide();
		$j(aff_link).show();
	} else if($j(aff_type).val() == 'juliet-aff-code') {
		$j(aff_link).hide();	
		$j(aff_code).show();
	} else {
		$j(aff_link).hide();
		$j(aff_code).hide();
	}

	//On Change
	$j(aff_type).on('change', function() {
		if($j(this).val() === 'juliet-aff-link') {
			$j(aff_code).hide();
			$j(aff_link).fadeIn();
		} else if ($j(this).val() === 'juliet-aff-code') {
			$j(aff_link).hide();	
			$j(aff_code).fadeIn();
		} else if ($j(this).val() === 'juliet-no-intro-feature') {
			$j(aff_link).hide();
			$j(aff_code).hide();
		}
	});	
}	


/*****************************************************************************
	* Animate Meta Box Based on Selected Layout
****************************************************************************/
function juliet_meta_animate_options( page_template ) {

	"use strict";

	//On Load
	if($j('#page_template').val() === page_template.template_pages[0]) {
		$j.each(page_template.to_hide, function(i, value) {
			$j(page_template.to_hide[i]).hide();
		});

		$j.each(page_template.to_show, function(i, value) {
			$j(page_template.to_show[i]).fadeIn();
		});
	}

	//On Change 
	$j('#page_template').on('change', function() {
		if($j(this).val() === page_template.template_pages[0]) {
			$j.each(page_template.to_hide, function(i, value) {
				$j(page_template.to_hide[i]).hide();	
			});
			$j.each(page_template.to_show, function(i, value) {
				$j(page_template.to_show[i]).fadeIn();
			});
		}
	});		
}

/*****************************************************************************
 	* IMAGES - Use WP Media Uploader to Select Images for Promo Features
***************************************************************************/
function juliet_upload_meta_media(event) {

	"use strict";

	event.preventDefault();

	var uploadMedia;
	var	upload_button = $j(this),
		url_input = upload_button.prev(),
		image_wrapper = upload_button.next(),
		image = image_wrapper.children(':first'),
		remove_button = image.next();

	$j(remove_button).css('display', 'none');

	if(uploadMedia) {
		uploadMedia.open();
		return;
	}

	uploadMedia = wp.media.frames.file_frame = wp.media({
			title: 'Select Image',
			button: {
			text: 'Select Image'
		}, 
		multiple: false, 
	});

	uploadMedia.on('select', function() {
			var selection = uploadMedia.state().get('selection').first().toJSON();
			url_input.val(selection.url);

			image_wrapper.css('display', 'block');

			juliet_resize_meta_img(selection.url, image, upload_button, remove_button);
	});

	uploadMedia.open();

}

/*****************************************************************************
 * IMAGES - Get & Post Resized Image 
***************************************************************************/
function juliet_resize_meta_img(img_url, image, button, remove_button) {

	"use strict";

	var data =  {
		action: 'juliet_resize_img',
		image: img_url,
		height: 250,
		width: 250,
	}	

	$j.ajax({
		type: 'POST',
		url: ajaxurl, 
		data: data, 
	}).done(function(response) {
		button.css('display', 'none');
		image.attr('src', response);
		$j(remove_button).css('display', 'block');
	});
}	

/*****************************************************************************
	* IMAGES - Delete Promo Feature Image 
***************************************************************************/	
function juliet_delete_uploaded_meta_img() {

	"use strict";
	
	var img_wrapper = $j(this).parent(),
		form = img_wrapper.parent().parent().parent(),
		img_wrapper_img_url = img_wrapper.children(':first'),
		upload_button = img_wrapper.prev(),
		img_url = upload_button.prev();
	
	img_wrapper.css('display', 'none');
	upload_button.css('display', 'inline-block');

	img_wrapper_img_url.attr('src', '');
	img_url.val('');
}

/*****************************************************************************
 * IMAGES - Selected Image is Deleted from the Media Library
***************************************************************************/
function juliet_meta_media_is_deleted() {

	"use strict";

	//Reset the Values if Selected Image is Deleted from the Media Library		
	$j('.upload_img_preview').each(function() {
		if($j(this).attr('src') == '') {
			var img_wrapper = $j(this).parent();
			var upload_button = img_wrapper.prev();
			var img_url = upload_button.prev();

			upload_button.css('display', 'block');
			img_url.val('');
		}	
	});
}

/*****************************************************************************
 * Do not display Custom Theme Categories in post pag
***************************************************************************/
function juliet_hide_custom_categories() {
	$j("#categorychecklist").find("label:contains(' juliet_cp_feat_slider')").parent().addClass('juliet_customcategory');
	$j("#categorychecklist").find("label:contains(' juliet_cp_feature_image')").parent().addClass('juliet_customcategory');
	$j("#categorychecklist").find("label:contains(' juliet_cp_triple_images')").parent().addClass('juliet_customcategory');
}
