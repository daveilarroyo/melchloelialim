/*
 * This files contains the js functionality for the theme options 
 * contained within the admin menu page.
 *
 * @author Pix & Hue
 */

jQuery(document).ready(function($) {

	"use strict";

 	/*****************************************************************************
 	* Init Options Functions
	***************************************************************************/
	$(document).ready(function() {

		var toggle_checkbox_on = '.checkbox-toggle-on',
			toggle_checkbox_off = '.checkbox-toggle-off',
			options_nav = '#juliet_options_nav a',
			upload_btn = '.juliet-option-upload-button',
			remove_upload = '.juliet-option-remove-upload-button',
			submit_btn = '#juliet-options-form';

		juliet_style_option_boxes();
		juliet_option_save_reminder();

		//Bind Events to Functions
		$(options_nav).bind('click', juliet_options_tabs);
		$(upload_btn).bind('click', juliet_upload_option_images);
		$(remove_upload).bind('click', juliet_delete_option_images);
		$(toggle_checkbox_on).bind('click', juliet_toggle_on);
		$(toggle_checkbox_off).bind('click', juliet_toggle_off);
		$(submit_btn).bind('submit', juliet_save_theme_options);
	});
	 	
 	/*****************************************************************************
 	* Theme Options - Tab Functionality
	***************************************************************************/
	function juliet_options_tabs() {
 		
 		$('#juliet-options-error-message').fadeOut();
		$('#juliet-options-success-message').fadeOut();

		$(this).parent().addClass('active-li');
 		$(this).parent().siblings().removeClass('active-li');
 		
 		var currentAttrValue = $(this).parent().attr('id');

 		$('#juliet_options_container #' + currentAttrValue).fadeIn().siblings().hide();
 		$('#juliet_options_container #' + currentAttrValue).addClass('active').siblings().removeClass('active'); 
	} 

	/*****************************************************************************
 	* Theme Options - Upload Image
	***************************************************************************/
	function juliet_upload_option_images(event) {
		
		event.preventDefault();

		var uploadMedia,
			upload_button = $(this),
			url_input = upload_button.prev(),
			image_wrapper = upload_button.next(),
			image = image_wrapper.children(':first'),
			remove_button = image.next();

		var image_type = upload_button.prev().attr('id'),
		height = 100, 
		width = 400;

		if(image_type == 'juliet_site_favicon') {
			height = 20;
			width = 20;
		}
	
		if(uploadMedia) {
			uploadMedia.open();
			return;
		}

		uploadMedia = wp.media.frames.file_frame = wp.media({
  			title: 'Select Image',
  			button: {
  			text: 'Select Image'
			}, 
			multiple: false 
		});

		uploadMedia.on('select', function() {
  			
  			var selection = uploadMedia.state().get('selection').first().toJSON();
  			url_input.val(selection.url);
			image_wrapper.css('display', 'block');
			juliet_resize_option_img(selection.url, image, upload_button, remove_button, height, width);
			
			$('#juliet-options-save-reminder').fadeIn();
			$('#juliet-options-success-message').hide();

		});

		uploadMedia.open();
	}

	/*****************************************************************************
 	* Theme Options - Delete Image
	***************************************************************************/
	function juliet_delete_option_images() {

		var img_wrapper = $(this).parent(),
		img_wrapper_img_url = img_wrapper.children(':first'),
		upload_button = img_wrapper.prev(),
		img_url = upload_button.prev();

		var save_msg = $('#juliet-options-success-message'),
			reminder_msg = $('#juliet-options-save-reminder');

		img_wrapper.css('display', 'none');
		upload_button.css('display', 'inline-block');
		img_wrapper_img_url.attr('src', '');
		img_url.val('');
		reminder_msg.fadeIn();
		save_msg.hide();
	}

	/*****************************************************************************
 	* Theme Options - Get & Post Resized Image 
	***************************************************************************/
	function juliet_resize_option_img(img_url, image, button, remove_button, height, width) {

		var data =  {
			action: 'juliet_resize_img',
			image: img_url,
			height: height,
			width: width,
		}	

		$.ajax({
			type: 'POST',
			url: ajaxurl, 
			data: data, 
		}).done(function(response) {
			button.css('display', 'none');
			image.attr('src', response);
			$(remove_button).css('display', 'block');
		});
	}	
	
	/*****************************************************************************
 	* Button Switch (On/Off) Functionality 
	***************************************************************************/
	//Style Buttons
	function juliet_style_option_boxes()  {

		var juliet_option_on = $('.juliet-option-checkbox');
		
		$(juliet_option_on).each(function() {
			if ($(this).is(' :checked')) {
				$(this).next().css('background-color', '#3c7e3a');
				$(this).next().text('ON');
				$(this).next().next().css('background-color', 'lightgrey');
				$(this).next().next().text('');
			}
		});
	}

	//On Button - Toggle
	function juliet_toggle_on(event) {

		var checkbox = $(this).prev();
		var toggle_on = $(this);
		var toggle_off = $(toggle_on).next();

		$('#juliet-options-success-message').hide();
		$('#juliet-options-save-reminder').fadeIn();

		if($(checkbox).is(' :checked')) {
			$(checkbox).removeAttr('checked');
			$(this).animate({backgroundColor: 'lightgrey'}, 500)
			$(toggle_off).animate({backgroundColor: '#932528'}, 500)
			$(toggle_on).text('');
			$(toggle_off).text('OFF')
		} else {
			$(checkbox).attr('checked', ' :checked');
			$(this).animate({backgroundColor: '#3c7e3a'}, 500)
			$(toggle_off).animate({backgroundColor: 'lightgrey'}, 500)
			$(toggle_on).text('ON');
			$(toggle_off).text('')
		}

		event.preventDefault();	
	}

	//Off Button - Toggle
	function juliet_toggle_off(event) {

		var toggle_on = $(this).prev(),
		checkbox = $(toggle_on).prev();

		$('#juliet-options-success-message').hide();
		$('#juliet-options-save-reminder').fadeIn();
		
		if($(checkbox).is(' :checked')) {
			$(checkbox).removeAttr('checked');
			$(toggle_on).animate({backgroundColor: 'lightgrey'}, 500);
			$(this).animate({backgroundColor: '#932528'}, 500);
			$(toggle_on).text('');
			$(this).text('OFF');
		} else {
			$(checkbox).attr('checked', ' :checked');
			$(toggle_on).animate({backgroundColor: '#3c7e3a'}, 500);
			$(this).animate({backgroundColor: 'lightgrey'}, 500);
			$(toggle_on).text('ON');
			$(this).text('');
		}

		event.preventDefault();
	}	

	/*****************************************************************************
 	* Display Reminder to Save Settings 
	***************************************************************************/
	function juliet_option_save_reminder() {

		var form = '#juliet_options_container',
			reminder_msg = $('#juliet-options-save-reminder'),
			success_msg = reminder_msg.next();

		$(form + ' :input').on('change input', function() {
			success_msg.hide();
			reminder_msg.fadeIn();
		});
	}	

		
 	/*****************************************************************************
 	* Theme Options - Save Changes
	***************************************************************************/
	var arrayOfData = [];
	
	function juliet_save_theme_options(event) {

		event.preventDefault();

		$('#juliet_options_footer .options-loader').css('display', 'block');
		$('.juliet_options_overlay').css('display', 'block');
		$('#juliet-options-error-message').hide();
		$('#juliet-options-save-reminder').hide();
		$('#juliet-options-success-message').hide();
		$('#juliet_options_footer .button').prop('disabled', true);

		var data =  $(this).serialize(),
			social_media_index = data.indexOf("option_page=juliet_social_media_options"),
			blog_options_index = data.indexOf("option_page=juliet_blog_options"),
			archive_options_index = data.indexOf("option_page=juliet_archive_options"),
			post_options_index = data.indexOf("option_page=juliet_post_options"),
			shop_options_index = data.indexOf("option_page=juliet_shop_options");
		
		arrayOfData[0] = data.substring(0, social_media_index);
		arrayOfData[1] = data.substring(social_media_index, blog_options_index);
		arrayOfData[2] = data.substring(blog_options_index, archive_options_index);
		arrayOfData[3] = data.substring(archive_options_index, post_options_index);
		arrayOfData[4] = data.substring(post_options_index, shop_options_index);
		arrayOfData[5] = data.substring(shop_options_index);

		for(var i = 0; i < arrayOfData.length; i++) {
			data = arrayOfData[i];

			$.post('options.php', data).error(
			function() {
				$('#juliet_options_footer .options-loader').css('display', 'none');
				$('.juliet_options_overlay').fadeOut();
				$('#juliet-options-error-message').fadeIn(400);
				$('#juliet_options_footer .button').prop('disabled', false);
			}).success(function() {
				$('#juliet_options_footer .options-loader').css('display', 'none');
				$('.juliet_options_overlay').fadeOut();
				$('#juliet-options-success-message').fadeIn(400);
				$('#juliet_options_footer .button').prop('disabled', false);
				console.log("success!");
			});
		}
	}
});