/*
 * This files contains the js functionality for interacting with 
 * custom pages (theme submenu pages) to add/delete/update theme features.
 *
 * @author Pix & Hue
 */

jQuery(document).ready(function($) {

	"use strict";

	/*****************************************************************************
 	* Init Promo Feature Functions
	***************************************************************************/	
	juliet_promo_bind_handlers(); 
	juliet_dialog_add();
	juliet_dialog_delete();
	juliet_item_save_reminder();
	juliet_check_image_values(); 
	juliet_media_is_deleted();
	juliet_load_feat_slider_type();

	//Initialize Wordpress Color Picker
	var color_options = {
		change: function(event, ui) {
			var form = $(this).parents('.juliet-custom-feature-form'),
			reminder_msg = $(form).find('.juliet-custom-save-reminder'),
			success_msg = reminder_msg.next();
		
			success_msg.hide();
			reminder_msg.fadeIn();
		},
	}	

	$('.juliet-color-picker').wpColorPicker(color_options);
	
	/*****************************************************************************
 	* Bind Event Handlers:
 		Promo Feature - Animate, Add, Update & Delete Functions 
 		WP Media Uploader - Upload & Delete Images
	***************************************************************************/
	function juliet_promo_bind_handlers() {

		var feature_wrapper_id = '.juliet-custom-features',
			feature_wrapper_header = '.juliet-custom-feature-header';

		var change_slider_type = '#juliet_feat_slider_type',
			form = '.juliet-custom-feature-form',
			color_picker = '.wp-picker-container';
		
		//The Buttons
		var	add_button = '.juliet-custom-feature-add-button',
			update_button = '.juliet-custom-feature-update',
			delete_button = '.juliet-custom-feature-delete',
			upload_button = '.juliet-upload-button',
			delete_upload_button = '.juliet-remove-upload-button';

		//Add Event Handlers - Promo Features (Animate, Add, Update & Delete)
		$(feature_wrapper_id).delegate(feature_wrapper_header, 'click', juliet_animate_promo_feature);
		$(add_button).bind('click', juliet_add_promo_feature);
		$(feature_wrapper_id).delegate(update_button, 'click', juliet_update_promo_feature);
		$(feature_wrapper_id).delegate(delete_button, 'click', juliet_dialog_delete_actions);
		$(form).find(change_slider_type).each(function() {
			$(this).on('change', juliet_change_feat_slider_type);	
		});

		//Add Event Handlers - Images (Upload & Delete)
		$(feature_wrapper_id).delegate(upload_button, 'click', juliet_upload_media);
		$(feature_wrapper_id).delegate(delete_upload_button, 'click', juliet_delete_uploaded_img);
	}	

	/*****************************************************************************
 	* Function Set: Dialog Boxes
 	*
 	* Description: Add & Delete Dialog Boxes 
	******************************************************************************/

	/*****************************************************************************
 	* Dialog Box - Add New Item
	***************************************************************************/
	function juliet_dialog_add() {

		$('.juliet-add-custom-feature').dialog({
			autoOpen: false,
			modal: true,
			title: "Add New Item",
			closeText: "x",
			dialogClass: "juliet_options_dialog",
			resizable: false,
		});

		$('.juliet-create-custom-feature').on('click', function() {
			$('.juliet-add-custom-feature').dialog('open');
		});
	}
		
	/*****************************************************************************
 	* Dialog Box - Delete Item
	***************************************************************************/	
	function juliet_dialog_delete() {

		$('.juliet-custom-delete-box').dialog({
			autoOpen: false,
			modal: true,
			title: "Delete Item",
			closeText: "x",
			dialogClass: "juliet_options_dialog",
			resizable: false,
		});
	}
	
	/*****************************************************************************
 	* Dialog Box - Delete - Cancel & Delete Actions
	***************************************************************************/	
	function juliet_dialog_delete_actions() {

		var cancel_button = '.juliet-custom-cancel-delete',
			delete_button = '.juliet-custom-confirm-delete',
			form_to_delete;
		
		$('.juliet-custom-delete-box').dialog('open');

		form_to_delete = $(this).parent().next();
		
		$(cancel_button).on('click', function() {
			$('.juliet-custom-delete-box').dialog('close');
			return;
		});	

		$(delete_button).on('click', function() {
			$('.juliet-custom-delete-box').dialog('close');
			juliet_delete_promo_feature(form_to_delete);
		});
	}
	
	/*****************************************************************************
 	* Function Set: Promo Features 
 	*
 	* Description: Animate, Add, Update, & Delete Functions
	******************************************************************************/

	/*****************************************************************************
 	* Animate Promo Feature Set - Slide Up/Down Elements 
	***************************************************************************/	
	function juliet_animate_promo_feature(event) {
		
		var delete_button = '.juliet-custom-feature-delete',
			form = $(this).next(),
			animate_triangle = $(this).children(':first');
	
		if($(event.target).is(delete_button)) {
			event.preventDefault();
			return;
		}

		form.slideToggle();
		animate_triangle.toggleClass('expanded', 800).toggleClass('collapsed', 800);
	}

	/*****************************************************************************
 	* Add New Promo Feature Set
	***************************************************************************/
	function juliet_add_promo_feature() {

		var feature_cat = $('.juliet-custom-feature-type').val(),
			feature_cat_id = $('.juliet-custom-feature-id').val(),
			feature_wrapper_id = $('.juliet-custom-feature-items'),
			add_nonce = $('#juliet-custom-page-nonce').val();	
		
		var data =  {
			action: 'juliet_add_instance',
			name: $('.juliet-custom-feature-name').val(),
			cat_name: feature_cat,
			cat_id: feature_cat_id,
			nonce: add_nonce, 
		}

		var color_options = {
			change: function(event, ui) {
				var form = $(this).parents('.juliet-custom-feature-form'),
				reminder_msg = $(form).find('.juliet-custom-save-reminder'),
				success_msg = reminder_msg.next();
			
				success_msg.hide();
				reminder_msg.fadeIn();
			},
		}		

		$.ajax({
			type: 'POST',
			url: ajaxurl,
			data: data,
			dataType: 'text'
		}).done(function(response) {
			feature_wrapper_id.prepend(response);
			juliet_item_save_reminder();
			$('.juliet-no-custom-features').css('display', 'none');
			$('.juliet-color-picker').wpColorPicker(color_options);
			$('#juliet_feat_slider_type').on('change', juliet_change_feat_slider_type);

		});

		$('.juliet-add-custom-feature').dialog('close');
		$('.juliet-custom-feature-name').val('');
	}
		
	/*****************************************************************************
 	* Update Promo Feature Set 
	***************************************************************************/
	function juliet_update_promo_feature() {

		var form = $(this).parent().parent(),
			success_msg = $(form).find('.footer').children(':first').next().next().next(),
			reminder_msg = success_msg.prev(),
			error_msg = success_msg.next(),
			gif = reminder_msg.prev(),
			nonce = $('#juliet-custom-page-nonce').val(),
			data = form.serialize() + '&nonce=' + nonce +'&action=juliet_update_instance';

		$(gif).css('display', 'block');

		$.ajax({
			type: 'POST',
			url: ajaxurl,
			data: data, 
			dataType: 'text'
		}).done(function(response) {
			$(gif).css('display', 'none');
			reminder_msg.hide();
			success_msg.fadeIn();
		}).fail(function() {
			reminder_msg.hide();
			error_msg.fadeIn();
		});
	}

	/*****************************************************************************
 	* Delete Promo Feature Set 
	***************************************************************************/
	function juliet_delete_promo_feature(form) {
				
		var form_to_delete = form,
			item_wrapper = '.juliet-custom-feature-items',
			welcome_note = '.juliet-no-custom-features',
			nonce = $('#juliet-custom-page-nonce').val(),
			data = form_to_delete.serialize() + '&nonce=' + nonce + '&action=juliet_delete_instance',
			obj_to_delete = form_to_delete.parent();

		$.ajax({
			type: 'POST',
			url: ajaxurl, 
			data: data, 
			dataType: 'text',
		}).done(function(response) {
			obj_to_delete.fadeOut('normal', function() {
				$(this).remove();
				if($(item_wrapper).children().length == 0) {
					$(welcome_note).fadeIn();
				}
	
			});
		});	
	}

	/*****************************************************************************
 	* Display Reminder to Save Settings 
	***************************************************************************/
	function juliet_item_save_reminder() {

		var form = '.juliet-custom-feature-form';
		
		$(form + ' :input').on('change input', function() {
			var form = $(this).parents('.juliet-custom-feature-form'),
				reminder_msg = $(form).find('.juliet-custom-save-reminder'),
				success_msg = reminder_msg.next();
			
				success_msg.hide();
				reminder_msg.fadeIn();
		});
	}	

	/*****************************************************************************
 	* Animate Feature Slider Type - Cat vs Manual Entry
	***************************************************************************/
	function juliet_change_feat_slider_type() {

		var top_row = $(this).parent().parent();
		var cat_row = top_row.next();
		var id_row = cat_row.next();

		if($(this).val() === 'man-slides') {
			cat_row.hide();
			id_row.fadeIn();
		} else {
			id_row.hide();
			cat_row.fadeIn();
		}
	}

	function juliet_load_feat_slider_type() {

		$('.juliet-custom-feature-form').find('#juliet_feat_slider_type').each(function() {
			var top_row = $(this).parent().parent();
			var cat_row = top_row.next();
			var id_row = cat_row.next();

			if($(this).val() === 'man-slides') {
				cat_row.hide();
				id_row.fadeIn();
			} else {
				id_row.hide();
				cat_row.fadeIn();	
			}
		});
	}

	/*****************************************************************************
 	* Function Set: Images selected for Feat Image or Promo Boxes
 	*
 	* Description: Select, Resize, and Delete Images
	******************************************************************************/
	
	/*****************************************************************************
 	* IMAGES - Use WP Media Uploader to Select Images for Promo Features
	***************************************************************************/
	function juliet_upload_media(event) {

		event.preventDefault();

		var uploadMedia;
		var	upload_button = $(this),
			url_input = upload_button.prev(),
			image_wrapper = upload_button.next(),
			image = image_wrapper.children(':first'),
			remove_button = image.next();

		$(remove_button).css('display', 'none');

		if(uploadMedia) {
			uploadMedia.open();
			return;
		}

		uploadMedia = wp.media.frames.file_frame = wp.media({
  			title: 'Select Image',
  			button: {
  			text: 'Select Image'
			}, 
			multiple: false, 
		});

		uploadMedia.on('select', function() {
			var form = url_input.parent().parent().parent();
			var reminder_msg = $(form).find('.footer').children(':first').next().next();	
			var success_msg = reminder_msg.next();
  			var selection = uploadMedia.state().get('selection').first().toJSON();
  			url_input.val(selection.url);
 
  			image_wrapper.css('display', 'block');
  
  			juliet_resize_promo_img(selection.url, image, upload_button, remove_button);

  			(reminder_msg).fadeIn();
  			(success_msg).hide();
		});

		uploadMedia.open();
	}

	/*****************************************************************************
 	* IMAGES - Get & Post Resized Image 
	***************************************************************************/
	function juliet_resize_promo_img(img_url, image, button, remove_button) {

		var data =  {
			action: 'juliet_resize_img',
			image: img_url,
			height: 250,
			width: 250,
		}	

		$.ajax({
			type: 'POST',
			url: ajaxurl, 
			data: data, 
		}).done(function(response) {
			button.css('display', 'none');
			image.attr('src', response);
			$(remove_button).css('display', 'block');
		});
	}	
	
	/*****************************************************************************
 	* IMAGES - Delete Promo Feature Image 
	***************************************************************************/	
	function juliet_delete_uploaded_img() {
		
		var img_wrapper = $(this).parent(),
			form = img_wrapper.parent().parent().parent(),
			footer = form.find('.footer'),
			reminder_msg = footer.children(':first').next().next(),
			success_msg = reminder_msg.next(),
			img_wrapper_img_url = img_wrapper.children(':first'),
			upload_button = img_wrapper.prev(),
			img_url = upload_button.prev();
		
		img_wrapper.css('display', 'none');
		success_msg.hide();
		reminder_msg.fadeIn();
		upload_button.css('display', 'inline-block');

		img_wrapper_img_url.attr('src', '');
		img_url.val('');
	}

	/*****************************************************************************
 	* IMAGES - Selected Image is Deleted from the Media Library
	***************************************************************************/
	function juliet_media_is_deleted() {

		//Reset the Values if Selected Image is Deleted from the Media Library		
		$('.upload_img_preview').each(function() {
			if($(this).attr('src') == '') {
				var img_wrapper = $(this).parent();
				var upload_button = img_wrapper.prev();
				var img_url = upload_button.prev();

				upload_button.css('display', 'block');
				img_url.val('');
			}	
		});
	}

	/*****************************************************************************
 	* IMAGES - Display Remove Button if Image has a Value
	***************************************************************************/	
	function juliet_check_image_values() {

		var image = '.upload_img_preview';
		
		$(image).each(function() {
			if($(this).attr('src').length != 0) {
				$(this).next().css('display', 'block');
			}
		});	
	}
});