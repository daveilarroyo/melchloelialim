<?php
/**
 * This file contains the main init code for WP admin:
 * Enqueuing CSS and JS files
 *
 * @author Pix & Hue
 */

/*****************************************************************************
 * Register & Enqueue Admin styles and scripts
****************************************************************************/
add_action( 'admin_enqueue_scripts','juliet_load_admin_scripts' );

if ( !function_exists('juliet_load_admin_scripts') )  {

	function juliet_load_admin_scripts() {

		//Enqueue WordPress dependenices 
	   	wp_enqueue_style( 'wp-color-picker' );
	   	wp_enqueue_script('jquery-ui-dialog');
	   	wp_enqueue_script('jquery-color');
	   	wp_enqueue_media();
	   	
	   	//Enqueue Styles & Scripts
	   	wp_enqueue_style( 'juliet-options-style', get_template_directory_uri() . '/lib/css/options.css');
	   	wp_enqueue_style( 'juliet-custom-page-style', get_template_directory_uri() . '/lib/css/custom-page.css');
	   	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');	

	   	wp_enqueue_script('juliet-custom-page-scripts', get_template_directory_uri() . '/lib/js/custom-page.js', array('jquery', 'jquery-ui-dialog', 'wp-color-picker'), true);
	   	wp_enqueue_script('juliet-option-scripts', get_template_directory_uri() . '/lib/js/options.js', array('jquery', 'jquery-color'), true);
	}   	
}

/*****************************************************************************
 * Register & Enqueue Styles and Scripts for Meta Boxes
****************************************************************************/
add_action( 'admin_enqueue_scripts', 'juliet_add_admin_scripts', 10, 1 );

if ( !function_exists('juliet_add_admin_scripts') )  {

	function juliet_add_admin_scripts( $hook ) {

	    global $post;

	    if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
	        if ( $post->post_type === 'page' || $post->post_type === 'post') {     
	            
	    	    //Enqueue meta admin scripts and styles
	            wp_enqueue_style('juliet-meta-style', get_template_directory_uri() . '/lib/css/meta.css');
	            wp_enqueue_script('juliet-meta-scripts', get_template_directory_uri() . '/lib/js/meta.js', array('jquery'), true);
	        }
	    }
	}
}		 