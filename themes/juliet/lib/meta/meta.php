<?php 

/**
 * Contains the main functionality for post & page meta boxes 
 *
 * @author Pix & Hue
 */

/*****************************************************************************
 * Add the Meta Box
****************************************************************************/
add_action( 'add_meta_boxes', 'juliet_add_meta_box' );

if ( !function_exists('juliet_add_meta_box') )  {

	function juliet_add_meta_box() {
	    add_meta_box( 'juliet-post-meta-box', 'Juliet Post Settings', 'juliet_render_post_meta_box', 'post', 'normal', 'high' );  
	    add_meta_box( 'juliet-meta-box', 'Juliet Page Settings', 'juliet_render_meta_box', 'page', 'normal', 'high' );  
	}
}	

/*****************************************************************************
 * Render the Meta Post & Page Boxes
****************************************************************************/
if ( !function_exists('juliet_render_post_meta_box') )  {

	function juliet_render_post_meta_box() {
		wp_nonce_field( 'juliet_post_meta_box_nonce', 'meta_box_nonce' );
		juliet_render_post_meta_box_options();
	}
}	

if ( !function_exists('juliet_render_meta_box') )  {

	function juliet_render_meta_box($post) { 
		wp_nonce_field( 'juliet_meta_box_nonce', 'meta_box_nonce' );
		juliet_render_page_meta_box_options();
	}
}	

/*****************************************************************************
 * Render Meta Options for Post
****************************************************************************/
if ( !function_exists('juliet_render_post_meta_box_options') )  {

	function juliet_render_post_meta_box_options() {

		global $post;
		$values = get_post_custom( $post->ID );	

		$highlighted_cat = isset( $values['juliet-cat-highlight'] ) ? esc_attr( $values['juliet-cat-highlight'][0] ) : '';
		$pin_description = isset( $values['juliet-pin-desc'] ) ? esc_attr( $values['juliet-pin-desc'][0] ) : '';
		$affiliate_type = isset( $values['juliet-affiliate-type'] ) ? esc_attr( $values['juliet-affiliate-type'][0] ) : '';
		$affiliate_url = isset( $values['juliet-affiliate-url'] ) ? esc_attr( $values['juliet-affiliate-url'][0] ) : '';
		$affiliate_img = isset( $values['juliet-affiliate-img'] ) ? esc_attr( $values['juliet-affiliate-img'][0] ) : '';
		$affiliate_code = isset( $values['juliet-affiliate-code'] ) ? esc_attr( $values['juliet-affiliate-code'][0] ) : '';
	

		$meta_options = array(

			//Highlight Category
			array('title' => 'Category Settings', 'id' => 'juliet-post-cat-settings', 'type' => 'section'),
			array('title' => 'Select Category to Highlight in Block Layout:', 'name' => 'juliet-cat-highlight', 'type' => 'cat-select', 'meta' => 'post', 'value' => $highlighted_cat),
			array('type' => 'end-section'),

			//Custom Pinterest Description
			array('title' => 'Pinterest Description', 'id' => 'juliet-pin-settings', 'type' => 'section'),
			array('title' => 'Write a custom pin description for this post. All images pinned from this post will have this custom description.', 'name' => 'juliet-pin-desc', 
				  'id'=>'juliet-pin-desc', 'type' => 'textarea', 'rows' => 4, 'value' => $pin_description),
			array('type' => 'end-section'),

			//Affiliate Links
			array('title' => 'Affiliate Link Settings', 'id' => 'juliet-affiliate-settings', 'type' => 'section'),
			array('title' => 'Select Type:', 'name' => 'juliet-affiliate-type', 'type' => 'select', 'value' => $affiliate_type,
	 			  'options'=> array(array('id'=>'juliet-no-intro-feature', 'name' => 'None'), array('id'=>'juliet-aff-link', 'name' => 'Link + Image'), 
	 								array('id'=>'juliet-aff-code', 'name'=>'HTML Code'))),
			array('type' => 'end-section'),
			array('title' => 'Content of Affiliate Link', 'id' => 'juliet-affiliate-content', 'type' => 'section'),
			array('title' => 'Web Address of Affiliate Link', 'id' => 'juliet-affiliate-url', 'type' => 'text', 'text_type' => 'website', 'value' => $affiliate_url),
			array('title' => 'Image of Affiliate Link', 'id' => 'juliet-affiliate-img', 'type' => 'image', 'value' => $affiliate_img),
			array('type' => 'end-section'),
			array('title' => 'HTML Code of Affiliate Widget', 'id' => 'juliet-affiliate-code-section', 'type' => 'section'),
			array('title' => 'Enter HTML Code:', 'id' => 'juliet-affiliate-code', 'type' => 'textarea', 'rows' => 8, 'value' => $affiliate_code),
			array('type' => 'end-section'),

					    );

		//Render the Options within the Meta Box
		foreach ($meta_options as $option) {

			if(isset($option['type'])) {
				if(isset($option['value'])) {
					$saved_val = $option['value'];
				}	

				switch ($option['type']) {
				
					case 'section':	
						juliet_meta_new_option_section($option);
						break;	

					case 'select':	
						juliet_meta_option_select($option, $saved_val);
						break;	

					case 'cat-select':	
						juliet_meta_cat_selection($option, $saved_val);
						break;		

					case 'text':	
						juliet_meta_option_text($option, $saved_val);
						break;	

					case 'textarea':	
						juliet_meta_option_textarea($option, $saved_val);
						break;	

					case 'image':	
						juliet_meta_option_upload($option, $saved_val);
						break;			

					case 'end-section':	
						juliet_meta_end_section();
						break;		
				}	
			}	
		}
	}
}			

/*****************************************************************************
 * Render Meta Options for Page
****************************************************************************/
if ( !function_exists('juliet_render_page_meta_box_options') )  {

	function juliet_render_page_meta_box_options() {
		
		//Set the Variables for the Meta Box 
		global $post;
		$values = get_post_custom( $post->ID );	

		//Page Title
		$ph_std_page_title = isset( $values['juliet-standard-page-title'] ) ? esc_attr( $values['juliet-standard-page-title'][0] ) : '';
		$pin_description = isset( $values['juliet-pin-desc'] ) ? esc_attr( $values['juliet-pin-desc'][0] ) : '';

		//Features Section
		$ph_feature_type = isset( $values['juliet-promo-feature-type'] ) ? esc_attr( $values['juliet-promo-feature-type'][0] ) : '';
		$ph_feat_img = isset( $values['juliet-feat-image-select'] ) ? esc_attr( $values['juliet-feat-image-select'][0] ) : '';
		$ph_feat_slider = isset( $values['juliet-feat-slider-select'] ) ? esc_attr( $values['juliet-feat-slider-select'][0] ) : '';
		$ph_triple_images = isset( $values['juliet-trip-images-select'] ) ? esc_attr( $values['juliet-trip-images-select'][0] ) : '';
		
		//Blog
		$ph_blog_cat = isset( $values['blog_cat'] ) ? esc_attr( $values['blog_cat'][0] ) : '';
		$ph_blog_page_width = isset( $values['juliet-blog-page-width'] ) ? esc_attr( $values['juliet-blog-page-width'][0] ) : '';
		$ph_blog_page_title = isset( $values['juliet-blog-page-title'] ) ? esc_attr( $values['juliet-blog-page-title'][0] ) : '';

		//Block
		$ph_block_cat = isset( $values['block_cat'] ) ? esc_attr( $values['block_cat'][0] ) : '';

		//Gal
		$ph_gal_overlay = isset( $values['juliet-gal-overlay'] ) ? esc_attr( $values['juliet-gal-overlay'][0] ) : '';

		//Define the Options for the Page Meta Box
		$meta_options = array(

			//Page Title Options
			array('title' => 'Show/Hide Page Title', 'id' => 'juliet-standard-layout', 'type' => 'section'),
			array('title' => 'Display Page Title:', 'name' => 'juliet-standard-page-title', 'type' => 'select', 'value' => $ph_std_page_title,
				 'options'=> array(array('id'=>'juliet-no-title', 'name' => 'No'), array('id'=>'juliet-title', 'name'=>'Yes'))),
			array('type' => 'end-section'),

			//Custom Pinterest Description
			array('title' => 'Pinterest Description', 'id' => 'juliet-pin-settings', 'type' => 'section'),
			array('title' => 'Write a custom pin description for this page. All images pinned from this page will have this custom description.', 'name' => 'juliet-pin-desc', 
				  'id'=>'juliet-pin-desc', 'type' => 'textarea', 'rows' => 4, 'value' => $pin_description),
			array('type' => 'end-section'),
		
			//Page Feature Options
			array('title' => 'Select Page Feature', 'id' => 'juliet-page-feature', 'type' => 'section'),
			array('title' => 'Select Page Feature Type:', 'name' => 'juliet-promo-feature-type', 'type' => 'select', 'value' => $ph_feature_type,
				 'options'=> array(array('id'=>'juliet-no-intro-feature', 'name' => 'None'), array('id'=>'juliet-featured-image', 'name'=>'Featured Image'), 
				 				   array('id'=>'juliet-featured-slider', 'name'=>'Featured Slider'), array('id'=>'juliet-triple-images', 'name'=>'Triple Images'))),
			array('title' => 'Select Featured Image:', 'name' => 'juliet-feat-image-select','type' => 'select', 'value' => $ph_feat_img, 'cat-type' => 'juliet_cp_feature_image'),
			array('title' => 'Select Featured Slider:', 'name' => 'juliet-feat-slider-select','type' => 'select', 'value' => $ph_feat_slider, 'cat-type' => 'juliet_cp_feat_slider'),
			array('title' => 'Select Set of Triple Images:', 'name' => 'juliet-trip-images-select','type' => 'select', 'value' => $ph_triple_images, 'cat-type' => 'juliet_cp_triple_images'),
			array('type' => 'end-section'),

			//Blog Page Options
			array('title' => 'Page Layout Options', 'id' => 'juliet-blog-selection', 'type' => 'section'),
			array('title' => 'Select the Category:', 'name' => 'blog_cat', 'type' => 'cat-select', 'value'=> $ph_blog_cat),
			array('title' => 'Enable Sidebar:', 'name' => 'juliet-blog-page-width', 'type' => 'select', 'value' => $ph_blog_page_width,
				 'options'=> array(array('id'=>'juliet-no-sidebar', 'name' => 'No'), array('id'=>'juliet-sidebar', 'name'=>'Yes'))),
			array('type' => 'end-section'),

			//Block Page Options
			array('title' => 'Layout Options', 'id' => 'juliet-block-selection', 'type' => 'section'),
			array('title' => 'Select the Category:', 'name' => 'block_cat', 'type' => 'cat-select', 'value'=> $ph_block_cat),
			array('type' => 'end-section'),

			//Gallery Options
			array('title' => 'Gallery Options', 'id' => 'juliet-gal-selection', 'type' => 'section'),
			array('title' => 'Select Rollover Effect:', 'name' => 'juliet-gal-overlay', 'type' => 'select', 'value' => $ph_gal_overlay,
				 'options'=> array(array('id'=>'juliet-no-overlay', 'name' => 'No Rollover Effect'), array('id'=>'juliet-gal-hover-overlay', 'name'=>'Color Background + Post Title'), 
				 array('id'=>'juliet-gal-pin-button', 'name'=>'Pin It Button'))),
			array('type' => 'end-section'),
						
						);

		//Render the Options within the Meta Box
		foreach ($meta_options as $option) {

			if(isset($option['type'])) {
				if(isset($option['value'])) {
					$saved_val = $option['value'];
				}	

				switch ($option['type']) {
					case 'section':	
						juliet_meta_new_option_section($option);
						break;	

					case 'select':	
						juliet_meta_option_select($option, $saved_val);
						break;	

					case 'cat-select':		
						juliet_meta_cat_selection($option, $saved_val);
						break;

					case 'textarea':	
						juliet_meta_option_textarea($option, $saved_val);
						break;		

					case 'end-section':	
						juliet_meta_end_section();
						break;	
				}	
			}	

		}
	}
}	

/*****************************************************************************
 * Render New Option Section in Meta Box
****************************************************************************/
if ( !function_exists('juliet_meta_new_option_section') )  {

	function juliet_meta_new_option_section($option) { ?>

		<div id="<?php echo esc_attr($option['id']); ?>" class="juliet-meta-option-section">
			<h4 class="juliet-meta-option-heading"><?php echo esc_html($option['title']); ?></h4>
			<div class="juliet-meta-option">

	<?php }
}

/*****************************************************************************
 * Render Textbox in Meta Box
****************************************************************************/	
if ( !function_exists('juliet_meta_option_text') )  {

	function juliet_meta_option_text($option, $saved_val) { ?>

		<div class="juliet-meta-option-half-column">

			<?php if(isset($option['title'])) { ?>
				<h4><?php echo esc_html($option['title']); ?></h4>
			<?php } ?>

			<input type="text" id="<?php echo esc_attr($option['id']);?>" name="<?php echo esc_attr($option['id']);?>" class="option-input" value="<?php echo esc_attr($saved_val); ?>"/>

			<?php if(isset($option['text_type']) && $option['text_type'] == 'website') { ?>
				<p class="url_note">Note: use http:// in front of your URL.</p>
			<?php } ?>

		</div>

	<?php }	
}	

/*****************************************************************************
 * Render Textarea in Meta Box
****************************************************************************/	
if ( !function_exists('juliet_meta_option_textarea') )  {

	function juliet_meta_option_textarea($option, $saved_val) { ?>
		
		<div class="juliet-meta-option-full-column">

			<?php if(isset($option['title'])) { ?>
				<h4><?php echo esc_html($option['title']); ?></h4>
			<?php } ?>

			<textarea id="<?php echo esc_attr($option['id']);?>" name="<?php echo esc_attr($option['id']);?>" rows="<?php echo esc_attr($option['rows']);?>" cols="100"><?php echo esc_textarea($saved_val);?></textarea>

		</div>

	<?php }	
}	


/*****************************************************************************
 * Render Dropdown in Meta Box
****************************************************************************/
if ( !function_exists('juliet_meta_option_select') )  {

	function juliet_meta_option_select($option, $saved_val) { ?>

		<div class="juliet-meta-option-half-column">
		
		<?php if(isset($option['title'])) { ?>
				<h4><?php echo esc_html($option['title']); ?></h4>
		<?php } ?>

			<select name="<?php echo esc_attr($option['name']);?>" id="<?php echo esc_attr($option['name']);?>">

			<?php //If there are manual options, display them
			if(isset($option['options'])) {
				foreach ($option['options'] as $option ) {
					$selected = $saved_val==$option['id']?' selected="selected"':''; ?>
					<option value="<?php echo esc_attr($option['id']);?>" <?php echo esc_attr($selected); ?>><?php echo esc_html($option['name']); ?></option>
				<?php }
			}
			
			//Query for the dynamic options
			if (isset($option['cat-type'])) {
				
				$cat_type = $option['cat-type'];
				$args = array(
					'category_name' => $cat_type,
					'post_status' => 'juliet-custom-page',
					'posts_per_page' => -1,
				);

				$meta_select_options = get_posts($args);

				foreach($meta_select_options as $select_option) { 
					$selected = $saved_val==$select_option->ID ? ' selected="selected"' : ''; ?>
					<option value="<?php echo esc_attr($select_option->ID);?>" <?php echo esc_attr($selected); ?>><?php echo esc_html($select_option->post_title);?></option>
				<?php }

				wp_reset_postdata();
			} ?>

			</select>	
		</div>

	<?php }
}

/*****************************************************************************
 * Render Cat Dropdown in Meta Box
****************************************************************************/
if ( ! function_exists('juliet_meta_cat_selection') ) {

	function juliet_meta_cat_selection($option, $saved_val) { 

		if(is_numeric($saved_val)) {
			$saved_val = get_category($saved_val)->slug;
		} ?>

		<div class="juliet-meta-option-half-column">

		<?php if(isset($option['title'])) { ?>
				<h4><?php echo esc_html($option['title']); ?></h4>
		<?php } ?>

			<select name="<?php echo esc_attr($option['name']);?>" id="<?php echo esc_attr($option['name']);?>">

			<?php //All Cats
			if(!isset($option['meta']) || $option['meta'] != 'post') {
				$all_cat = $saved_val=="juliet-all-cats" ? "selected='selected'" : ''; ?>
				<option value="juliet-all-cats" <?php echo esc_attr($all_cat); ?>>All</option>
			<?php } else { 
				$no_cat = $saved_val=="juliet-no-cats" ? "selected='selected'" : ''; ?>
				<option value="juliet-no-cats" <?php echo esc_attr($no_cat); ?>>None</option>
			<?php }	?>

			<?php //Select Single Cat 
			$categories = get_categories();

			foreach ($categories as $category) {
				$selected = $saved_val==$category->slug ? ' selected="selected"' : ''; ?>
				<option value="<?php echo esc_attr($category->slug);?>" <?php echo esc_attr($selected); ?>><?php echo esc_html($category->name); ?></option>
			<?php } ?>		

			</select>
		</div>

	<?php }
}

/*****************************************************************************
 * Render Image Field in Meta Box
****************************************************************************/
if ( ! function_exists('juliet_meta_option_upload') ) {

	function juliet_meta_option_upload($option, $saved_val) { 

		//Convert imported images to local images.
		$url_parsed = wp_parse_url($saved_val);
		$post_id = get_the_ID(); $field_id = 0;
		if(isset($url_parsed['host']) && ($url_parsed['host'] == 'juliet.pixandhue.com' || $url_parsed['host'] == 'www.juliet.pixandhue.com')) {
			$saved_val = juliet_adjust_img_url($saved_val, $post_id, $field_id);
		} ?>

		<div class="juliet-meta-option-half-column">

			<?php if(isset($option['title'])) { ?>
					<h4><?php echo esc_html($option['title']); ?></h4>
			<?php } ?>
				
			<input type="hidden" class="juliet-aff-img-url" id="<?php echo esc_attr($option['id']);?>" name="<?php echo esc_attr($option['id']);?>" value ="<?php echo esc_url($saved_val); ?>" />
			
			<?php if(empty($saved_val)) { ?>
				<input type="button" value="Select Image" class="juliet-upload-button"/>
			<?php } else { ?>
				<input type="button" value="Select Image" class="juliet-upload-button" style="display: none"/>
			<?php } ?>

			<div class="upload_img_wrapper">
				<?php if(empty($saved_val)) { ?>
					<img src="" class="upload_img_preview">
				<?php } else { ?>
					<img class="upload_img_preview" src="<?php echo esc_url(juliet_resize_wp_image($saved_val, 250, 250)); ?>"/>
				<?php } ?>	
				<input type="button" value="x" class="juliet-remove-upload-button" />
			</div>

		</div>

	<?php }
}

/*****************************************************************************
 * End Option Section in Meta Box
****************************************************************************/
if ( !function_exists('juliet_meta_end_section') )  {

	function juliet_meta_end_section() { ?>
			</div>
		</div>
	<?php }
}		

/*****************************************************************************
 * Save Data in Meta Box Fields
****************************************************************************/
add_action( 'save_post', 'juliet_save_meta_box' );

if ( !function_exists('juliet_save_meta_box') )  {

	function juliet_save_meta_box($post_id) {

	   	//Check User Permissions & Nonce For Security
	    if( !isset( $_POST['meta_box_nonce'] ) || (!wp_verify_nonce( $_POST['meta_box_nonce'], 'juliet_meta_box_nonce' ) && get_post_type($post_id) == 'page'))  {
	    	return;
	    }	

	    if( !isset( $_POST['meta_box_nonce'] ) || (!wp_verify_nonce( $_POST['meta_box_nonce'], 'juliet_post_meta_box_nonce' ) && get_post_type($post_id) == 'post'))  {
	    	return;
	    }

	    if( !current_user_can( 'edit_post', $post_id ) ) {
	    	return;
	    }	

	    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
	    	return;
	    }

	    //For each of the options, Update the DB
	    $meta_options = array('juliet-promo-feature-type', 'juliet-feat-image-select', 'juliet-feat-slider-select', 'juliet-trip-images-select', 
	    					  'juliet-standard-page-title', 'juliet-gal-overlay', 'blog_cat', 'block_cat',
	    					   'juliet-blog-page-width', 'juliet-blog-page-title', 'juliet-cat-highlight', 'juliet-pin-desc',
	    					   'juliet-affiliate-type', 'juliet-affiliate-url', 'juliet-affiliate-img', 'juliet-affiliate-code');

	    foreach( $meta_options as $option ) {
	    	if( isset( $_POST[$option] ) ) {
	    		update_post_meta( $post_id, $option, esc_attr( $_POST[$option] ) );
	    	}
	    }
	}  
}