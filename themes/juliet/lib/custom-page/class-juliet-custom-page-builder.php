<?php

/**
 * This is a custom page builder class - it is used to build (render) 
 * custom pages in the theme admin. Custom pages are made up of forms 
 * to create/update/delete the promo features and display any data
 * previously added to the forms.
 * 
 * @author Pix & Hue
 */

class Juliet_Custom_Page_Builder {

	public $custom_page = null;
	const custom_prefix = 'juliet_';
	const nonce = 'juliet-custom-nonce';

	function __construct ( $custom_page ) {
		$this->custom_page = $custom_page;
	}

	/*****************************************************************************
 	* Render the Custom Page
	****************************************************************************/
	public function juliet_build_custom_page() { ?>

		<div class="juliet-custom-features"> 
			<?php $this->juliet_build_custom_page_header(); 

			$post_array = $this->_juliet_get_custom_posts();
			if(count($post_array) == 0) {
				$this->juliet_render_welcome_note();
			} ?>

			<div class="juliet-custom-feature-items"> 
				<?php foreach($post_array as $post) {
					$post_id = $post;
					$this->juliet_build_custom_feature($post_id, $this->custom_page->fields);
				} ?> 
			</div>	
		</div>  
		
	<?php }

	/*****************************************************************************
 	* Render Header of the Custom Page 
	****************************************************************************/
	public function juliet_build_custom_page_header() { ?>

		<h2><?php echo esc_html($this->custom_page->page_name); ?></h2>
		<a class="juliet-create-custom-feature">+ | Create New Item</a>
		<div class="juliet-dialog-box-wrapper">
	
			<form class="juliet-add-custom-feature" method="POST">
				<label for="juliet-custom-feature-name">Name:</label>
				<input type="text" name="juliet-custom-feature-name" class="juliet-custom-feature-name">
				<input type="button" class="juliet-custom-feature-add-button" value="Add">
				<input type="hidden" class="juliet-custom-feature-type" name="juliet-custom-feature-type" value="<?php echo esc_attr($this->custom_page->cat_name); ?>">
				<input type="hidden" class="juliet-custom-feature-id" name="juliet-custom-feature-id" value="<?php echo esc_attr($this->custom_page->cat_id); ?>">
				<input type="hidden" id="juliet-custom-page-nonce" value="<?php echo esc_attr(wp_create_nonce( self::nonce ));?>" />
			</form>

			<div class="juliet-custom-delete-box">
				<p>This feature and all the settings associated with it will be permanently deleted.</p>
				<p>Are you sure you you want to continue?</p>
				<input type="button" class="juliet-custom-confirm-delete" value="Delete">
				<input type="button" class="juliet-custom-cancel-delete" value="Cancel">	
			</div>

		</div>	

	<?php }

	/*****************************************************************************
 	* Render Welcome Note - if no custom features have been created
	****************************************************************************/
	public function juliet_render_welcome_note() { ?>
		<div class="juliet-no-custom-features">
			<p>You have not created any <?php echo esc_html($this->custom_page->page_name); ?> yet.</p>
			<p>To get started, click the "Create New Item" button above.</p>
		</div>

	<?php }	

	/*****************************************************************************
 	* Render each Individual Promo/Feature Object on the Custom Page 
	****************************************************************************/
	public function juliet_build_custom_feature($post_id, $fields) { ?>

		<div class ="juliet-custom-feature-wrapper">

			<?php $this->juliet_build_custom_feature_header($post_id); ?>

			<form class="juliet-custom-feature-form">

				<?php foreach ( $fields as $field) {
					$field_id = self::custom_prefix.$field['id'];
					$field_position_array = $this->_juliet_set_field_position($field);
					$start_row = $field_position_array[0];
					$start_field = $field_position_array[1];
					$end_field = $field_position_array[2];
					$end_row = $field_position_array[3];

					if($start_row && $field['id'] === 'feat_post_ids') { ?>
						<div class="juliet-custom-page-row feat_post_ids">
					<?php } else if($start_row && $field['id'] === 'feat_content_cat') { ?>
						<div class="juliet-custom-page-row feat_content_cat">
					<?php } else if ($start_row) { ?>
						<div class="juliet-custom-page-row">
					<?php }

					if(isset($field['number'])) { ?>
						<h4 class="juliet-custom-item-number">Image <?php echo esc_html($field['number']); ?></h4>
					<?php }	
					
					if($start_field) { ?>
						<div class="juliet-custom-two-column">
					<?php }

					$saved_val = '';
					if(!empty($post_id)) {
						$saved_val = get_post_meta($post_id, $field_id, true);
					}
					
					if(isset($field['type'])) {
						switch ($field['type']) {
							case 'text':
								$this->juliet_build_custom_text_input($field, $field_id, $saved_val);
								break;

							case 'select':
								$this->juliet_build_custom_select_input($field, $field_id, $saved_val);
								break;

							case 'select_cat':
								$this->juliet_build_custom_select_cat_input($field, $field_id, $saved_val);
								break;	

							case 'upload':
								$this->juliet_build_custom_upload_input($field, $field_id, $saved_val, $post_id);
								break;

							case 'color':
								$this->juliet_build_custom_color_input($field, $field_id, $saved_val);
								break;	
						}
					}

					if($end_field) { ?>
						</div>
					<?php }

					if($end_row) { ?>
						</div>
					<?php }
				} ?>
				
				<div class="juliet-custom-page-row footer">
					<input type="button" class="juliet-custom-feature-update" value="Update"/>
					<span class="custom-page-options-loader"></span>
					<p class="juliet-custom-save-reminder options-message">You have made changes to this item. Please select "Update" to save item settings.</p>
					<p class="juliet-custom-success-message options-message">Item successfully updated!</p>
					<p class="juliet-custom-error-message options-message">Error, please try again.</p>
				</div>
				
				<!--//Hidden Fields are used to Update/Delete Custom Posts & Nonces to Verify Security-->
				<input type="hidden" name="juliet-promo-feature-cat-type" value="<?php echo esc_attr($this->custom_page->cat_name); ?>"/>
				<input type="hidden" name="juliet-promo-feature-cat-id" value="<?php echo esc_attr($this->custom_page->cat_id); ?>"/>
				<input type="hidden" name="juliet-promo-feature-post-id" value="<?php echo esc_attr($post_id); ?>"/>
				<input type="hidden" id="juliet-custom-page-nonce" value="<?php echo esc_attr(wp_create_nonce( self::nonce )); ?>" />

			</form>

		</div>
		
	<?php }

	/*****************************************************************************
 	* Render each Individual Promo Feature Header
	****************************************************************************/
	public function juliet_build_custom_feature_header($post_id) { ?>

		<div class="juliet-custom-feature-header">
			<div class="juliet-animate-triangle expanded"></div><h3 class="juliet-custom-feature-title"><?php echo esc_html(get_the_title($post_id)); ?></h3>
			<input type="button" class="juliet-custom-feature-delete" value="X"/>
		</div>

	<?php }

	/*****************************************************************************
 	* Custom Page Field: Render Text Input 
	****************************************************************************/
	public function juliet_build_custom_text_input($field, $field_id, $saved_val) { ?>

		<h5><?php echo esc_html($field['title']); ?></h5>
		<input type="text" id="<?php echo esc_attr($field_id);?>" name="<?php echo esc_attr($field_id);?>" class="option-input" value="<?php echo esc_attr($saved_val); ?>"/>

		<?php if(isset($field['text_type']) && $field['text_type'] == 'website') { ?>
			<p class="url_note">Note: use http:// in front of your URL.</p>
		<?php }
	}

	/*****************************************************************************
 	* Custom Page Field: Render Select
	****************************************************************************/
	public function juliet_build_custom_select_input($field, $field_id, $saved_val) { ?>

		<h5><?php echo esc_html($field['title']); ?></h5>

		<select id="<?php echo esc_attr($field_id);?>" name="<?php echo esc_attr($field_id);?>">
		<?php foreach ($field['options'] as $option ) {
			$selected = $saved_val==$option['id']?' selected="selected"':''; ?>
			<option value="<?php echo esc_attr($option['id']);?>" <?php echo esc_attr($selected); ?>><?php echo esc_html($option['name']);?></option>
		<?php } ?>

		</select>
	
	<?php }

	/*****************************************************************************
 	* Custom Page Field: Render Cat Dropdown
	****************************************************************************/
	public function juliet_build_custom_select_cat_input($field, $field_id, $saved_val) { 

		if(is_numeric($saved_val)) {
			$saved_val = get_category($saved_val)->slug;
		} ?>

		<h5><?php echo esc_html($field['title']); ?></h5>
		<select id="<?php echo esc_attr($field_id);?>" name="<?php echo esc_attr($field_id);?>">	

		<?php $all_cat = $saved_val=="juliet-slider-all-cats" ? "selected='selected'" : ''; ?>
		<option value="juliet-slider-all-cats" <?php echo esc_attr($all_cat); ?>>All</option>

		<?php $categories = get_categories();
		foreach ($categories as $category) {
			$selected = $saved_val==$category->slug ? ' selected="selected"' : ''; ?>
			<option value="<?php echo esc_attr($category->slug); ?>" <?php echo esc_attr($selected); ?>><?php echo esc_html($category->name); ?></option>
		<?php } ?>		

		</select>
	<?php }

	/*****************************************************************************
 	* Custom Page Field: Render Upload Image Button & Image Preview 
	****************************************************************************/
	public function juliet_build_custom_upload_input($field, $field_id, $saved_val, $post_id) { 

		$url_parsed = wp_parse_url($saved_val);
		if(isset($url_parsed['host']) && ($url_parsed['host'] == 'juliet-blog.pixandhue.com' || $url_parsed['host'] == 'juliet.pixandhue.com')) {
			$saved_val = juliet_adjust_img_url($saved_val, $post_id, $field_id);
		} ?>
			
		<h5><?php echo esc_html($field['title']); ?></h5>	
		
		<input type="hidden" class="juliet-img-url" id="<?php echo esc_attr($field_id); ?>" name="<?php echo esc_attr($field_id); ?>" value ="<?php echo esc_url($saved_val); ?>" />
		
		<?php if(empty($saved_val)) { ?>
			<input type="button" value="Select Image" class="juliet-upload-button"/>
		<?php } else { ?>
			<input type="button" value="Select Image" class="juliet-upload-button" style="display: none"/>	
		<?php } ?>

		<div class="upload_img_wrapper">
			<?php if(empty($saved_val)) { ?>
				<img src="" class="upload_img_preview">
			<?php } else { ?>
				<img class="upload_img_preview" src="<?php echo esc_url(juliet_resize_wp_image(esc_attr($saved_val), 250, 250)); ?>"/>
			<?php } ?>	

			<input type="button" value="x" class="juliet-remove-upload-button" />
		</div>
	<?php }

	/*****************************************************************************
 	* Custom Page Field: Render Color Picker
	****************************************************************************/	
	public function juliet_build_custom_color_input($field, $field_id, $saved_val) { ?>
		
		<h5><?php echo esc_html($field['title']); ?></h5>	
		<input type="text" id="'<?php echo esc_attr($field_id); ?>" name="<?php echo esc_attr($field_id); ?>" class="option-input juliet-color-picker" 
			   data-default-color="<?php echo esc_attr($field['default']); ?>" value="<?php echo esc_attr($saved_val); ?>"/>
	<?php }

	/*****************************************************************************
 	* (Internal) Return an Array of IDs of Custom Post Type
	****************************************************************************/
	protected function _juliet_get_custom_posts() {

		$post_array = array();
		$loop = new WP_Query(
			array(
				'fields' => 'ids',
				'cat' => $this->custom_page->cat_id,
				'posts_per_page' => -1,
			)
		);

		while($loop->have_posts()){
			$loop->the_post();
			$post_id = get_the_ID();
			array_push($post_array, $post_id);
		}

		wp_reset_postdata();

		return $post_array;
	}

	/*****************************************************************************
 	* (Internal) Set Position Divs for Each Field
	****************************************************************************/
	protected function _juliet_set_field_position($field) {

		$start_row = false;
		$start_field = false;
		$end_field = false;
		$end_row = false;
		$div_position_array = array($start_row, $start_field, $end_field, $end_row);

		if(isset($field['start_field'])) {
			if($field['start_field'] == 'start_row') {
				$div_position_array[0] = true;
			}

			if($field['start_field'] == 'start' || $field['start_field'] == 'start_row') {
				$div_position_array[1] = true;
			} 
		}	

		if(isset($field['end_field'])) {
			if($field['end_field'] == 'end'|| $field['end_field'] == 'end_row') {
				$div_position_array[2] = true;;
			} 

			if($field['end_field'] == 'end_row') {
				$div_position_array[3] = true;;
			}
		}	

		return $div_position_array;
	}
}