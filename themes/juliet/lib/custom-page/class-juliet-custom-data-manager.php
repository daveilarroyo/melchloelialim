<?php 

/**
 * This is a custom data manager class - this class interacts with the 
 * WP Database to allow users to add, update, and delete the custom page
 * post types: feat image, feat slider, and triple images within 
 * the theme admin.
 * 
 * @author Pix & Hue
 */

class Juliet_Custom_Data_Manager {
	
	public $title = null;
	public $post_type = null;
	public $post_id = null;
	public $cat_name = null;
	public $cat_id = null;

	const user_capability = 'edit_theme_options';
	const custom_prefix = 'juliet_';
	const nonce = 'juliet-custom-page';

	/*****************************************************************************
 	* Get & Set Functions
	****************************************************************************/
	//Set Function: Post Id
	public function juliet_set_post_ID($post_id) {
		$this->post_id = $post_id;
	}

	//Set Function: Cat Name
	public function juliet_set_post_cat_name($cat_name) {
		$this->cat_name = $cat_name;
	}

	//Set Function: Cat ID
	public function juliet_set_post_cat_id($cat_id) {
		$this->cat_id = $cat_id;
	}

	//Set Function: Post Title
	public function juliet_set_post_title($title) {
		$this->title = $title;
	}

	//Get Function: Post Id
	public function juliet_get_post_ID() {
		return $this->post_id;
	}

	/*****************************************************************************
 	* Insert New Custom Post 
	****************************************************************************/
	public function juliet_insert_custom_post() {
		
		global $current_user, $juliet;

		//Check User Permissions before Updating Database
		if ( !current_user_can( self::user_capability ) ) {
			return false;
		}
		
		$custom_page = $juliet->custom_pages[$this->post_type];
		$cat_id = $this->cat_id;

		$new_post = array(
			'post_type' => 'post',
			'post_status' => 'juliet-custom-page',
			'post_category' => array($cat_id),
			'post_title' => $this->title,
			'post_name' => $this->title,
		);
		
		//Create a New Post
		$post_id = wp_insert_post($new_post);
		$this->juliet_set_post_ID($post_id);

		//Add Custom Fields for New Post 
		foreach($custom_page->fields as $field) {
			$key = isset($field['id']) ? self::custom_prefix.$field['id'] : '';
			$value = isset($_POST[$key]) ? $_POST[$key] : '';
			add_post_meta($this->post_id, $key, $value);
		}
	}
	
	/*****************************************************************************
 	* Update Custom Post 
	****************************************************************************/
	public function juliet_update_custom_post() {

		global $current_user, $juliet_theme_data;

		//Check User Permissions before Updating Database
		if ( !current_user_can( self::user_capability ) ) {
			return false;
		}

		$custom_page = $juliet_theme_data->custom_pages[$this->cat_name];
		$post_id = $this->post_id; 

		//Update Custom Fields for New Post 
		foreach($custom_page->fields as $field) {
			$key = isset($field['id']) ? self::custom_prefix.$field['id'] : '';
			$value = isset($_POST[$key]) ? wp_strip_all_tags($_POST[$key]) : '';
			update_post_meta($post_id, $key, $value);
		}
	}

	/*****************************************************************************
 	* Delete Custom Post 
	****************************************************************************/
	public function juliet_delete_custom_post() {

		global $current_user;

		//Check User Permissions before Updating Database
		if ( !current_user_can( self::user_capability ) ) {
			return false;
		}

		$post_id = $this->post_id; 
		wp_delete_post($post_id, true);
	}
}	