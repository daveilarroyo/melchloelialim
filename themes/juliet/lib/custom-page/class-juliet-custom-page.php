<?php

/**
 * This is a custom page class - In each custom page, users can 
 * create, update, and delete the custom page post types - 
 * feat image, feat slider, and triple images
 * Uses the Juliet_Custom_Page_Builder class to render a custom page.
 * 
 * @author Pix & Hue
 */

class Juliet_Custom_Page {
	public $cat_name = null;
	public $cat_id = null;
	public $page_name = 'none';
	public $page_slug = 'none';
	public $fields = array();

	const user_capability = 'edit_theme_options';
	
	function __construct($cat, $page_name, $page_slug, $fields ){

		$this->cat_name = $cat;
		$this->page_name = $page_name;
		$this->page_slug = $page_slug;
		$this->fields = $fields;

		if(get_category_by_slug($this->cat_name)) {
			$this->cat_id = get_category_by_slug($this->cat_name)->term_id;
		}

		$this->_juliet_add_actions();
	}

	protected function _juliet_add_actions(){
		add_action('admin_menu', array($this, 'juliet_add_to_menu'));
	}

	/*****************************************************************************
 	* Create Sub Menu Page for Each Promo Feature Type
	****************************************************************************/
	public function juliet_add_to_menu(){
		add_theme_page($this->page_name, $this->page_name, self::user_capability, $this->page_slug, array($this, 'juliet_init_page_manager') );
	}

	/*****************************************************************************
 	* Callback function to display the Sub Menu Page 
	****************************************************************************/
	public function juliet_init_page_manager() {
		$custom_page_builder = new Juliet_Custom_Page_Builder($this); 
		$custom_page_builder->juliet_build_custom_page();
	}
}