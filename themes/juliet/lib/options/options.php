<?php 

/**
 * Renders the primary theme options page (with tabs)
 * Uses Wordpress Settings API
 * Sections & Settings Registered in init-options.php
 *
 * @author Pix & Hue
 */

/*****************************************************************************
 * Render the Primary Theme Options Page - Nav Menu & Tabbed Pages
****************************************************************************/
if ( !function_exists('juliet_render_options_page') )  {

	function juliet_render_options_page() { ?>

		<div id="juliet_options_box">
           	
           	<div id="juliet_options_nav">
				<ul>
					<li id="tab1"><a href="#">General Options</a></li>
					<li id="tab2"><a href="#">Social Media</a></li>
					<li id="tab3"><a href="#">Page Settings</a></li>
					<li id="tab4"><a href="#">Archive Settings</a></li>
					<li id="tab5"><a href="#">Post Settings</a></li>
					<li id="tab6"><a href="#">Shop Settings</a></li>
	           	</ul>
	        </div>

			<form id="juliet-options-form" action="options.php" method="POST">	          

		        <div id="juliet_options_container">
		        	<div class="juliet_options_overlay"></div>
	        		 <!--General Settings -->
			        <div id="tab1" class="options-tab active">   	
			           <?php settings_fields( 'juliet_general_theme_options' ); ?>
			           <?php do_settings_sections( 'juliet_general_theme_options' ); ?>
					</div> 
		       
			        <!--Social Media Settings -->
			        <div id="tab2" class="options-tab">   	
			           <?php settings_fields( 'juliet_social_media_options' ); ?>
			           <?php do_settings_sections( 'juliet_social_media_options' ); ?>
					</div> 

					<!--Page Settings -->
					<div id="tab3" class="options-tab">
						<?php settings_fields( 'juliet_blog_options' ); ?>
			           	<?php do_settings_sections( 'juliet_blog_options' ); ?>
					</div> 

					<!--Archive Settings -->
					<div id="tab4" class="options-tab">
						<?php settings_fields( 'juliet_archive_options' ); ?>
			           	<?php do_settings_sections( 'juliet_archive_options' ); ?>
					</div> 

					<!--Post Settings -->
					<div id="tab5" class="options-tab">
						<?php settings_fields( 'juliet_post_options' ); ?>
			           	<?php do_settings_sections( 'juliet_post_options' ); ?>
					</div> 
					
					<!--Shop Settings -->
					<div id="tab6" class="options-tab">
						<?php settings_fields( 'juliet_shop_options' ); ?>
			           	<?php do_settings_sections( 'juliet_shop_options' ); ?>
					</div> 

				</div>	

				<!--Save all Settings -->
				<div id="juliet_options_footer">
					<?php submit_button(); ?>
					<span class="options-loader"></span>	
					<p id="juliet-options-save-reminder" class="options-message">You have made changes - Please select "Save Changes" to save settings.</p>
					<p id="juliet-options-success-message" class="options-message">All Settings Saved!</p>
					<p id="juliet-options-error-message" class="options-message">Error, please try again.</p>
            	</div>	         

        	</form>

        </div>	

          	<div id="juliet_theme_extras">
        	<div class="pixandhue-logo">
				<img src="<?php echo get_template_directory_uri() . '/images/defaults/pixandhue_logo.png';?>">
			</div>	
			<p>Thank you for purchasing the Juliet Theme!</p>
			<h3>Theme Tutorial</h3>
			<p>Your puchase includes a theme tutorial, which contains detailed instructions and screenshots to help with set up. Review the tutorial
			<a href="<?php echo esc_url('http://www.pixandhue.com/documentation/juliet/'); ?>" target="_blank">here</a>.</p>
			<h3>Customer Support</h3>
			<p>If you are happy with the Juliet theme, please take a moment to rate us <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> 
				on <a href="<?php echo esc_url('https://themeforest.net/item/juliet-a-feminine-blog-shop-theme-for-wordpress/17625325');?>" target="_blank">ThemeForest</a>. We appreciate your support!</p>
			<p>If you have any questions about the theme, please do not hesitate to contact our Customer Support. We love to hear from our customers!</p>

        </div>  

        <?php
	}
}