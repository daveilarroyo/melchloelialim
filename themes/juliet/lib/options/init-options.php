<?php

/**
 * Register the Sections & Settings of the Theme Options
 * Uses Wordpress Settings API
 * For the related Option functions: refer to functions/function-options.php
 *
 * @author Pix & Hue
 */


/*****************************************************************************
 * Register the Setting Page & Fields
****************************************************************************/
add_action('admin_init', 'juliet_init_theme_options');

if ( !function_exists('juliet_init_theme_options') )  {

	function juliet_init_theme_options() {

		$option_callback = 'juliet_render_option_field';

		/*****************************************************************************
 		* General Theme Options
		****************************************************************************/
		if(get_option( 'juliet_general_theme_options' ) === false) {  
        	add_option( 'juliet_general_theme_options' );
    	}

    	add_settings_section('juliet_general_options_section', 'General Options', 'juliet_theme_options_callback', 'juliet_general_theme_options' );

    	$theme_option_name = 'juliet_general_theme_options';
    	$theme_options_section = 'juliet_general_options_section';
        $option_description = 'Customize the General Settings for the Juliet Theme.';

    	$theme_options_array = array(
    		array('id' => 'site_logo', 'title' => 'Upload Logo', 'type' => 'upload', 'height' => 100, 'width' => 400),
            array('id' => 'top_bar', 'title' => 'Enable Top Bar', 'type' => 'checkbox'),
            array('id' => 'menu_position', 'title' => 'Enable Fixed Menu', 'type' => 'checkbox'),
            array('id' => 'left_sidebar', 'title' => 'Enable Left Sidebar', 'type' => 'checkbox'),
            array('id' => 'back_to_top', 'title' => 'Enable Back To Top Button', 'type' => 'checkbox'),
            array('id' => 'disable_loading_gif', 'title' => 'Disable Loading Gif', 'type' => 'checkbox'),
            array('id' => 'disable_responsive', 'title' => 'Disable Responsive', 'type' => 'checkbox'),
            array('id' => 'affiliate_text', 'title' => '"Shop The Look" Text', 'type' => 'text'),
            array('id' => 'copyright_text', 'title' => 'Copyright Text', 'type' => 'text'),
    	);

    	foreach($theme_options_array as $theme_option) {
    		juliet_create_settings_field($theme_option, $option_callback, $theme_option_name, $theme_options_section);
    	}

    	register_setting('juliet_general_theme_options', 'juliet_general_theme_options');
	 
	   	/*****************************************************************************
 		* Social Media
		****************************************************************************/
	    if(get_option( 'juliet_social_media_options' ) === false) {  
        	add_option( 'juliet_social_media_options' );
    	}

	    add_settings_section('juliet_social_media_section', 'Social Media', 'juliet_theme_options_callback', 'juliet_social_media_options' );

	    $social_media_option = 'juliet_social_media_options';
	    $social_media_section = 'juliet_social_media_section';

	    $social_media_array = array(
	    	array('id' => 'facebook', 'title' => 'Facebook', 'type' => 'text'),
	    	array('id' => 'twitter', 'title' => 'Twitter', 'type' => 'text'),
	    	array('id' => 'instagram', 'title' => 'Instagram', 'type' => 'text'),
	    	array('id' => 'pinterest', 'title' => 'Pinterest', 'type' => 'text'),
	    	array('id' => 'bloglovin', 'title' => 'Bloglovin', 'type' => 'text'),
	    	array('id' => 'google_plus', 'title' => 'Google Plus', 'type' => 'text'),
	    	array('id' => 'tumblr', 'title' => 'Tumblr', 'type' => 'text'),
            array('id' => 'youtube', 'title' => 'YouTube', 'type' => 'text'),
	    	array('id' => 'vimeo', 'title' => 'Vimeo', 'type' => 'text'),
	    	array('id' => 'linked_in', 'title' => 'LinkedIn', 'type' => 'text'),
	    	array('id' => 'rss', 'title' => 'RSS', 'type' => 'text'),
	    );

	    foreach($social_media_array as $social_option) {
	    	juliet_create_settings_field($social_option, $option_callback, $social_media_option, $social_media_section);
	    }

	    register_setting('juliet_social_media_options', 'juliet_social_media_options', 'juliet_validate_social_media');

	    /*****************************************************************************
 		* Blog Page Settings
		****************************************************************************/
		if(get_option( 'juliet_blog_options' ) === false) {  
        	add_option( 'juliet_blog_options' );
    	}

    	add_settings_section('juliet_blog_section', 'Page Settings', 'juliet_theme_options_callback', 'juliet_blog_options' );

    	$blog_settings_array = array(
    		array('id' => 'blog_hide_featured_image', 'title' =>  'Hide Featured Image at Top of Post', 'type' => 'checkbox'),
            array('id' =>'blog_enable_pinterest', 'title' =>'Enable Pinterest Button', 'type' => 'checkbox'),
            array('id' =>'blog_hide_cat', 'title' =>'Hide Post Categories', 'type' => 'checkbox'),
            array('id' =>'blog_hide_date', 'title' =>'Hide Post Date', 'type' => 'checkbox'),
    		array('id' =>'blog_hide_excerpt', 'title' =>'Show Full Post Content', 'type' => 'checkbox'),
            array('id' =>'blog_hide_comment_count', 'title' =>'Hide Comment Count', 'type' => 'checkbox'),
    		array('id' =>'blog_hide_share_buttons', 'title' =>'Hide Share Buttons', 'type' => 'checkbox'),
    	);

    	$blog_page = 'juliet_blog_options';
    	$blog_section = 'juliet_blog_section';

    	foreach($blog_settings_array as $blog_setting) {
	    	juliet_create_settings_field($blog_setting, $option_callback, $blog_page, $blog_section);
	    }

    	register_setting('juliet_blog_options', 'juliet_blog_options');

        /*****************************************************************************
        * Archive Settings
        ****************************************************************************/
        if(get_option( 'juliet_archive_options' ) === false) {  
            add_option( 'juliet_archive_options' );
        }

        add_settings_section('juliet_archive_section', 'Archive Settings', 'juliet_theme_options_callback', 'juliet_archive_options' );

        $archive_settings_array = array(
            array('id' => 'archive_layout', 'title' => 'Select Layout', 'type' => 'select', 'options'=> array(array('id'=>'default', 'name'=>'Default Layout'), array('id'=>'block', 'name'=>'Block Layout'), 
                  array('id'=>'gallery', 'name'=>'Gallery Layout'))),
            array('id' => 'archive_gal_options', 'title' => 'Gallery Options', 'type' => 'select', 'options'=> array(array('id'=>'no-overlay', 'name'=>'No Rollover Effect'), 
                  array('id'=>'gal-hover-overlay', 'name'=>'Color Background + Post Title'), array('id'=>'gal-pin-button', 'name'=>'Pin It Button'))),
            array('id' =>'archive_page_width', 'title' =>'Enable Sidebar', 'type' => 'checkbox'),
        );    

        $archive_page = 'juliet_archive_options';
        $archive_section = 'juliet_archive_section';

        foreach($archive_settings_array as $archive_setting) {
            juliet_create_settings_field($archive_setting, $option_callback, $archive_page, $archive_section);
        }

        register_setting('juliet_archive_options', 'juliet_archive_options');
    	
	    /*****************************************************************************
 		* Post Settings
		****************************************************************************/
		if(get_option( 'juliet_post_options' ) === false) {  
        	add_option( 'juliet_post_options' );
    	}

    	add_settings_section('juliet_post_section', 'Post Settings', 'juliet_theme_options_callback', 'juliet_post_options' );

    	$post_settings_array = array(
    		array('id' => 'enable_post_sidebar', 'title' => 'Enable Sidebar on Posts', 'type' => 'checkbox'), 
            array('id' => 'enable_pinterest', 'title' =>  'Enable Pinterest Button', 'type' => 'checkbox'), 
    		array('id' => 'hide_featured_image', 'title' =>  'Hide Featured Image from Top of Post', 'type' => 'checkbox'), 
            array('id' =>'hide_date', 'title' =>'Hide Date', 'type' => 'checkbox'),
    		array('id' =>'hide_cat', 'title' =>'Hide Category', 'type' => 'checkbox'),
    		array('id' =>'hide_tags', 'title' =>'Hide Tags', 'type' => 'checkbox'),
            array('id' =>'hide_comment_count', 'title' =>'Hide Comment Count', 'type' => 'checkbox'),
            array('id' =>'hide_share_buttons', 'title' =>'Hide Share Buttons', 'type' => 'checkbox'),
            array('id' =>'hide_pagination', 'title' =>'Hide Pagination', 'type' => 'checkbox'),
    		array('id' =>'hide_author', 'title' =>'Hide Author Box', 'type' => 'checkbox'),		
    		array('id' =>'hide_related_posts', 'title' =>'Hide Related Posts', 'type' => 'checkbox')
    	);

    	$post_page = 'juliet_post_options';
    	$post_section = 'juliet_post_section';


	    foreach($post_settings_array as $post_setting) {
	    	juliet_create_settings_field($post_setting, $option_callback, $post_page, $post_section);
	    }

    	register_setting('juliet_post_options', 'juliet_post_options');

         /*****************************************************************************
        * Shop Settings
        ****************************************************************************/
        if(get_option( 'juliet_shop_options' ) === false) {  
            add_option( 'juliet_shop_options' );
        }

        add_settings_section('juliet_shop_section', 'Shop Settings', 'juliet_theme_options_callback', 'juliet_shop_options' );

        $post_settings_array = array(
            array('id' => 'enable_woo_cart_icon', 'title' =>  'Enable Shopping Cart Icon', 'type' => 'checkbox'), 
            array('id' => 'enable_woo_sidebar', 'title' => 'Enable WooCommerce Sidebar', 'type' => 'checkbox'), 
            array('id' => 'hide_woo_related_products', 'title' => 'Hide Related Products', 'type' => 'checkbox'), 
            array('id' => 'woo_items_per_page', 'title' => 'Number of Items on Shop Page', 'type' => 'text'),
        );

        $post_page = 'juliet_shop_options';
        $post_section = 'juliet_shop_section';

        foreach($post_settings_array as $post_setting) {
            juliet_create_settings_field($post_setting, $option_callback, $post_page, $post_section);
        }

        register_setting('juliet_shop_options', 'juliet_shop_options');
	}
}