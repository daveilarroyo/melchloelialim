<?php
/**
 * Plugin Name: Social Widget
 */

class juliet_social_widget extends WP_Widget {


	/**
	 * Widget setup.
	 */
	function __construct() {

		// Widget settings.
		$widget_ops = array( 'classname' => 'juliet_social_widget', 'description' => esc_html__('A widget that displays social media icons.', 'juliet') );

		// Widget control settings. 
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'juliet_social_widget' );

		//Create the Widget 
		parent::__construct( 'juliet_social_widget', esc_html__('Juliet: Social Media', 'juliet'), $widget_ops, $control_ops);
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$facebook = isset( $instance['facebook'] ) ? esc_attr( $instance['facebook'] ) : '';
		$twitter = isset( $instance['twitter'] ) ? esc_attr( $instance['twitter'] ) : '';
		$instagram = isset( $instance['instagram'] ) ? esc_attr( $instance['instagram'] ) : '';
		$pinterest = isset( $instance['pinterest'] ) ? esc_attr( $instance['pinterest'] ) : '';
		$googleplus = isset( $instance['googleplus'] ) ? esc_attr( $instance['googleplus'] ) : '';
		$tumblr = isset( $instance['tumblr'] ) ? esc_attr( $instance['tumblr'] ) : '';
		$youtube = isset( $instance['youtube'] ) ? esc_attr( $instance['youtube'] ) : '';
		$vimeo = isset( $instance['vimeo'] ) ? esc_attr( $instance['vimeo'] ) : '';
		$bloglovin = isset( $instance['bloglovin'] ) ? esc_attr( $instance['bloglovin'] ) : '';
		$linkedin = isset( $instance['linkedin'] ) ? esc_attr( $instance['linkedin'] ) : '';
		$rss = isset( $instance['rss'] ) ? esc_attr( $instance['rss'] ) : '';
	
		
		/* Before widget (defined by themes). */
		echo wp_kses_post($before_widget);

		$juliet_social_options = get_option('juliet_social_media_options');

		?>	
			<div class="juliet-social-widget">
				<?php if($facebook) : ?><a href="<?php echo esc_url('http://facebook.com/' . $juliet_social_options['juliet_facebook']); ?>" target="_blank"><i class="fa fa-facebook"></i></a><?php endif; ?>
				<?php if($instagram) : ?><a href="<?php echo esc_url('http://instagram.com/' . $juliet_social_options['juliet_instagram']);?>" target="_blank"><i class="fa fa-instagram"></i></a><?php endif; ?>
				<?php if($pinterest) : ?><a href="<?php echo esc_url('http://pinterest.com/' . $juliet_social_options['juliet_pinterest']);?>" target="_blank"><i class="fa fa-pinterest-p"></i></a><?php endif; ?>
				<?php if($twitter) : ?><a href="<?php echo esc_url('http://twitter.com/' . $juliet_social_options['juliet_twitter']);?>" target="_blank"><i class="fa fa-twitter"></i></a><?php endif; ?>
				<?php if($googleplus) : ?><a href="<?php echo esc_url('http://plus.google.com/' . $juliet_social_options['juliet_google_plus']);?>" target="_blank"><i class="fa fa-google-plus"></i></a><?php endif; ?>
				<?php if($bloglovin) : ?><a href="<?php echo esc_url('http://bloglovin.com/' . $juliet_social_options['juliet_bloglovin']);?>" target="_blank"><i class="fa fa-heart"></i></a><?php endif; ?>
				<?php if($tumblr) : ?><a href="<?php echo esc_url('http://' . $juliet_social_options['juliet_tumblr'] . '.tumblr.com/'); ?>" target="_blank"><i class="fa fa-tumblr"></i></a><?php endif; ?>
				<?php if($youtube) : ?><a href="<?php echo esc_url($juliet_social_options['juliet_youtube']); ?>" target="_blank"><i class="fa fa-youtube-play"></i></a><?php endif; ?>
				<?php if($vimeo) : ?><a href="<?php echo esc_url('http://vimeo.com/' . $juliet_social_options['juliet_vimeo']);?>" target="_blank"><i class="fa fa-vimeo"></i></a><?php endif; ?>
				<?php if($linkedin) : ?><a href="<?php echo esc_url($juliet_social_options['juliet_linked_in']);?>" target="_blank"><i class="fa fa-linkedin"></i></a><?php endif; ?>
				<?php if($rss) : ?><a href="<?php echo esc_url($juliet_social_options['juliet_rss']);?>" target="_blank"><i class="fa fa-rss"></i></a><?php endif; ?> 
			</div>
			
		<?php

		/* After widget (defined by themes). */
		echo wp_kses_post($after_widget);
	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		//$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['facebook'] = strip_tags( $new_instance['facebook'] );
		$instance['twitter'] = strip_tags( $new_instance['twitter'] );
		$instance['instagram'] = strip_tags( $new_instance['instagram'] );
		$instance['pinterest'] = strip_tags( $new_instance['pinterest'] );
		$instance['googleplus'] = strip_tags( $new_instance['googleplus'] );
		$instance['tumblr'] = strip_tags( $new_instance['tumblr'] );
		$instance['youtube'] = strip_tags( $new_instance['youtube'] );
		$instance['vimeo'] = strip_tags( $new_instance['vimeo'] );
		$instance['bloglovin'] = strip_tags( $new_instance['bloglovin'] );
		$instance['linkedin'] = strip_tags( $new_instance['linkedin'] );
		$instance['rss'] = strip_tags( $new_instance['rss'] );

		return $instance;
	}

		function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array('facebook' => 'on', 'twitter' => 'on', 'instagram' => 'on', 'pinterest' => '', 'googleplus' => '', 'tumblr' => '', 'vimeo' => '', 'youtube' => '',
							'bloglovin' => '', 'linkedin' => '', 'rss' => '');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>Note: Set your social links in Appearance > Juliet Options > Social Media </p>
		
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'facebook' )); ?>">Show Facebook:</label>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'facebook' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'facebook' )); ?>" <?php checked( (bool) $instance['facebook'], true ); ?> />
		</p>
		
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'twitter' )); ?>">Show Twitter:</label>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'twitter' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'twitter' )); ?>" <?php checked( (bool) $instance['twitter'], true ); ?> />
		</p>
		
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'instagram' )); ?>">Show Instagram:</label>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'instagram' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'instagram' )); ?>" <?php checked( (bool) $instance['instagram'], true ); ?> />
		</p>
		
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'pinterest' )); ?>">Show Pinterest:</label>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'pinterest' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'pinterest' )); ?>" <?php checked( (bool) $instance['pinterest'], true ); ?> />
		</p>
		
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'googleplus' )); ?>">Show Google Plus:</label>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'googleplus' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'googleplus' )); ?>" <?php checked( (bool) $instance['googleplus'], true ); ?> />
		</p>
		
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'tumblr' )); ?>">Show Tumblr:</label>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'tumblr' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'tumblr' )); ?>" <?php checked( (bool) $instance['tumblr'], true ); ?> />
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'youtube' )); ?>">Show YouTube:</label>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'youtube' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'youtube' )); ?>" <?php checked( (bool) $instance['youtube'], true ); ?> />
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'vimeo' )); ?>">Show Vimeo:</label>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'vimeo' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'vimeo' )); ?>" <?php checked( (bool) $instance['vimeo'], true ); ?> />
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'bloglovin' )); ?>">Show Bloglovin:</label>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'bloglovin' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bloglovin' )); ?>" <?php checked( (bool) $instance['bloglovin'], true ); ?> />
		</p>
		
		
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'linkedin' )); ?>">Show Linkedin:</label>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'linkedin' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'linkedin' )); ?>" <?php checked( (bool) $instance['linkedin'], true ); ?> />
		</p>
		
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'rss' )); ?>">Show RSS:</label>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'rss' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'rss' )); ?>" <?php checked( (bool) $instance['rss'], true ); ?> />
		</p>

	<?php
	}

}

add_action( 'widgets_init', 'juliet_register_social_widget' );

function juliet_register_social_widget() {
	
	register_widget( 'juliet_social_widget' );
} ?>