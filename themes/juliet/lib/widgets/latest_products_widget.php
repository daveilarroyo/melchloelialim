<?php
/**
 * Plugin Name: Latest Products Widget
 */

class juliet_latest_products_widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function __construct() {

		// Widget settings.
		$widget_ops = array( 'classname' => 'juliet_latest_products_widget', 'description' => esc_html__('Display your latest products.', 'juliet') );

		// Widget control settings. 
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'juliet_latest_products_widget' );

		//Create the Widget 
		parent::__construct( 'juliet_latest_products_widget', esc_html__('Juliet: Latest Products', 'juliet'), $widget_ops, $control_ops);
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );

		/* Before widget (defined by themes). */
		echo wp_kses_post($before_widget); ?>

			<div class="juliet-latest-products-widget">

				<?php /* Display the widget title if one was input (before and after defined by themes). */

				if ( $title ) {
					echo wp_kses_post($before_title . $title . $after_title);
				}

				$i = 1;

				$product_title = 'product_title_' . ($i); 
				$product_link = 'product_link_' . ($i);
				$product_image = 'image_' . ($i);
				$alt_desc = isset($product_title) ? $product_title : $title;

				while( $i < 4 && (($instance[$product_title]) || ($instance[$product_image]))) { ?>

					<div class="juliet-latest-product-item">

						<?php if($instance[$product_link]) { ?>
							<a href="<?php echo esc_url($instance[$product_link]); ?>" target="_blank">
						<?php } ?>

							<?php if($instance[$product_image]) { ?>
								<div class="juliet-product-image">
									<img src="<?php echo esc_url($instance[$product_image]); ?>" alt="<?php echo esc_attr($alt_desc); ?>" />
								</div>
							<?php } ?>
							

							<?php if($instance[$product_title]) { ?>
								<p class="juliet-latest-product-title"><?php echo esc_html($instance[$product_title]); ?></p>
							<?php } ?>	

						<?php if($instance[$product_link]) { ?>
							</a>
						<?php } ?>
		
					</div>

					<?php 

					$i++;

					if ($i < 4) {
						$product_title = 'product_title_' . ($i); 
						$product_link = 'product_link_' . ($i);
						$product_image = 'image_' . ($i);
						$alt_desc = isset($product_title) ? $product_title : $title;
					}	
					
				}	?>

			</div> <!--End Widget-->
			
		<?php

		/* After widget (defined by themes). */
		echo wp_kses_post($after_widget);
	}

		/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['product_title_1'] = $new_instance['product_title_1'];
		$instance['product_link_1'] = strip_tags( $new_instance['product_link_1'] );
		$instance['image_1'] = strip_tags( $new_instance['image_1'] );
		$instance['product_title_2'] = $new_instance['product_title_2'];
		$instance['product_link_2'] = strip_tags( $new_instance['product_link_2'] );
		$instance['image_2'] = strip_tags( $new_instance['image_2'] );
		$instance['product_title_3'] = $new_instance['product_title_3'];
		$instance['product_link_3'] = strip_tags( $new_instance['product_link_3'] );
		$instance['image_3'] = strip_tags( $new_instance['image_3'] );
	
		return $instance;
	}


	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'Latest Products', 'product_title_1' => '', 'product_link_1' => '', 'image_1' => '',
			                'product_title_2' => '', 'product_link_2' => '', 'image_2' => '', 'product_title_3' => '', 'product_link_3' => '', 'image_3' => '');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<div id="latest_products_widget_form" class="juliet_widget">

			<!-- Widget Title: Text Input -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>">Widget Title:</label><br />
				<input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>"/>
			</p>

			<?php for($i = 1; $i < 4; $i++) { ?>

				<h4>Product #<?php echo esc_html($i); ?></h4>

				<?php 
				$product_title = 'product_title_' . ($i); 
				$product_link = 'product_link_' . ($i);
				$product_image = 'image_' . ($i)
				?>
		
				<!-- Product Title -->
				<p>
					<label for="<?php echo esc_attr($this->get_field_id( $product_title )); ?>">Product Title</label><br />
					<input id="<?php echo esc_attr($this->get_field_id( $product_title )); ?>" name="<?php echo esc_attr($this->get_field_name( $product_title )); ?>" value="<?php echo esc_attr($instance[$product_title]); ?>" /><br />
				</p>

				<!-- Product Link -->
				<p>
					<label for="<?php echo esc_attr($this->get_field_id( $product_link )); ?>">Product Link</label><br />
					<input id="<?php echo esc_attr($this->get_field_id(  $product_link )); ?>" name="<?php echo esc_attr($this->get_field_name( $product_link )); ?>" value="<?php echo esc_attr($instance[$product_link]); ?>" /><br />
				</p>

				<!-- Product Image URL -->
				<p>
					<label for="<?php echo esc_attr($this->get_field_id( $product_image  )); ?>">Product Image URL</label><br />
					<input id="<?php echo esc_attr($this->get_field_id( $product_image  )); ?>" name="<?php echo esc_attr($this->get_field_name( $product_image  )); ?>" value="<?php echo esc_attr($instance[$product_image]); ?>" /><br />
				</p>
			
			<?php } ?>
			
		</div>	

	<?php
	}
}

add_action( 'widgets_init', 'juliet_register_latest_products_widget' );

function juliet_register_latest_products_widget() {
	
	register_widget( 'juliet_latest_products_widget' );
} ?>