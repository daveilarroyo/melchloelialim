<?php
/**
 * Plugin Name: Custom List Widget
 */

class juliet_custom_list_widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function __construct() {

		// Widget settings.
		$widget_ops = array( 'classname' => 'juliet_custom_list_widget', 'description' => esc_html__('A widget that displays a custom list.', 'juliet') );

		// Widget control settings. 
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'juliet_custom_list_widget' );

		//Create the Widget 
		parent::__construct( 'juliet_custom_list_widget', esc_html__('Juliet: Custom List', 'juliet'), $widget_ops, $control_ops);
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );

		/* Before widget (defined by themes). */
		echo wp_kses_post($before_widget); ?>

			<div class="juliet-custom-list-widget">

				<?php /* Display the widget title if one was input (before and after defined by themes). */

				if ( $title) {
					echo wp_kses_post($before_title . $title . $after_title);
				}

				$i = 1;

				$item_title = 'item_title_' . ($i); 
				$item_link = 'item_link_' . ($i);

				while($i < 11 && $instance[$item_title]) { ?>

					<div class="juliet-custom-list-item">

						<?php if($instance[$item_link]) { ?>
							<a href="<?php echo esc_url($instance[$item_link]); ?>" target="_blank">
						<?php } ?>					

							<?php if($instance[$item_title]) { ?>
								<p class="juliet-custom-list-title"><?php echo esc_html($instance[$item_title]); ?></p>
							<?php } ?>	

						<?php if($instance[$item_link]) { ?>
							</a>
						<?php } ?>
		
					</div>

					<?php $i++;
					$item_title = 'item_title_' . ($i); 
					$item_link = 'item_link_' . ($i);
					
				}	?>

			</div> <!--End Widget-->
			
		<?php

		/* After widget (defined by themes). */
		echo wp_kses_post($after_widget);
	}

		/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		
		$instance['item_title_1'] = $new_instance['item_title_1'];
		$instance['item_link_1'] = strip_tags( $new_instance['item_link_1'] );
		$instance['item_title_2'] = $new_instance['item_title_2'];
		$instance['item_link_2'] = strip_tags( $new_instance['item_link_2'] );
		$instance['item_title_3'] = $new_instance['item_title_3'];
		$instance['item_link_3'] = strip_tags( $new_instance['item_link_3'] );
		$instance['item_title_4'] = $new_instance['item_title_4'];
		$instance['item_link_4'] = strip_tags( $new_instance['item_link_4'] );
		$instance['item_title_5'] = $new_instance['item_title_5'];
		$instance['item_link_5'] = strip_tags( $new_instance['item_link_5'] );
		$instance['item_title_6'] = $new_instance['item_title_6'];
		$instance['item_link_6'] = strip_tags( $new_instance['item_link_6'] );
		$instance['item_title_7'] = $new_instance['item_title_7'];
		$instance['item_link_7'] = strip_tags( $new_instance['item_link_7'] );
		$instance['item_title_8'] = $new_instance['item_title_8'];
		$instance['item_link_8'] = strip_tags( $new_instance['item_link_8'] );
		$instance['item_title_9'] = $new_instance['item_title_9'];
		$instance['item_link_9'] = strip_tags( $new_instance['item_link_9'] );
		$instance['item_title_10'] = $new_instance['item_title_10'];
		$instance['item_link_10'] = strip_tags( $new_instance['item_link_10'] );
	
		return $instance;
	}


	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => '', 'item_title_1' => '', 'item_link_1' => '', 'item_title_2' => '', 'item_link_2' => '', 'item_title_3' => '', 'item_link_3' => '',
						   'item_title_4' => '', 'item_link_4' => '', 'item_title_5' => '', 'item_link_5' => '', 'item_title_6' => '', 'item_link_6' => '',
						   'item_title_7' => '', 'item_link_7' => '', 'item_title_8' => '', 'item_link_8' => '', 'item_title_9' => '', 'item_link_9' => '',
						    'item_title_10' => '', 'item_link_10' => '' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<div id="custom_list_widget_form" class="juliet_widget">

			<!-- Widget Title: Text Input -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>">Widget Title:</label><br />
				<input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>"/>
			</p>

			<?php for($i = 1; $i < 11; $i++) { ?>

				<h4>List Item #<?php echo esc_html($i); ?></h4>

				<?php 
				$item_title = 'item_title_' . ($i); 
				$item_link = 'item_link_' . ($i);
				?>
		
				<!-- List Item Title -->
				<p>
					<label for="<?php echo esc_attr($this->get_field_id( $item_title )); ?>">Item Title</label><br />
					<input id="<?php echo esc_attr($this->get_field_id( $item_title )); ?>" name="<?php echo esc_attr($this->get_field_name( $item_title )); ?>" value="<?php echo esc_attr($instance[$item_title]); ?>" /><br />
				</p>

				<!-- List Item Link -->
				<p>
					<label for="<?php echo esc_attr($this->get_field_id( $item_link )); ?>">Item Link</label><br />
					<input id="<?php echo esc_attr($this->get_field_id(  $item_link )); ?>" name="<?php echo esc_attr($this->get_field_name( $item_link )); ?>" value="<?php echo esc_attr($instance[$item_link]); ?>" /><br />
				</p>
			
			<?php } ?>
			
		</div>	

	<?php
	}
}

add_action( 'widgets_init', 'juliet_register_custom_list_widget' );

function juliet_register_custom_list_widget() {
	
	register_widget( 'juliet_custom_list_widget' );
} ?>