<?php
/**
 * Plugin Name: About Widget
 */

class juliet_about_widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function __construct() {

		// Widget settings.
		$widget_ops = array( 'classname' => 'juliet_about_widget', 'description' => esc_html__('Author image and description.', 'juliet') );

		// Widget control settings. 
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'juliet_about_widget' );

		//Create the Widget 
		parent::__construct( 'juliet_about_widget', esc_html__('Juliet: About Me', 'juliet'), $widget_ops, $control_ops);
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$image = $instance['image'];
		$description = $instance['description'];

		/* Before widget (defined by themes). */
		echo wp_kses_post($before_widget);

		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title )
			echo wp_kses_post($before_title . $title . $after_title); ?>

			<div class="juliet-about-widget">
				<?php if($image) { ?>
					<div class="juliet-about-img">
						<img src="<?php echo esc_url($image); ?>" alt="<?php echo esc_attr($title); ?>" />
					</div>

				<?php }
				if($description) { ?>
					<p><?php echo wp_kses_post($description); ?></p>
				<?php } ?>
			</div>	

		<?php 

		/* After widget (defined by themes). */
		echo wp_kses_post($after_widget);
	}

		/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['image'] = strip_tags( $new_instance['image'] );
		$instance['description'] = $new_instance['description'];

		return $instance;
	}


	function form( $instance ) {

	/* Set up some default widget settings. */
		$defaults = array( 'title' => 'About Me', 'image' => '', 'subtitle' => '', 'description' => '');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>">Title:</label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>"/>
		</p>
		
		<!-- image url -->
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'image' )); ?>">Image URL:</label>
			<input id="<?php echo esc_attr($this->get_field_id( 'image' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'image' )); ?>" value="<?php echo esc_url($instance['image']); ?>" /><br />
		</p>
		
		<!-- description -->
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'description' )); ?>">About me text:</label>
			<textarea id="<?php echo esc_attr($this->get_field_id( 'description' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'description' )); ?>" style="width:95%;" rows="6"><?php echo esc_textarea($instance['description']); ?></textarea>
		</p>

	<?php
	}
}

add_action( 'widgets_init', 'juliet_register_about_widget' );

function juliet_register_about_widget() {
	
	register_widget( 'juliet_about_widget' );
} ?>