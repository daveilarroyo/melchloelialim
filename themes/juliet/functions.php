<?php
/**
 * Functions
 *
 * This is the main functions file for the theme.
 */

/*****************************************************************************
 * Set up the Juliet Theme, including menu support ....
****************************************************************************/
add_action( 'init', 'juliet_register_theme_menu' );

if ( !function_exists('juliet_register_theme_menu') ) {

	function juliet_register_theme_menu() {

		// Register navigation menu
		register_nav_menus(
			array(
				'main-menu' => esc_html__( 'Primary Menu', 'juliet' ),
			)
		);
	}
}	

add_action( 'after_setup_theme', 'juliet_theme_setup' );

if ( !function_exists('juliet_theme_setup') ) {

	function juliet_theme_setup() {
	
		// Localization support
		load_theme_textdomain('juliet', get_template_directory() . '/lang');
		
		// Post formats
		add_theme_support( 'post-formats', array( 'gallery', 'video', 'audio' ) );
		
		// Featured image
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'juliet-full-thumb', 1000, 0, true );
		add_image_size( 'juliet-featured-thumb', 1700, 0, true );
		add_image_size( 'juliet-misc-thumb', 520, 400, true );
		
		// Feed Links
		add_theme_support( 'automatic-feed-links' );

		// WooCommerce Support
		add_theme_support( 'woocommerce' );	
	}
}

/*****************************************************************************
 * Register & Enqueue styles and scripts
****************************************************************************/
add_action( 'wp_enqueue_scripts','juliet_load_scripts' );

if ( !function_exists('juliet_load_scripts') )  {

	function juliet_load_scripts() {
		
		// Enqueue scripts and styles
		wp_enqueue_style('juliet-style', get_template_directory_uri() . '/style.css');
		wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');

		//Enqueue Responsive Stylesheet based on theme options
		$juliet_general_options = get_option('juliet_general_theme_options');
		$disable_responsive = isset($juliet_general_options['juliet_disable_responsive']) ? $juliet_general_options['juliet_disable_responsive'] : false;
		
		if(!$disable_responsive) {
			wp_enqueue_style('juliet-responsive-css', get_template_directory_uri() . '/css/responsive.css');
		}
			
		wp_enqueue_script('juliet-theme-scripts', get_template_directory_uri() . '/js/theme_scripts.js', array('jquery'), '', true);
		wp_enqueue_script('juliet-scripts', get_template_directory_uri() . '/js/juliet.js', array('jquery'), '', true);
		
		// Fonts
		juliet_enqueue_google_fonts();

		if (is_singular() && get_option('thread_comments'))	wp_enqueue_script('comment-reply');
	}
}

/*****************************************************************************
 * Content Width
****************************************************************************/
if ( ! isset( $content_width ) )
	$content_width = 1080;

/*****************************************************************************
 * Theme Global Vars - Used for Theme Menu & Theme Options
****************************************************************************/
global $juliet_theme_data;
$juliet_theme_data = new stdClass();

global $juliet_gen_theme_options;
$juilet_gen_theme_options = array();

/*****************************************************************************
 * Load php files
****************************************************************************/
/** -- Lib Files -- **/
require get_template_directory() . '/lib/admin-scripts-and-styles.php';
require get_template_directory() . '/lib/custom-page/class-juliet-custom-page.php';
require get_template_directory() . '/lib/custom-page/class-juliet-custom-page-builder.php';
require get_template_directory() . '/lib/custom-page/class-juliet-custom-data-manager.php';
require get_template_directory() . '/lib/meta/meta.php';
require get_template_directory() . '/lib/options/init-options.php';
require get_template_directory() . '/lib/options/options.php';
require get_template_directory() . '/lib/utils/aq_resizer.php';

/* -- Widgets -- */
require get_template_directory() . '/lib/widgets/about_widget.php';
require get_template_directory() . '/lib/widgets/custom_list_widget.php';
require get_template_directory() . '/lib/widgets/facebook_widget.php';
require get_template_directory() . '/lib/widgets/latest_products_widget.php';
require get_template_directory() . '/lib/widgets/recent_posts_widget.php';
require get_template_directory() . '/lib/widgets/social_widget.php';

/** -- Functions Files -- **/
require get_template_directory() . '/functions/init-data.php';
require get_template_directory() . '/functions/functions-ajax.php';
require get_template_directory() . '/functions/functions-general.php';
require get_template_directory() . '/functions/functions-options.php';
require get_template_directory() . '/functions/functions-woocommerce.php';
require get_template_directory() . '/functions/customizer/juliet_customizer_settings.php';
require get_template_directory() . '/functions/customizer/juliet_customizer_style.php';

/*****************************************************************************
 * Register Sidebars
****************************************************************************/
add_action( 'widgets_init','juliet_register_sidebars' );

if ( !function_exists('juliet_register_sidebars') )  {

	function juliet_register_sidebars() {
		
		if ( function_exists('register_sidebar') ) {
			
			register_sidebar(array(
				'name' => esc_html__( 'Sidebar', 'juliet' ),
				'id' => 'juliet-sidebar',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h4 class="widget-title">',
				'after_title' => '</h4>',
			));

			register_sidebar(array(
				'name' => esc_html__( 'Instagram Footer', 'juliet' ),
				'id' => 'juliet-instagram-sidebar',
				'before_widget' => '<div id="%1$s" class="instagram-widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h4 class="instagram-title">',
				'after_title' => '</h4>',
				'description' => esc_html__('Use the "Instagram" widget. IMPORTANT: For best result set number of photos to 6 or 12.', 'juliet'),
			));
		}

		/* --- WooCommerce Sidebar --- */
		if (in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
			
			if ( function_exists('register_sidebar') ) {
				
				register_sidebar(array(
					'name' => esc_html__( 'WooCommerce Sidebar', 'juliet'),
					'id' => 'juliet-woocommerce-sidebar',
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h4 class="widget-title"><span>',
					'after_title' => '</span></h4>',
				));
			}
		}
	}
}		

/*****************************************************************************
 * Disable Theme Update Notifications
****************************************************************************/
add_filter( 'site_transient_update_themes', 'juliet_disable_theme_update_notification' );

function juliet_disable_theme_update_notification( $value ) {
    if ( isset( $value ) && is_object( $value ) ) {
        unset( $value->response['juliet'] );
    }
    return $value;
}

/*****************************************************************************
 * Add Required Plugins
****************************************************************************/
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme Juliet for publication on ThemeForest
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

/**
 * Load the TGM_Plugin_Activation class.
 */
require_once get_template_directory() . '/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'juliet_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function juliet_register_required_plugins() {

	/**
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		array(
			'name'     				=> esc_html__('Vafpress Post Formats UI', 'juliet'), // The plugin name
			'slug'     				=> 'vafpress-post-formats-ui-develop', // The plugin slug (typically the folder name)
			'source'   				=> get_template_directory() . '/plugins/vafpress-post-formats-ui-develop.zip', // The plugin source
			'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name'     				=> esc_html__('WP Instagram Widget', 'juliet'), // The plugin name
			'slug'     				=> 'wp-instagram-widget', // The plugin slug (typically the folder name)
			'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),

	);

	/**
	 * Array of configuration settings. Amend each line as needed.
	 * If you want the default strings to be available under your own theme domain,
	 * leave the strings uncommented.
	 * Some of the strings are added into a sprintf, so see the comments at the
	 * end of each line for what each argument will be.
	 */
	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'juliet',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		'strings'      => array(
			'page_title'                      => esc_html__( 'Install Required Plugins', 'juliet' ),
			'menu_title'                      => esc_html__( 'Install Plugins', 'juliet' ),
			/* translators: %s: plugin name. */
			'installing'                      => esc_html__( 'Installing Plugin: %s', 'juliet' ),
			/*translators: %s: plugin name. */
			'updating'                        => esc_html__( 'Updating Plugin: %s', 'juliet' ),
			'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'juliet' ),
			'notice_can_install_required'     => _n_noop(
				/* translators: 1: plugin name(s). */
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'juliet'
			),
			'notice_can_install_recommended'  => _n_noop(
				/* translators: 1: plugin name(s). */
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'juliet'
			),
			'notice_ask_to_update'            => _n_noop(
				/* translators: 1: plugin name(s). */
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'juliet'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				/* translators: 1: plugin name(s). */
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'juliet'
			),
			'notice_can_activate_required'    => _n_noop(
				/* translators: 1: plugin name(s). */
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'juliet'
			),
			'notice_can_activate_recommended' => _n_noop(
				/* translators: 1: plugin name(s). */
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'juliet'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'juliet'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'juliet'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'juliet'
			),
			'return'                          => esc_html__( 'Return to Required Plugins Installer', 'juliet' ),
			'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'juliet' ),
			'activated_successfully'          => esc_html__( 'The following plugin was activated successfully:', 'juliet' ),
			/* translators: 1: plugin name. */
			'plugin_already_active'           => esc_html__( 'No action taken. Plugin %1$s was already active.', 'juliet' ),
			/* translators: 1: plugin name. */
			'plugin_needs_higher_version'     => esc_html__( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'juliet' ),
			/* translators: 1: dashboard link. */
			'complete'                        => esc_html__( 'All plugins installed and activated successfully. %1$s', 'juliet' ),
			'dismiss'                         => esc_html__( 'Dismiss this notice', 'juliet' ),
			'notice_cannot_install_activate'  => esc_html__( 'There are one or more required or recommended plugins to install, update or activate.', 'juliet' ),
			'contact_admin'                   => esc_html__( 'Please contact the administrator of this site for help.', 'juliet' ),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
		
	);

	tgmpa( $plugins, $config );
}

?>