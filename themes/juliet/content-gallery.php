<?php 

/**
* Template for blog posts displayed on a page with Gallery Layout
* Used by: page-gallery.php
*/
global $juliet_gen_theme_options; global $juliet_theme_data;
$juliet_sidebar = $juliet_gen_theme_options['sidebar'];
$juliet_add_pin_it = $juliet_gen_theme_options['add_pin_it'];
$juliet_gal_overlay = $juliet_gen_theme_options['gal_overlay'];
$juliet_count = $juliet_theme_data->count;
$juliet_gal_type = $juliet_sidebar && $juliet_count % 2 == 0 || !$juliet_sidebar && $juliet_count % 3 == 0 ? 'juliet-gal-end' : '';
$juliet_classes = array('juliet-gallery-item', $juliet_gal_type); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class($juliet_classes); ?>>
	
	<?php if(has_post_thumbnail()) { 
		$image_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'juliet-full-thumb' );
		if($juliet_add_pin_it) {
			echo juliet_page_pin_it_button('<a href="' . esc_url(get_permalink()) . '"><div class="juliet-gallery-post-img juliet-pin-it-gal" style="background-image:url(' . esc_url($image_src[0]) .')"></div></a>', get_post_thumbnail_id());
		} else { ?>
				<a href="<?php echo esc_url(get_permalink()); ?>">
					<div class="juliet-gallery-post-img" style="background-image:url(<?php echo esc_url($image_src[0]); ?>)"></div>
				</a>	
		<?php }
	} else if(!has_post_thumbnail()) { ?>
		<a href="<?php echo esc_url(get_permalink()); ?>">
			<div class="juliet-gallery-post-img">
				<div class="juliet-gallery-img-overlay juliet-no-rollover-gal">
					<div class="juliet-gallery-title">
						<?php if(get_the_title() != '') { ?>
							<div class="juliet-gallery-title-border"><h2><?php the_title(); ?></h2></div>
						<?php } ?>			
					</div>
				</div>		
			</div>
		</a>
	<?php }

	if($juliet_gal_overlay && has_post_thumbnail()) { ?>	
		<a href="<?php echo esc_url(get_permalink()); ?>">
			<div class="juliet-gallery-img-overlay juliet-rollover">
				<div class="juliet-gallery-title">
					<?php if(get_the_title() != '') { ?>
						<div class="juliet-gallery-title-border"><h2><?php the_title(); ?></h2></div>	
					<?php } ?>	
				</div>
			</div>			
		</a>		
	<?php } ?>
	
</article>	