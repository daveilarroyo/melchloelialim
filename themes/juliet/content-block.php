<?php 

/**
* Template for blog posts displayed on a page with the Block Layout
* Used by: page-block.php
*/

//Theme Options
global $juliet_theme_data; global $juliet_gen_theme_options;
$juliet_count = $juliet_theme_data->count;
$juliet_post_options = get_option('juliet_blog_options');
$juliet_gen_theme_options['hide_share_buttons'] = isset($juliet_post_options['juliet_blog_hide_share_buttons']) ? $juliet_post_options['juliet_blog_hide_share_buttons'] : false;
$juliet_gen_theme_options['hide_feat_image'] = isset($juliet_post_options['juliet_blog_hide_featured_image']) ? $juliet_post_options['juliet_blog_hide_featured_image'] : false;
$juliet_gen_theme_options['hide_post_excerpt'] = isset($juliet_post_options['juliet_blog_hide_excerpt']) ? $juliet_post_options['juliet_blog_hide_excerpt'] : false;
$juliet_gen_theme_options['hide_comm_count'] = isset($juliet_post_options['juliet_blog_hide_comment_count']) ? $juliet_post_options['juliet_blog_hide_comment_count'] : false;
$juliet_gen_theme_options['enable_pinterest'] = isset($juliet_post_options['juliet_blog_enable_pinterest']) ? $juliet_post_options['juliet_blog_enable_pinterest'] : false;
$juliet_gen_theme_options['post_img_type'] = ($juliet_gen_theme_options['hide_feat_image'] || !has_post_thumbnail()) ? 'juliet-no-post-img' : '';
$juliet_gen_theme_options['no_post_footer'] = $juliet_gen_theme_options['hide_post_excerpt'] && $juliet_gen_theme_options['hide_share_buttons'] && ($juliet_gen_theme_options['hide_comm_count'] || !comments_open()) ? 'juliet-no-post-footer' : '';
$juliet_gen_theme_options['block_layout'] = true;
$juliet_gen_theme_options['sidebar'] = false;
$juliet_gen_theme_options['excerpt_size'] = 240;
$juliet_gen_theme_options['layout_type'] = 'block';

//Post Meta 
$juliet_gen_theme_options['aff_type'] = get_post_meta(get_the_ID(), 'juliet-affiliate-type', true);
$juliet_gen_theme_options['aff_code'] = get_post_meta(get_the_ID(), 'juliet-affiliate-code', true);
$juliet_gen_theme_options['aff_image'] = "";
$juliet_hide_post_cat = isset($juliet_post_options['juliet_blog_hide_cat']) ? $juliet_post_options['juliet_blog_hide_cat'] : false;
$juliet_cat_highlighted = get_post_meta(get_the_ID(), 'juliet-cat-highlight', true);
$juliet_block_type = $juliet_count % 2 == 0 ? 'juliet-even-row' : 'juliet-odd-row'; 
$juliet_aff_in_post = $juliet_gen_theme_options['aff_type'] == 'juliet-aff-code' ? true : false; ?>


<article id="post-<?php the_ID(); ?>" <?php post_class('juliet-block-layout'); ?>>

	<div class="juliet-post-content <?php echo esc_attr($juliet_block_type); ?>">

		<?php if(!($juliet_count % 2 == 0)) { ?>
		
			<!--Display Post Media-->
			<?php locate_template( array('inc/templates/blog_templates/post_media.php'), true, false); ?>
		
			<!--Display Post Content-->
			<div class="juliet-post-entry <?php echo esc_attr($juliet_gen_theme_options['post_img_type']); ?>">

				<?php if($juliet_cat_highlighted != '' && $juliet_cat_highlighted != 'juliet-no-cats' && !$juliet_hide_post_cat) { ?>	
					<a href="<?php echo esc_url(get_category_link($juliet_cat_highlighted)); ?>"><h4 class="juliet-cat-highlighted"><?php echo esc_html(get_cat_name($juliet_cat_highlighted)); ?></h4></a>
				<?php } 		

				if($juliet_aff_in_post) { 
					locate_template( array('inc/templates/blog_templates/affiliate_links/affiliate_link_below.php'), true, false);
				} else {
					locate_template( array('inc/templates/blog_templates/affiliate_links/affiliate_link_none.php'), true, false);
				} ?>
		
			</div>

		<?php } 

		else { ?>

			<!--Display Post Media (Mobile) -->
			<div class="juliet-block-mobile-post-media">
					<?php locate_template( array('inc/templates/blog_templates/post_media.php'), true, false); ?>
			</div>	

			<!--Display Post Content-->
			<div class="juliet-post-entry <?php echo esc_attr($juliet_gen_theme_options['post_img_type']); ?>">

				<?php if($juliet_cat_highlighted != '' && $juliet_cat_highlighted != 'juliet-no-cats' && !$juliet_hide_post_cat) { ?>	
					<a href="<?php echo esc_url(get_category_link($juliet_cat_highlighted)); ?>"><h4 class="juliet-cat-highlighted"><?php echo esc_html(get_cat_name($juliet_cat_highlighted)); ?></h4></a>
				<?php } 		

				if($juliet_aff_in_post) { 
					locate_template( array('inc/templates/blog_templates/affiliate_links/affiliate_link_below.php'), true, false);
				} else {
					locate_template( array('inc/templates/blog_templates/affiliate_links/affiliate_link_none.php'), true, false);
				} ?>
		
			</div>

			<!--Display Post Media-->
			<?php locate_template( array('inc/templates/blog_templates/post_media.php'), true, false); ?>

		<?php } ?>	

	</div>

</article>