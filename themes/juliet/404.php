<?php get_header(); 

//Theme Options
$juliet_general_options = get_option('juliet_general_theme_options');
$juliet_left_sidebar = isset($juliet_general_options['juliet_left_sidebar']) ? $juliet_general_options['juliet_left_sidebar'] : false;

//Archive Options
$juliet_archive_options = get_option('juliet_archive_options');
$juliet_sidebar = isset($juliet_archive_options['juliet_archive_page_width']) ? $juliet_archive_options['juliet_archive_page_width'] : false;
$juliet_content_classes;
if($juliet_sidebar && $juliet_left_sidebar) {
	$juliet_content_classes = 'juliet-default-width juliet-content-right';
} else if ($juliet_sidebar) {
	$juliet_content_classes = 'juliet-default-width';
} else {
	$juliet_content_classes = 'juliet-full-width';
}

?>
	
</div> <!--end header wrapper -->

<div id="juliet-content-container">
	
	<div class="juliet-container">
	
		<div id="juliet-content" class="<?php echo esc_attr($juliet_content_classes); ?>">
		
			<div class="juliet-error-page">
				
				<h1>404</h1>
				<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'juliet' ); ?></p>
				<?php get_search_form(); ?>
				
			</div>
		
		</div> <!--End juliet-content -->

	<?php if($juliet_sidebar) {
		get_sidebar(); 
	} ?>

	</div> <!--End juliet-container -->

	<?php get_footer(); ?>