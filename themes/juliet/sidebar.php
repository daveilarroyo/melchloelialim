<?php //Theme Options
$juliet_general_options = get_option('juliet_general_theme_options');
$juliet_shop_options = get_option('juliet_shop_options'); 
$juliet_left_sidebar = isset($juliet_general_options['juliet_left_sidebar']) &&  $juliet_general_options['juliet_left_sidebar'] == true ? 'juliet-left-sidebar' : ''; 
$juliet_enable_woo_sidebar = isset($juliet_shop_options['juliet_enable_woo_sidebar']) ?  $juliet_shop_options['juliet_enable_woo_sidebar'] : false; ?>

<aside id="juliet-sidebar" class="<?php echo esc_attr($juliet_left_sidebar); ?>">
	
	<?php	/* Widgetised Area */	

		if(in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
			if($juliet_enable_woo_sidebar && (is_woocommerce() || is_checkout() || is_cart() || is_account_page())) {
				if ( is_active_sidebar('juliet-woocommerce-sidebar') ) { 
					dynamic_sidebar('juliet-woocommerce-sidebar'); 
				} 
			} else {
				if ( is_active_sidebar('juliet-sidebar') ) { 
					dynamic_sidebar('juliet-sidebar'); 
				} 
			}	
		} else {
			if ( is_active_sidebar('juliet-sidebar') ) { 
				dynamic_sidebar('juliet-sidebar'); 
			} 
		} ?>
			
</aside>