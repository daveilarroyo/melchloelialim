<div class="juliet-post-comments" id="comments">
	
	<?php $juliet_placeholder = esc_html__( 'COMMENT', 'juliet' );

	$juliet_custom_comment_field = '<p class="juliet-comment-form-comment"><textarea id="comment" name="comment" placeholder="'. $juliet_placeholder . '" rows="12" aria-required="true"></textarea></p>';  //label removed for cleaner layout

	comment_form(array(
		'comment_field'			=> $juliet_custom_comment_field,
		'comment_notes_after'	=> '',
		'logged_in_as' 			=> '',
		'comment_notes_before' 	=> '',
		'title_reply'			=> '',
		'cancel_reply_link'		=> esc_html__('Cancel Comment', 'juliet'),
		'label_submit'			=> esc_html__('Submit', 'juliet')
	));

	echo "<ul class='comments'>";
	
		wp_list_comments(array(
			'avatar_size'	=> 60,
			'max_depth'		=> 5,
			'style'			=> 'ul',
			'callback'		=> 'juliet_comments',
			'type'			=> 'all'
		));

	echo "</ul>";

	echo "<div id='comments_pagination'>";
	paginate_comments_links(array('prev_text' => '&laquo;', 'next_text' => '&raquo;'));
	echo "</div>";
 ?>


</div>