<?php 

//Theme Options
$juliet_post_options = get_option('juliet_blog_options');
$juliet_general_options = get_option('juliet_general_theme_options');
$juliet_hide_share_buttons = isset($juliet_post_options['juliet_blog_hide_share_buttons']) ? $juliet_post_options['juliet_blog_hide_share_buttons'] : false;
$juliet_left_sidebar = isset($juliet_general_options['juliet_left_sidebar']) ? $juliet_general_options['juliet_left_sidebar'] : false;
$juliet_woo_sidebar = isset($juliet_shop_options['juliet_enable_woo_sidebar']) ? $juliet_shop_options['juliet_enable_woo_sidebar'] : false;
$juliet_content_classes;
if($juliet_woo_sidebar && $juliet_left_sidebar) {
	$juliet_content_classes = 'juliet-default-width juliet-content-right';
} else if ($juliet_woo_sidebar) {
	$juliet_content_classes = 'juliet-default-width';
} else {
	$juliet_content_classes = 'juliet-full-width';
}

get_header(); ?>

</div>	<!-- End juliet-header-wrapper -->
<div id="juliet-content-container">
	
	<div class="juliet-container">
		
		<div id="juliet-content" class="<?php echo esc_attr($juliet_content_classes); ?>">

			<?php woocommerce_content(); ?>	

			<?php if(!$juliet_hide_share_buttons && !is_shop()) { ?>
				<div class="juliet-woo-footer">
					<span class="juliet-share-text-border"><span class="juliet-share-text"><?php esc_html_e( 'Share This Page', 'juliet' ); ?></span></span>
					<div class="juliet-woo-share-buttons">
						<?php echo juliet_share_buttons(); ?>
					</div>	
				</div>	
			<?php } ?>

		</div>	<!--end juliet-content -->

		<?php if($juliet_woo_sidebar) {
			get_sidebar(); 
		} ?>			
	
	</div>	<!--end juliet-container -->
	
	<?php get_footer(); ?>		